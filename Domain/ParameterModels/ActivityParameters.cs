﻿using Shared.RequestFeatures;

namespace Domain.ParameterModels
{
    public class ActivityParameters : RequestParameters
    {
        public string CategoryFilter { get; set; }

        public bool IsGoingFilter { get; set; }

        public bool IsHostFilter { get; set; }

        public DateTime StartDate { get; set; } = DateTime.UtcNow;

        public string SearchTerm { get; set; }

        public ActivityParameters()
        {
            // Default sorted by title
            OrderBy = "title";
        }
    }
}
