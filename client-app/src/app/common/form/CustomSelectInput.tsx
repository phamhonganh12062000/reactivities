import { useField } from "formik";
import { Form, Label, Select } from "semantic-ui-react";

interface Props {
    placeholder: string;
    name: string;
    options: any;
    label?: string;
}

// Reusable text input
export default function CustomSelectInput(props: Props) {
    // field contains props + even handlers,
    // while meta contains metadata about the field e.g., touch state, validaion errors
    const [field, meta, helpers] = useField(props.name);

    return (
        // Form.Field wraps the label + input element
        // Cast the error into a boolean => Display error if field touched & has an error
        <Form.Field error={meta.touched && !!meta.error}>
            <label>{props.label}</label>
            {/* field passes properties like name, value, onChange, onBlur to the input field
                props passes in additional props to the component to CUSTOMIZE the behavior + appearance of the input field
            */}
            <Select
                clearable
                options={props.options}
                value={field.value || null}
                onChange={(e, d) => helpers.setValue(d.value)}
                onBlur={() => helpers.setTouched(true)}
                placeholder={props.placeholder}
            />
            {meta.touched && meta.error ? (
                <Label basic color="red">
                    {meta.error}
                </Label>
            ) : null}
        </Form.Field>
    );
}
