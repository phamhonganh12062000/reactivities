﻿using Application.Caching;
using Application.Services.Activities.Commands;
using AutoMapper;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Exceptions.Activities;
using Domain.Exceptions.AppUser;
using Domain.Models;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Handlers
{
  public sealed record UpdateAttendanceCommand(Guid Id, bool TrackChanges) : IRequest<Result<Unit>>
  {
  }

  public sealed class UpdateAttendanceHandler : IRequestHandler<UpdateAttendanceCommand, Result<Unit>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly ILoggerManager _logger;
    private readonly IUserAccessor _userAccessor;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public UpdateAttendanceHandler(IUnitOfWork unitOfWork, ILoggerManager logger, IUserAccessor userAccessor, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _logger = logger;
      _userAccessor = userAccessor;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Unit>> Handle(UpdateAttendanceCommand request, CancellationToken cancellationToken)
    {
      var activityCacheKey = CachingStatics.GetActivityById(request.Id);

      // Remove cache for the activity
      _memoryCacheWrapper.Remove(activityCacheKey);

      var activity = await _unitOfWork.Activity.GetSingleActivityAsync(request.Id, request.TrackChanges) ?? throw new ActivityNotFoundException(request.Id);

      var currentLoggedInUser = await _unitOfWork.AppUser.GetAppUserAsync(_userAccessor.GetCurrentUserUsername(), trackChanges: request.TrackChanges);

      if (currentLoggedInUser is null)
      {
        throw new AppUserNotFoundException(currentLoggedInUser);
      }

      var host = await _unitOfWork.ActivityAttendee.GetActivityHostAsync(activity, trackChanges: request.TrackChanges);

      var attendance = await _unitOfWork.ActivityAttendee.GetActivityAttendeeByUsernameAsync(activity, currentLoggedInUser, trackChanges: request.TrackChanges);

      // Toggle cancellation status for activity as activity host
      if (attendance != null && host.AppUser.UserName == currentLoggedInUser.UserName)
      {
        activity.IsCancelled = !activity.IsCancelled;
      }

      // Remove attendee if attended already
      if (attendance != null && host.AppUser.UserName != currentLoggedInUser.UserName)
      {
        activity.Attendees.Remove(attendance);
      }

      if (attendance is null)
      {
        attendance = new ActivityAttendee
        {
          AppUser = currentLoggedInUser,
          Activity = activity,
          IsHost = false,
        };

        activity.Attendees.Add(attendance);
      }

      var isSavedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSavedSuccess)
      {
        _logger.LogError("Problem updating attendance");
        return Result<Unit>.Failure("Problem updating attendance");
      }

      // _memoryCacheWrapper.Set(activityCacheKey, activity, null);

      return Result<Unit>.Success(Unit.Value);
    }
  }
}
