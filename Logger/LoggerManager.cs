﻿using Contracts.Logger;
using Serilog;

namespace Logger
{
    public class LoggerManager : ILoggerManager
    {
        private readonly ILogger _logger;

        public LoggerManager()
        {
            _logger = Log.Logger;
        }

        public void LogDebug(string message, params object[] propertyValues)
        {
            _logger.Debug(message, propertyValues);
        }

        public void LogError(string message)
        {
            _logger.Error(message);
        }

        // TODO: WIP, might be unnecessary
        public void LogError(Exception ex, string message, params object[] propertyValues)
        {
            _logger.Error(ex, message, propertyValues);
        }

        public void LogInfo(string message, params object[] propertyValues)
        {
            _logger.Information(message, propertyValues);
        }

        public void LogWarning(string message, params object[] propertyValues)
        {
            _logger.Warning(message, propertyValues);
        }
    }
}
