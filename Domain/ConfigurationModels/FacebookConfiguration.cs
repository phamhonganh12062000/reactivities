namespace Domain.ConfigurationModels
{
  public class FacebookConfiguration
  {
    public string AppId { get; set; }

    public string AppSecret { get; set; }
  }
}
