namespace Domain.Dtos.Photo
{
    public record PhotoDisplayDto
    {
        public string Id { get; init; }

        public string Url { get; init; }

        public bool IsMain { get; init; }
    }
}
