using AutoMapper;
using Domain.Dtos.Photo;
using Domain.Models;

namespace Application.Mappers
{
    public class MappingPhotoProfile : Profile
    {
        public MappingPhotoProfile()
        {
            CreateMap<PhotoUploadResultDto, Photo>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.PublicId))
                .ForMember(d => d.Url, o => o.MapFrom(s => s.Url));

            CreateMap<Photo, PhotoDisplayDto>();
        }
    }
}
