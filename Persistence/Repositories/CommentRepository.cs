using Contracts.Persistence;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(DataContext context)
            : base(context)
        {
        }

        public async Task<IEnumerable<Comment>> GetAllActivityCommentsAsync(Guid activityId, bool trackChanges)
        {
            return await Find(a => a.Activity.Id.Equals(activityId), trackChanges)
                .Include(a => a.Author)
                .ThenInclude(p => p.Photos)
                .OrderByDescending(x => x.CreatedAt)
                .ToListAsync();
        }
    }
}
