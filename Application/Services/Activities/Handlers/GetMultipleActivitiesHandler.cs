﻿using Application.Services.Activities.Queries;
using AutoMapper;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Exceptions;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Handlers
{
    public sealed class GetMultipleActivitiesHandler : IRequestHandler<GetMultipleActivitiesQuery, Result<IEnumerable<ActivityDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMultipleActivitiesHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<IEnumerable<ActivityDto>>> Handle(GetMultipleActivitiesQuery request, CancellationToken cancellationToken)
        {
            if (request.Ids is null)
            {
                throw new IdParametersBadRequestException();
            }

            var collectionOfActivities = await _unitOfWork.Activity.GetMultipleActivitiesAsync(request.Ids, request.TrackChanges);

            if (request.Ids.Count() != collectionOfActivities.Count())
            {
                throw new CollectionByIdsBadRequestException();
            }

            return Result<IEnumerable<ActivityDto>>.Success(_mapper.Map<IEnumerable<ActivityDto>>(collectionOfActivities));
        }
    }
}
