using Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace  Contracts.Application.Services
{
  public interface IFacebookLoginService
  {
    Task<(IdentityResult, AppUser)> FacebookLogin(string accessToken);
  }
}
