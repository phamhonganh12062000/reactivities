import { createBrowserRouter, Navigate, RouteObject } from "react-router-dom";
import ActivityDashboard from "../../features/activities/dashboard/ActivityDashboard";
import ActivityDetails from "../../features/activities/details/ActivityDetails";
import ActivityForm from "../../features/activities/form/ActivityForm";
import NotFound from "../../features/errors/NotFoundError";
import ServerError from "../../features/errors/ServerError";
import TestErrors from "../../features/errors/TestError";
import ProfilePage from "../../features/profiles/ProfilePage";
import ConfirmEmail from "../../features/users/ConfirmEmail";
import LoginForm from "../../features/users/LoginForm";
import App from "../layout/App";
import RequireAuth from "./RequireAuth";
import RegisterSuccess from "../../features/users/RegisterSuccess";

export const routes: RouteObject[] = [
    {
        path: "/",
        element: <App />,
        children: [
            {
                element: <RequireAuth />,
                children: [
                    { path: "activities", element: <ActivityDashboard /> },
                    { path: "activities/:id", element: <ActivityDetails /> },
                    { path: "profiles/:username", element: <ProfilePage /> },
                    {
                        path: "createActivity",
                        element: <ActivityForm key={"create"} />,
                    },
                    {
                        path: "editActivity/:id",
                        element: <ActivityForm key={"edit"} />,
                    },
                    {
                        path: "login",
                        element: <LoginForm />,
                    },
                    {
                        path: "errors",
                        element: <TestErrors />,
                    },
                ],
            },
            {
                path: "not-found",
                element: <NotFound />,
            },
            {
                path: "server-error",
                element: <ServerError />,
            },
            {
                path: "*",
                element: <Navigate replace to="/not-found" />,
            },
            {
                path: "/account/verifyEmail",
                element: <ConfirmEmail />,
            },
            {
                path: "/account/registerSuccess",
                element: <RegisterSuccess />,
            },
        ],
    },
];

export const router = createBrowserRouter(routes);
