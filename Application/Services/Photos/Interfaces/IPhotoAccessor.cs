using Domain.Dtos.Photo;
using Microsoft.AspNetCore.Http;

namespace Application.Services.Photos.Interfaces
{
    // NOTE: Contracts.csprj does not have library Microsoft.AspNetCore.Http
    public interface IPhotoAccessor
    {
        Task<PhotoUploadResultDto> AddPhoto(IFormFile file);

        Task<string> DeletePhoto(string publicId);
    }
}
