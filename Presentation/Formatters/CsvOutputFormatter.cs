﻿using Domain.Dtos.Activity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System.Text;

namespace Presentation.Formatters
{
    public class CsvOutputFormatter : TextOutputFormatter
    {
        public CsvOutputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }

        /// <summary>
        /// Indicate whether or not ActivityDto can be written by this serializer.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override bool CanWriteType(Type? type)
        {
            if (typeof(ActivityDto).IsAssignableFrom(type)
                || typeof(IEnumerable<ActivityDto>).IsAssignableFrom(type))
            {
                return base.CanWriteType(type);
            }

            return false;
        }

        private static void FormatCsv(StringBuilder buffer, ActivityDto activity)
        {
            buffer.AppendLine($"{activity.Id},\"{activity.Title},\"{activity.Date},\"{activity.Description},\"{activity.Category},\"{activity.City},\"{activity.Venue}");
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var response = context.HttpContext.Response;
            var buffer = new StringBuilder();

            if (context.Object is IEnumerable<ActivityDto>)
            {
                foreach (var activity in (IEnumerable<ActivityDto>)context.Object)
                {
                    FormatCsv(buffer, activity);
                }
            }
            else
            {
                FormatCsv(buffer, (ActivityDto)context.Object);
            }

            await response.WriteAsync(buffer.ToString());
        }
    }
}
