import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import utc from "dayjs/plugin/utc";
import { Field, FieldProps, Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { Comment, Header, Loader, Segment } from "semantic-ui-react";
import * as Yup from "yup";
import { useStore } from "../../../app/stores/baseStore";

interface Props {
    activityId: string;
}

export default observer(function ActivityDetailChat({ activityId }: Props) {
    const { commentStore } = useStore();

    const commentValidation = Yup.object({
        body: Yup.string().required(),
    });
    useEffect(() => {
        if (activityId) {
            commentStore.createHubConnection(activityId);
        }

        return () => {
            commentStore.clearComments();
        };
    }, [commentStore, activityId]);

    // Fix invalid time range on Linux prod
    dayjs.extend(utc);
    dayjs.extend(relativeTime);

    return (
        <>
            <Segment
                textAlign="center"
                attached="top"
                inverted
                color="teal"
                style={{ border: "none" }}
            >
                <Header>Chat about this event</Header>
            </Segment>
            <Segment attached clearing>
                <Formik
                    onSubmit={(values, { resetForm }) => {
                        commentStore.addComment(values).then(() => resetForm());
                    }}
                    initialValues={{ body: "" }}
                    validationSchema={commentValidation}
                >
                    {({ isSubmitting, isValid, handleSubmit }) => (
                        <Form className="ui form">
                            <Field name="body">
                                {(props: FieldProps) => (
                                    <div style={{ position: "relative" }}>
                                        <Loader active={isSubmitting} />
                                        <textarea
                                            placeholder="Enter your comment (Enter to submit, SHIFT + Enter for new line)"
                                            rows={2}
                                            {...props.field}
                                            onKeyDownCapture={(e) => {
                                                // Add new line
                                                if (
                                                    e.key === "Enter" &&
                                                    e.shiftKey
                                                ) {
                                                    return;
                                                }
                                                // Submit comment
                                                if (
                                                    e.key === "Enter" &&
                                                    !e.shiftKey
                                                ) {
                                                    e.preventDefault();
                                                    isValid && handleSubmit();
                                                }
                                            }}
                                        />
                                    </div>
                                )}
                            </Field>
                        </Form>
                    )}
                </Formik>
                <Comment.Group>
                    {commentStore.comments.map((comment) => (
                        <Comment key={comment.id}>
                            <Comment.Avatar
                                src={comment.image || "/assets/user.png"}
                            />
                            <Comment.Content>
                                <Comment.Author
                                    as={Link}
                                    to={`/profiles/${comment.username}`}
                                >
                                    {comment.displayName}
                                </Comment.Author>
                                <Comment.Metadata>
                                    <div>
                                        {/* format strict does not work too */}
                                        {dayjs(comment.createdAt).fromNow()}
                                    </div>
                                </Comment.Metadata>
                                <Comment.Text
                                    style={{ whiteSpace: "pre-wrap" }}
                                >
                                    {comment.body}
                                </Comment.Text>
                            </Comment.Content>
                        </Comment>
                    ))}
                </Comment.Group>
            </Segment>
        </>
    );
});
