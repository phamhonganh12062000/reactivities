﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Dtos.Authentication
{
    public record RegistrationDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; init; }

        [Required]
        [RegularExpression("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{10,}$", ErrorMessage = "Password must be complex.")]
        public string Password { get; init; }

        [Required]
        public string DisplayName { get; init; }

        [Required(ErrorMessage = "Username is required")]
        public string Username { get; init; }
    }
}
