using Contracts.Logger;
using Domain.ConfigurationModels;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Application.Caching
{
  public interface IMemoryCacheWrapper
  {
    Task<T> GetOrCreateAsync<T>(string cacheKey, Func<Task<T>> retrieveDataAsync, MemoryCacheEntryOptions? cacheEntryOptions);

    T GetOrCreate<T>(string cacheKey, Func<T> retrieveData, MemoryCacheEntryOptions? cacheEntryOptions);

    // void Set<T>(string cacheKey, T cacheEntry, MemoryCacheEntryOptions memoryCacheEntryOptions);

    void Remove(string cacheKey);
  }

  public class MemoryCacheWrapper : IMemoryCacheWrapper
  {
    private readonly IMemoryCache _memoryCache;

    private readonly IOptions<CacheConfiguration> _cacheConfiguration;

    private readonly ILoggerManager _logger;

    private readonly SemaphoreSlim _locker = new(1, 1);

    public MemoryCacheWrapper(IMemoryCache memoryCache, IOptions<CacheConfiguration> cacheConfiguration, ILoggerManager logger)
    {
      _memoryCache = memoryCache;
      _cacheConfiguration = cacheConfiguration;
      _logger = logger;
    }

    // TODO: Add cache for GetAllActivitiesHandler
    public T GetOrCreate<T>(string cacheKey, Func<T> retrieveData, MemoryCacheEntryOptions? cacheEntryOptions = null)
    {
      if (!_memoryCache.TryGetValue(cacheKey, out T cacheEntry))
      {
        try
        {
          _locker.Wait();

          cacheEntry = retrieveData();

          cacheEntryOptions ??= ReturnMemoryCacheEntryOptions(_cacheConfiguration);

          _memoryCache.Set(cacheKey, cacheEntry, cacheEntryOptions);
        }
        finally
        {
          _locker.Release();
        }
      }

      return cacheEntry;
    }

    public async Task<T> GetOrCreateAsync<T>(string cacheKey, Func<Task<T>> retrieveDataAsync, MemoryCacheEntryOptions? cacheEntryOptions = null)
    {
      if (!_memoryCache.TryGetValue(cacheKey, out T cacheEntry))
      {
        try
        {
          await _locker.WaitAsync();

          cacheEntry = await retrieveDataAsync();

          cacheEntryOptions ??= ReturnMemoryCacheEntryOptions(_cacheConfiguration);

          cacheEntryOptions.RegisterPostEvictionCallback(PostEviction);

          _memoryCache.Set(cacheKey, cacheEntry, cacheEntryOptions);
        }
        finally
        {
          _locker.Release();
        }
      }

      return cacheEntry;
    }

    public void Remove(string cacheKey)
    {
      if (DoesCacheExist(cacheKey))
      {
        _memoryCache.Remove(cacheKey);
      }
    }

    private static MemoryCacheEntryOptions ReturnMemoryCacheEntryOptions(IOptions<CacheConfiguration> cacheSettings)
    {
      return new MemoryCacheEntryOptions()
        .SetSlidingExpiration(cacheSettings.Value.SlidingExpiration)
            .SetAbsoluteExpiration(cacheSettings.Value.AbsoluteExpiration)
            .SetPriority(cacheSettings.Value.Priority)
            .SetSize(cacheSettings.Value.Size);
    }

    private void PostEviction(object key, object value, EvictionReason reason, object state)
    {
      _logger.LogDebug($"Cache entry evicted. key: {key}, Reason: {reason}");

      if (state is IDisposable disposable)
      {
        disposable.Dispose();
      }
    }

    private bool DoesCacheExist(string cacheKey)
    {
      if (!_memoryCache.TryGetValue(cacheKey, out var _))
      {
        return false;
      }

      return true;
    }

    // public void Set<T>(string cacheKey, T cacheEntry, MemoryCacheEntryOptions memoryCacheEntryOptions = null)
    // {
    //   if (memoryCacheEntryOptions != null)
    //   {
    //     _memoryCache.Set(cacheKey, cacheEntry, memoryCacheEntryOptions);
    //   }

    //   memoryCacheEntryOptions ??= ReturnMemoryCacheEntryOptions(_cacheConfiguration);

    //   memoryCacheEntryOptions.RegisterPostEvictionCallback(PostEviction);

    //   _memoryCache.Set(cacheKey, cacheEntry, memoryCacheEntryOptions);
    // }
  }
}
