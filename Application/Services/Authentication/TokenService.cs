﻿using Contracts.Application.Services;
using Domain.ConfigurationModels;
using Domain.Exceptions.Authentication;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Application.Services.Authentication
{
  internal class TokenService : ITokenService
  {
    private readonly IConfiguration _configuration;
    private readonly JwtConfiguration _jwtConfiguration;
    private readonly UserManager<AppUser> _userManager;

    private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;

    public TokenService(IConfiguration configuration, IOptions<JwtConfiguration> jwtConfigOptions, UserManager<AppUser> userManager)
    {
      _configuration = configuration;
      _jwtConfiguration = jwtConfigOptions.Value;
      _userManager = userManager;
      _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
    }

    public async Task<AppUser> FetchUserWithExpiredAccessToken(string refreshToken)
    {
      var user = await _userManager.Users
        .Include(p => p.Photos)
        .FirstOrDefaultAsync(r => r.RefreshToken == refreshToken);

      if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
      {
        throw new RefreshTokenBadRequest();
      }

      return user;
    }

    public async Task<string> GenerateAccessToken(AppUser user)
    {
      var signingCreds = GetSigningCredentials();

      var claims = GetClaims(user);

      var tokenDescriptor = GenerateAccessTokenDescriptor(signingCreds, claims);

      var token = _jwtSecurityTokenHandler.WriteToken(_jwtSecurityTokenHandler.CreateToken(tokenDescriptor));

      if (!ValidateAccessToken(token))
      {
        throw new SecurityTokenException("Invalid token.");
      }

      return token;
    }

    public async Task<string> PopulateRefreshTokenExpiryDate(AppUser user, bool populateExpiryDate)
    {
      var refreshToken = GenerateRefreshToken();

      user.RefreshToken = refreshToken;

      if (populateExpiryDate)
      {
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(7);
      }

      await _userManager.UpdateAsync(user);

      return refreshToken;
    }

    private byte[] ReturnEncodedSecretKey()
    {
      return Encoding.UTF8.GetBytes(_configuration["JWTSecretKey"]);
    }

    private SymmetricSecurityKey ReturnSecurityKey()
    {
      var encodedSecretKey = ReturnEncodedSecretKey();

      return new SymmetricSecurityKey(encodedSecretKey);
    }

    private SigningCredentials GetSigningCredentials()
    {
      var securityKey = ReturnSecurityKey();

      return new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);
    }

    private List<Claim> GetClaims(AppUser user)
    {
      return new List<Claim>
            {
                new(ClaimTypes.Name, user.UserName),
                new(ClaimTypes.NameIdentifier, user.Id),
                new(ClaimTypes.Email, user.Email),
            };
    }

    private SecurityTokenDescriptor GenerateAccessTokenDescriptor(SigningCredentials signingCreds, List<Claim> claims)
    {
      return new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(claims),
        Expires = DateTime.UtcNow.AddMinutes(Convert.ToDouble(_jwtConfiguration.Expires)),
        SigningCredentials = signingCreds,
      };
    }

    private string GenerateRefreshToken()
    {
      var randonNum = new byte[32];

      using var rng = RandomNumberGenerator.Create();

      rng.GetBytes(randonNum);

      return Convert.ToBase64String(randonNum);
    }

    private bool ValidateAccessToken(string accessToken)
    {
      var tokenValidationParams = new TokenValidationParameters
      {
        ValidateAudience = false,
        ValidateIssuer = false,
        ValidateIssuerSigningKey = true,
        ValidateLifetime = true,
        IssuerSigningKey = ReturnSecurityKey(),
      };

      _jwtSecurityTokenHandler.ValidateToken(accessToken, tokenValidationParams, out SecurityToken securityToken);

      var jwtSecurityToken = (JwtSecurityToken)securityToken;

      if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha512, StringComparison.InvariantCultureIgnoreCase))
      {
        return false;
      }

      return true;
    }
  }
}
