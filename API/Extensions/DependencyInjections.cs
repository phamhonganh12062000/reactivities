﻿using Application.Behaviors;
using Application.DataShaping;
using Application.Services;
using Contracts.Infrastructure;
using Contracts.Extensions;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Infrastructure.Security;
using Logger;
using MediatR;
using Persistence;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.Photos;
using Application.Services.Photos.Interfaces;
using Domain.Models;
using Contracts.Domain;
using Presentation.Utility;
using Presentation.ActionFilters;
using Contracts.Application.Services;
using Application.Caching;
using Domain.ConfigurationModels;
using Infrastructure.Email;

namespace API.Extensions
{
  public static class DependencyInjections
  {
    public static void AddLoggerService(this IServiceCollection services)
    {
      services.AddSingleton<ILoggerManager, LoggerManager>();
    }

    public static void AddUnitOfWork(this IServiceCollection services)
    {
      services.AddScoped<IUnitOfWork, UnitOfWork>();
    }

    public static void AddPipelineValidationBehavior(this IServiceCollection services)
    {
      services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationAppBehavior<,>));
    }

    public static void AddServiceManager(this IServiceCollection services)
    {
      services.AddScoped<IServiceManager, ServiceManager>();
    }

    public static void AddDataShaping(this IServiceCollection services)
    {
      services.AddScoped<IDataShaper<ActivityDto>, DataShaper<ActivityDto>>();
      services.AddScoped<IDataShaper<ProfileDto>, DataShaper<ProfileDto>>();
    }

    public static void AddUserAccessor(this IServiceCollection services)
    {
      services.AddScoped<IUserAccessor, UserAccessor>();
    }

    public static void AddAuthorizationHandler(this IServiceCollection services)
    {
      services.AddTransient<IAuthorizationHandler, HostRequirementHandler>();
    }

    public static void AddPhotoAccesor(this IServiceCollection services)
    {
      services.AddScoped<IPhotoAccessor, PhotoAccessor>();
    }

    public static void AddEntityLinks(this IServiceCollection services)
    {
      services.AddScoped<IAppUserLinks, AppUserLinks>();
    }

    public static void AddValidationForMediaTypeAttribute(this IServiceCollection services)
    {
      services.AddScoped<ValidateMediaTypeAttribute>();
    }

    public static void AddMemoryCacheWrapper(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddSingleton<IMemoryCacheWrapper, MemoryCacheWrapper>();
      services.Configure<CacheConfiguration>(configuration.GetSection("CacheConfiguration"));
    }

    public static void AddEmailSenderFactory(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddScoped<IEmailSenderFactory, EmailSenderFactory>();
      services.Configure<SmtpConfiguration>(configuration.GetSection("SmtpConfiguration"));
      services.Configure<PostmarkConfiguration>(configuration.GetSection("PostmarkConfiguration"));
    }
  }
}
