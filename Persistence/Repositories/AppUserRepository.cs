﻿using Contracts.Persistence;
using Domain.Models;
using Domain.ParameterModels;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
  public class AppUserRepository : RepositoryBase<AppUser>, IAppUserRepository
  {
    public AppUserRepository(DataContext dataContext)
        : base(dataContext)
    {
    }

    public async Task<AppUser> GetAppUserAsync(string username, bool trackChanges)
    {
      return await Find(u => u.UserName.Equals(username), trackChanges)
          .Include(p => p.Photos)
          .Include(fi => fi.Followings)
          .Include(fer => fer.Followers).ThenInclude(obs => obs.Observer)
          .FirstOrDefaultAsync();
    }
  }
}
