﻿namespace Domain.Dtos.Authentication
{
    public record AuthenticationDto
    {
        public string DisplayName { get; init; }

        public string AccessToken { get; init; }

        public string Image { get; init; }

        public string Username { get; init; }

        public string Bio { get; init; }
    }
}
