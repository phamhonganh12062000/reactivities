﻿namespace Shared.Static
{
    public static class SeedData
    {
        public static class Activities
        {
            public static Guid PastActivity1 = Guid.Parse("c2f9c062-7623-4101-9170-37698edc7707");
            public static Guid PastActivity2 = Guid.Parse("24d5c211-ed62-4c42-8e55-3a5b27964f2b");
            public static Guid FutureActivity1 = Guid.Parse("bf3011d1-d759-4a7c-a92b-8948945b4a2d");
            public static Guid FutureActivity2 = Guid.Parse("e48d0284-f3c7-4101-a9e8-98ce9657bc1a");
            public static Guid FutureActivity3 = Guid.Parse("8e3baf54-02e1-4ffe-b458-0004f5da7063");
            public static Guid FutureActivity4 = Guid.Parse("efca1dc6-cbde-47fd-8141-44c4a2d7c291");
            public static Guid FutureActivity5 = Guid.Parse("927d5ee8-27b0-4fd9-a748-6d2532fc3430");
            public static Guid FutureActivity6 = Guid.Parse("2cdbb75f-9c94-43f9-b80b-76da48ca38c9");
            public static Guid FutureActivity7 = Guid.Parse("a91286aa-4a4d-46f1-9b7e-a6236b4ff446");
            public static Guid FutureActivity8 = Guid.Parse("956b6ed6-dc59-47d2-91ad-3e4eb968901a");
        }

        public static class AppUsers
        {
            public const string Bob = "975ef757-940c-47a9-98b0-42b0707769c8";
            public const string Jane = "89aa0cb3-69a3-4dfd-b46f-1e442a14602c";
            public const string Tom = "0781475a-89de-4a77-98f6-4d04e2878cc1";
        }
    }
}
