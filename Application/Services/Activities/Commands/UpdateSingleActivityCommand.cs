﻿using Domain.Dtos.Activity;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Commands
{
    public sealed record UpdateSingleActivityCommand(Guid Id, ActivityForUpdateDto ActivityForUpdateDto, bool TrackChanges) : IRequest<Result<Unit>>
    {
    }
}
