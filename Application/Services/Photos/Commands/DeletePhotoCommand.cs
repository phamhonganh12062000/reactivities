using Domain.Results;
using MediatR;

namespace Application.Services.Photos.Commands
{
    public sealed record DeletePhotoCommand(string Id, bool UserTrackChanges) : IRequest<Result<Unit>>
    {
    }
}
