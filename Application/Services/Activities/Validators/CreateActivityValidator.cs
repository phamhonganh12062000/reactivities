﻿using Application.Services.Activities.Commands;
using Application.Services.Activities.Handlers;
using Domain.Dtos.Activity;
using FluentValidation;
using FluentValidation.Results;

namespace Application.Services.Activities.Validators
{
    public sealed class CreateActivityValidator : AbstractValidator<CreateSingleActivityCommand>
    {
        public CreateActivityValidator()
        {
            RuleFor(a => a.ActivityForCreationDto).SetValidator(new ActivityValidator());
        }

        /// <summary>
        /// Validate the null request body with Fluent Valdation rules.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override ValidationResult Validate(ValidationContext<CreateSingleActivityCommand> context)
        {
            return context.InstanceToValidate.ActivityForCreationDto is null
                ? new ValidationResult(new[]
                {
                    new ValidationFailure(nameof(ActivityForCreationDto), nameof(ActivityForCreationDto) + " object is null."),
                }) : base.Validate(context);
        }
    }
}
