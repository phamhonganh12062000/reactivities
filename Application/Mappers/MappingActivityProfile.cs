﻿using AutoMapper;
using Domain.Dtos.Activity;
using Domain.Models;

namespace Application.Mappers
{
    public class MappingActivityProfile : Profile
    {
        public MappingActivityProfile()
        {
            CreateMap<Activity, ActivityDto>()

                // Map the 1st attendee aka host to the username section
                .ForMember(d => d.HostUsername, o => o.MapFrom(s => s.Attendees
                    .FirstOrDefault(x => x.IsHost).AppUser.UserName));

            CreateMap<ActivityForCreationDto, Activity>();

            CreateMap<ActivityForUpdateDto, Activity>();
        }
    }
}
