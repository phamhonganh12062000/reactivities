using Serilog.Formatting;
using Serilog.Sinks.SystemConsole.Themes;

namespace Logger
{
  public static class CustomTemplate
  {
    public static AnsiConsoleTheme CustomTheme { get; } = new(
        new Dictionary<ConsoleThemeStyle, string>
        {
          [ConsoleThemeStyle.Text] = "\x1b[38;5;025m",
          [ConsoleThemeStyle.SecondaryText] = "\x1b[38;5;008m",
          [ConsoleThemeStyle.TertiaryText] = "\x1b[38;5;008m",
          [ConsoleThemeStyle.Invalid] = "\x1b[33;1m",
          [ConsoleThemeStyle.Null] = "\x1b[38;5;008m",
          [ConsoleThemeStyle.Name] = "\x1b[38;5;008m",
          [ConsoleThemeStyle.String] = "\x1b[38;5;086m",
          [ConsoleThemeStyle.Number] = "\x1b[38;5;151m",
          [ConsoleThemeStyle.Boolean] = "\x1b[38;5;215m",
          [ConsoleThemeStyle.Scalar] = "\x1b[38;5;077m",
          [ConsoleThemeStyle.LevelVerbose] = "\x1b[37m",
          [ConsoleThemeStyle.LevelDebug] = "\x1b[38;5;251m", // Dark gray
          [ConsoleThemeStyle.LevelInformation] = "\x1b[38;5;045m", // Blue
          [ConsoleThemeStyle.LevelWarning] = "\x1b[38;5;226m", // Yellow
          [ConsoleThemeStyle.LevelError] = "\x1b[38;5;196m", // Red
          [ConsoleThemeStyle.LevelFatal] = "\x1b[38;5;196m",
        });
  }

}
