import dayjs from "../utils/dayjsConfig"
import { makeAutoObservable, reaction, runInAction } from "mobx";
import agent from "../api/agent";
import { Activity, ActivityFormValues } from "../models/activity";
import { PagingMetaData, PagingParams } from "../models/pagination";
import { Profile } from "../models/profile";
import { baseStore } from "./baseStore";

export default class ActivityStore {
    activityRegistry = new Map<string, Activity>();
    selectedActivity: Activity | undefined = undefined;
    editMode = false;
    loading = false;
    loadingInitial = false;
    pagingMetaData: PagingMetaData | null = null;
    pagingPrams = new PagingParams();
    predicate = new Map().set("all", true);

    constructor() {
        makeAutoObservable(this);
        reaction(
            // 1st expression: Observe the keys to see if they change
            () => this.predicate.keys(),
            // 2nd expression
            () => {
                // reset params
                this.pagingPrams = new PagingParams();
                this.activityRegistry.clear();
                // Load the next batch of activities
                this.loadActivities();
            }
        );
    }

    /// Computed function
    get arrActivitiesSortedByDate() {
        return Array.from(this.activityRegistry.values()).sort(
            (a, b) => a.date!.getTime() - b.date!.getTime()
        );
    }

    /**
     * Return an arrays of arrays organized by dates
     * Each sub-array has two elements: A date and an array of activities sharing the same date
     * How it works (from the inside out)
     * 1. Format date and use the date as the key for a linked list, array of activities as the paired value
     * 2. Check if the date as key exist => Yes then append the activity to the array value/No then make the date as a new key and
     * append the activity to the array
     * 2.5. Current data structure is {"date1" : [activity1, activity2], "date2" : [activity3]}
     * 3. The `reduce()` method makes the object into an array of arrays
     * 3.5. Final data structure as [["date1", [activity1, activity2]], ["date2", [activity3]]]
     */
    get arrActivitiesGroupedByDate() {
        return Object.entries(
            this.arrActivitiesSortedByDate.reduce(
                (activitiesGroupedByDates, activity) => {
                    const date = dayjs(activity.date!).format("MMM D YYYY");
                    activitiesGroupedByDates[date] = activitiesGroupedByDates[
                        date
                    ]
                        ? [...activitiesGroupedByDates[date], activity]
                        : [activity];
                    return activitiesGroupedByDates;
                },
                {} as { [key: string]: Activity[] }
            )
        );
    }

    loadActivities = async () => {
        this.setLoadingInitial(true);
        try {
            const result = await agent.Activities.list(this.axiosParams);
            result.data.forEach((activity) => {
                this.setActivity(activity);
            });
            this.setPagination(result.pagingMetaData);
            this.setLoadingInitial(false);
        } catch (error) {
            console.log(error);
            this.setLoadingInitial(false);
        }
    };

    setPagingParams = (pagingParams: PagingParams) => {
        this.pagingPrams = pagingParams;
    };

    get axiosParams() {
        const params = new URLSearchParams();
        params.append("pageNumber", this.pagingPrams.pageNumber.toString());
        params.append("pageSize", this.pagingPrams.pageSize.toString());
        this.predicate.forEach((value, key) => {
            if (key === "startDate") {
                params.append(key, (value as Date).toISOString());
            } else {
                params.append(key, value);
            }
        });
        return params;
    }

    setPredicateKeyValue = (
        predicateKey: string,
        predicateValue: string | Date
    ) => {
        const resetPredicate = () => {
            this.predicate.forEach((value, key) => {
                if (key !== "startDate") {
                    this.predicate.delete(key);
                }
            });
        };
        switch (predicateKey) {
            case "all":
                resetPredicate();
                this.predicate.set("all", true);
                break;
            case "isGoingFilter":
                resetPredicate();
                this.predicate.set("isGoingFilter", true);
                break;
            case "isHostFilter":
                resetPredicate();
                this.predicate.set("isHostFilter", true);
                break;
            case "startDate":
                this.predicate.delete("startDate");
                this.predicate.set("startDate", predicateValue);
                break;
        }
    };

    private setPagination = (pagingMetaData: PagingMetaData) => {
        this.pagingMetaData = pagingMetaData;
    };

    loadSingleActivity = async (id: string) => {
        let activity = this.getActivity(id);
        if (activity) {
            this.setSelectedActivity(activity);
            return activity;
        } else {
            this.setLoadingInitial(true);
            try {
                activity = await agent.Activities.details(id);
                this.setActivity(activity);
                runInAction(() => this.setSelectedActivity(activity));
                this.setLoadingInitial(false);
                return activity;
            } catch (error) {
                console.log(error);
                this.setLoadingInitial(false);
            }
        }
    };

    private getActivity = (id: string) => {
        return this.activityRegistry.get(id);
    };

    private setLoadingInitial = (state: boolean) => {
        this.loadingInitial = state;
    };

    private setLoading = (state: boolean) => {
        this.loading = state;
    };

    private setEditMode = (state: boolean) => {
        this.editMode = state;
    };

    private setActivity = (activity: Activity) => {
        const user = baseStore.userStore.user;
        if (user) {
            // Check the attendees array to find matching usernames and set isGoing flag to true
            activity.isGoing = activity.attendees!.some(
                (a) => a.username === user.username
            );
            activity.isHost = activity.hostUsername === user.username;
            activity.host = activity.attendees?.find(
                (u) => u.username === activity.hostUsername
            );
        }
        activity.date = new Date(activity.date!);
        this.addActivityToList(activity);
    };

    // Set the updated activity on the screen
    private setSelectedActivity = (activity: Activity | undefined) => {
        this.selectedActivity = activity;
    };

    private addActivityToList = (activity: Activity) => {
        this.activityRegistry.set(activity.id, activity);
    };

    createActivity = async (activityToCreate: ActivityFormValues) => {
        const currentLoggedInUser = baseStore.userStore.user;
        const attendee = new Profile(currentLoggedInUser!);
        try {
            await agent.Activities.create(activityToCreate);
            const newActivity = new Activity(activityToCreate);
            newActivity.hostUsername = currentLoggedInUser!.username;
            newActivity.attendees = [attendee];
            this.setActivity(newActivity);
            runInAction(() => {
                this.setSelectedActivity(newActivity);
            });
        } catch (error) {
            console.log(error);
        }
    };

    updateActivity = async (activityToUpdate: ActivityFormValues) => {
        try {
            await agent.Activities.update(activityToUpdate);
            runInAction(() => {
                if (activityToUpdate.id) {
                    // Use activityToUpdate to overwrite the original activity
                    let updatedActivity = {
                        ...this.getActivity(activityToUpdate.id),
                        ...activityToUpdate,
                    };
                    // Add the updated activity to the list
                    this.activityRegistry.set(
                        activityToUpdate.id,
                        updatedActivity as Activity
                    );
                    this.setSelectedActivity(updatedActivity as Activity);
                }
            });
        } catch (error) {
            console.log(error);
        }
    };

    deleteActivity = async (id: string) => {
        this.setLoading(true);
        try {
            await agent.Activities.delete(id);
            runInAction(() => {
                this.activityRegistry.delete(id);
                this.setLoading(false);
            });
        } catch (error) {
            console.log(error);
            runInAction(() => {
                this.setLoading(false);
            });
        }
    };

    updateAttendance = async () => {
        const currentLoggedInUser = baseStore.userStore.user;
        this.setLoading(true);
        try {
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                // Cancel attendance
                if (this.selectedActivity?.isGoing) {
                    // Filter out the currently logged in user inside the attendee array
                    this.selectedActivity.attendees =
                        this.selectedActivity.attendees?.filter(
                            (aa) =>
                                aa.username !== currentLoggedInUser?.username
                        );
                    this.selectedActivity.isGoing = false;
                }
                // Attend action
                else {
                    const newAttendee = new Profile(currentLoggedInUser!);
                    this.selectedActivity?.attendees?.push(newAttendee);
                    this.selectedActivity!.isGoing = true;
                }
                this.addActivityToList(this.selectedActivity!);
            });
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => this.setLoading(false));
        }
    };

    cancelActivityToggle = async () => {
        this.setLoading(true);
        try {
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                this.selectedActivity!.isCancelled =
                    !this.selectedActivity?.isCancelled;
                this.addActivityToList(this.selectedActivity!);
            });
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => this.setLoading(false));
        }
    };

    clearSelectedActivity = () => {
        this.selectedActivity = undefined;
    };

    updateAttendeeFollowing = (username: string) => {
        this.activityRegistry.forEach((activity) => {
            activity.attendees.forEach((attendee) => {
                if (attendee.username === username) {
                    // Re-calculate the counts
                    attendee.isFollowingThisUser
                        ? attendee.followersCount--
                        : attendee.followersCount++;
                    // Update the flag
                    attendee.isFollowingThisUser =
                        !attendee.isFollowingThisUser;
                }
            });
        });
    };
}
