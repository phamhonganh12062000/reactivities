﻿using Domain.Dtos.Activity;
using Shared.RequestFeatures;
using Domain.Results;
using MediatR;
using Domain.ParameterModels;

namespace Application.Services.Activities.Queries
{
    public sealed record GetAllActivitiesQuery(ActivityParameters ActivityParameters, bool TrackChanges)
        : IRequest<(Result<PagedList<ActivityDto>> pagedActivitiesDto, MetaData metaData)>
    {
    }
}
