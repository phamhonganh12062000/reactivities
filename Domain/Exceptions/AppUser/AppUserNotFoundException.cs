﻿namespace Domain.Exceptions.AppUser
{
  public sealed class AppUserNotFoundException : NotFoundException
    {
        public AppUserNotFoundException(Models.AppUser user)
            : base($"The user with the username: {user.UserName} does not exist in the database.")
        {
        }
    }
}
