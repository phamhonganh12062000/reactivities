using Contracts.Infrastructure;
using Contracts.Logger;
using Domain.ConfigurationModels;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
namespace Infrastructure.Email
{
  public class EmailSmtp : IEmailSenderFactory
  {
    private readonly SmtpConfiguration _smtpConfiguration;
    private readonly ILoggerManager _logger;

    public EmailSmtp(IOptions<SmtpConfiguration> smtpConfiguration, ILoggerManager logger)
    {
      _smtpConfiguration = smtpConfiguration.Value;
      _logger = logger;
    }

    /// <summary>
    /// MailCatcher does not require authentication => null password here
    /// </summary>
    public bool IsValid => !string.IsNullOrEmpty(_smtpConfiguration.MailServer);

    public async Task SendEmailAsync(string email, string subject, string htmlMessage)
    {
      try
      {
        var mimeMessage = new MimeMessage();

        mimeMessage.From.Add(new MailboxAddress(_smtpConfiguration.SenderName, _smtpConfiguration.Sender));

        mimeMessage.To.Add(new MailboxAddress(_smtpConfiguration.SenderName, email));

        mimeMessage.Subject = subject;

        mimeMessage.Body = new TextPart("html")
        {
          Text = htmlMessage,
        };

        using var client = new SmtpClient();

        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

        await client.ConnectAsync(_smtpConfiguration.MailServer, _smtpConfiguration.MailPort, SecureSocketOptions.None);

        await client.SendAsync(mimeMessage);

        await client.DisconnectAsync(true);

        _logger.LogInfo($"Email send to {email} complete.");
      }
      catch (Exception ex)
      {
        _logger.LogError($"SMTP send error: {ex}");
      }
    }
  }
}
