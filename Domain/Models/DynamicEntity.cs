﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Domain.LinkModels;

namespace Domain.Models
{
    /// <summary>
    /// Handle dynamic property access when using the `dynamic` keyword.
    /// </summary>
    public class DynamicEntity : DynamicObject, IXmlSerializable, IDictionary<string, object>
    {
        private readonly string _root = "DynamicEntity";
        private readonly IDictionary<string, object> _expandoObject;

        public DynamicEntity()
        {
            _expandoObject = new ExpandoObject();
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            // Check if the property exists in the ExpandoObject
            if (_expandoObject.TryGetValue(binder.Name, out object value))
            {
                // If it exists, set the result
                result = value;
                return true;
            }

            // If not, let the base class handle property access
            return base.TryGetMember(binder, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _expandoObject[binder.Name] = value;
            return true;
        }

        public object this[string key]
        {
            get => _expandoObject[key];
            set => _expandoObject[key] = value;
        }

        public ICollection<string> Keys => _expandoObject.Keys;

        public ICollection<object> Values => _expandoObject.Values;

        public int Count => _expandoObject.Count();

        public bool IsReadOnly => _expandoObject.IsReadOnly;

        public void Add(string key, object value)
        {
            _expandoObject.Add(key, value);
        }

        public void Add(KeyValuePair<string, object> item)
        {
            _expandoObject.Add(item);
        }

        public void Clear()
        {
            _expandoObject.Clear();
        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return _expandoObject.Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return _expandoObject.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            _expandoObject.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _expandoObject.GetEnumerator();
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read the XML docs and populate the ExpandoObject dynamically.
        /// Example of a XML element:
        ///  <root>
        ///     <intElement type="System.Int32">42</intElement>
        ///     <stringElement type="System.String">Hello, World!</stringElement>
        /// </root>
        /// </summary>
        /// <param name="reader"></param>
        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement(_root);

            while (!reader.Name.Equals(_root))
            {
                // Represent the `type` attribute pf the XML element being processed
                string typeContent;

                // Store the .NET `Type` based on the `type` attribute of XML docs
                Type underlyingType;

                // Represent name of the current XML element being processed
                var name = reader.Name;

                reader.MoveToAttribute("type");
                typeContent = reader.ReadContentAsString();
                underlyingType = Type.GetType(typeContent);

                // Move the reader back to the content of the current element
                reader.MoveToContent();

                // Read the element content as a value of a data specified by `underlyingType` variable
                // Then assign the read element content to the `_expando` object with the property name `name` as the key
                _expandoObject[name] = reader.ReadElementContentAs(underlyingType, null);
            }
        }

        public bool Remove(string key)
        {
            return _expandoObject.Remove(key);
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return _expandoObject.Remove(item);
        }

        public bool TryGetValue(string key, [MaybeNullWhen(false)] out object value)
        {
            return _expandoObject.TryGetValue(key, out value);
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var key in _expandoObject.Keys)
            {
                var value = _expandoObject[key];
                WriteLinksToXml(key, value, writer);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void WriteLinksToXml(string key, object value, XmlWriter writer)
        {
            writer.WriteStartElement(key);

            if (value.GetType() == typeof(List<Link>))
            {
                foreach (var val in value as List<Link>)
                {
                    writer.WriteStartElement(nameof(Link));

                    // Recursion
                    WriteLinksToXml(nameof(val.Href), val.Href, writer);
                    WriteLinksToXml(nameof(val.Rel), val.Rel, writer);
                    WriteLinksToXml(nameof(val.Method), val.Method, writer);
                    writer.WriteEndElement();
                }
            }
            else
            {
                writer.WriteString(value.ToString());
            }

            writer.WriteEndElement();
        }
    }

    public class ShapedDynamicEntity
    {
        public ShapedDynamicEntity()
        {
            DynamicEntity = new DynamicEntity();
        }

        public Guid Id { get; set; }

        public DynamicEntity DynamicEntity { get; set; }
    }
}
