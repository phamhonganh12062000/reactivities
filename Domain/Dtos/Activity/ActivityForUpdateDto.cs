﻿namespace Domain.Dtos.Activity
{
    public record ActivityForUpdateDto : ActivityForManipulationDto
    {
    }
}
