﻿using Application.Caching;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Exceptions.AppUser;
using Domain.Models;
using Domain.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Followers.Handlers
{
  public sealed record ToggleFollowingCommand(string TargetUsername, bool UserTrackChanges)
      : IRequest<Result<Unit>>
  {
  }

  public sealed class ToggleFollowingHandler
      : IRequestHandler<ToggleFollowingCommand, Result<Unit>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUserAccessor _userAccessor;
    private readonly ILoggerManager _logger;
    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public ToggleFollowingHandler(IUnitOfWork unitOfWork, IUserAccessor userAccessor, ILoggerManager logger, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _userAccessor = userAccessor;
      _logger = logger;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Unit>> Handle(ToggleFollowingCommand request, CancellationToken cancellationToken)
    {
      // Remove the following caching of the observer
      var observerUsername = _userAccessor.GetCurrentUserUsername();

      var observerCacheKey = CachingStatics.GetAppUserByUsername(observerUsername);

      _memoryCacheWrapper.Remove(observerCacheKey);

      var observer = await _unitOfWork.AppUser.GetAppUserAsync(observerUsername,
          trackChanges: request.UserTrackChanges);

      // Remove the follower caching of the target
      var targetCacheKey = CachingStatics.GetAppUserByUsername(request.TargetUsername);

      _memoryCacheWrapper.Remove(targetCacheKey);

      var target = await _unitOfWork.AppUser.GetAppUserAsync(request.TargetUsername,
          trackChanges: request.UserTrackChanges);

      if (target is null)
      {
        throw new AppUserNotFoundException(target);
      }

      var following = await _unitOfWork.UserFollowing.GetUserFollowingAsync(observer.Id, target.Id, request.UserTrackChanges);

      if (following is null)
      {
        following = new UserFollowing
        {
          Observer = observer,
          Target = target,
        };

        await _unitOfWork.UserFollowing.AddFollowingAsync(following);
      }
      else
      {
        _unitOfWork.UserFollowing.RemoveFollowing(following);
      }

      var isSavedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSavedSuccess)
      {
        _logger.LogError("Failed to update following");
        return Result<Unit>.Failure("Failed to update following");
      }

      return Result<Unit>.Success(Unit.Value);

    }
  }
}
