using Domain.Models;

namespace Domain.LinkModels
{
    public class LinkResponse
    {
        public bool HasLinks { get; set; }

        public DynamicEntity ShapedEntity { get; set; }

        // If the response does NOT have links => ShapedEntities
        public List<DynamicEntity> ShapedEntities { get; set; }

        // If the response has links => LinkedEntities
        public LinkWrapper<DynamicEntity> LinkedEntities { get; set; }

        public LinkResponse()
        {
            LinkedEntities = new LinkWrapper<DynamicEntity>();
            ShapedEntities = new List<DynamicEntity>();
            ShapedEntity = new DynamicEntity();
        }
    }
}
