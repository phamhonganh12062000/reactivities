# Setup GitLab CICD Pipeline

## .YML file

- Create a .yml file at `${root}/.gitlab/workflows/.gitlab-ci.yml`
- [Example templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates)

## CI/CD configuration on GitLab

- Setup CI/CD configuration file: Go to Settings > CI/CD > General pipelines > CI/CD configuration file and add the path to the file from the root folder e.g., `.gitlab/workflows/.gitlab-ci.yml`

- Environment variables: Add key-value variables with tags `Protected` + `Expanded`

- Protect default branch: Go to Settings > Repository > Protected branches and set the protected branch for the environment variables to apply

- Make green pipeline as a required approval for merge request: Go to Settings > Merge requests > Merge checks and tick on the "Pipelines must succeed" option

## Placeholder

