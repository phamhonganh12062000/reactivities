using Shared.RequestFeatures;

namespace Domain.ParameterModels
{
    public class AppUserParameters : RequestParameters
    {
        public string SearchTerm { get; set; }

        public AppUserParameters()
        {
            OrderBy = "username";
        }
    }
}
