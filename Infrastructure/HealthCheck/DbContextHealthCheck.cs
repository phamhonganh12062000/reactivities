using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Infrastructure.HealthCheck
{
  public class DbContextHealthCheck<TContext> : IHealthCheck
    where TContext : DbContext
  {
    private readonly TContext _dbContext;

    public DbContextHealthCheck(TContext dbContext)
    {
      _dbContext = dbContext;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
      ArgumentNullException.ThrowIfNull(context);

      if (await _dbContext.Database.CanConnectAsync(cancellationToken))
      {
        return HealthCheckResult.Healthy();
      }

      return HealthCheckResult.Unhealthy();
    }
  }
}
