namespace Domain.ConfigurationModels
{
  public class AzureConfiguration
  {
    public string AzureKeyVaultName { get; set; }

    public string AzureManagedIdentityApplicationId { get; set; }

    public string AzureAppConfigurationConnectionString { get; set; }

    public string AzureTenantId { get; set; }

    public string AzureClientId { get; set; }

    public string AzureClientSecret { get; set; }

  }
}
