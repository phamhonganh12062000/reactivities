﻿using Domain.Dtos.Activity;
using FluentValidation;

namespace Application.Services.Activities.Validators
{
    public sealed class ActivityValidator : AbstractValidator<ActivityForManipulationDto>
    {
        public ActivityValidator()
        {
            RuleFor(a => a.Title).NotEmpty();
            RuleFor(a => a.Description).NotEmpty();
            RuleFor(a => a.Date).NotEmpty();
            RuleFor(a => a.Category).NotEmpty();
            RuleFor(a => a.City).NotEmpty();
            RuleFor(a => a.Venue).NotEmpty();
        }
    }
}
