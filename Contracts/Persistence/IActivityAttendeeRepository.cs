﻿using Domain.Models;

namespace Contracts.Persistence
{
    public interface IActivityAttendeeRepository
    {
        Task<ActivityAttendee> GetActivityHostAsync(Activity activity, bool trackChanges);

        IQueryable<ActivityAttendee> GetUserActivities(string username, bool trackChanges);

        Task<ActivityAttendee> GetActivityAttendeeByUsernameAsync(Activity activity, AppUser user, bool trackChanges);

        Task<ActivityAttendee> GetActivityAttendeeByIdAsync(Guid activityId, string userId, bool trackChanges);
    }
}
