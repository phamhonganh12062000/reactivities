using Application.Caching;
using AutoMapper;
using Contracts.Extensions;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Dtos.AppUser;
using Domain.Models;
using Domain.Results;
using MediatR;

namespace Application.Services.AppUsers.Handlers
{
  public sealed record UpdateAppUserInfoCommand(ProfileForUpdateDto ProfileForUpdateDto, bool TrackChanges) : IRequest<Result<Unit>>
  {
  }

  public sealed class UpdateAppUserProfileHandler : IRequestHandler<UpdateAppUserInfoCommand, Result<Unit>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUserAccessor _userAccessor;
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;
    private readonly IDataShaper<ProfileDto> _dataShaper;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public UpdateAppUserProfileHandler(
        IUnitOfWork unitOfWork,
        IUserAccessor userAccessor,
        ILoggerManager logger,
        IMapper mapper,
        IDataShaper<ProfileDto> dataShaper,
        IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _userAccessor = userAccessor;
      _logger = logger;
      _mapper = mapper;
      _dataShaper = dataShaper;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Unit>> Handle(UpdateAppUserInfoCommand request, CancellationToken cancellationToken)
    {
      var currentUserUsername = _userAccessor.GetCurrentUserUsername();

      _memoryCacheWrapper.Remove(CachingStatics.GetAppUserByUsername(currentUserUsername));

      var currentLoggedInUser = await _unitOfWork.AppUser.GetAppUserAsync(currentUserUsername,
          trackChanges: request.TrackChanges);

      var profileToUpdate = _mapper.Map<AppUser>(request.ProfileForUpdateDto);

      currentLoggedInUser.Bio = profileToUpdate.Bio ?? currentLoggedInUser.Bio;
      currentLoggedInUser.DisplayName = profileToUpdate.DisplayName ?? currentLoggedInUser.DisplayName;

      var isSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSuccess)
      {
        _logger.LogError("Problem updating profile");
        return Result<Unit>.Failure("Problem updating profile");
      }

      // var shapedDto = _dataShaper.ShapeDataSingleEntity(_mapper.Map<ProfileDto>(currentLoggedInUser),
      //     string.Join(",", nameof(ProfileDto.DisplayName), nameof(ProfileDto.Bio)));

      return Result<Unit>.Success(Unit.Value);
    }
  }
}
