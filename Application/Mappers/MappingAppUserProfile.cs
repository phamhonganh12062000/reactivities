﻿using AutoMapper;
using Contracts.Infrastructure;
using Domain.Dtos.AppUser;
using Domain.Dtos.Authentication;
using Domain.Models;

namespace Application.Mappers
{
    public class MappingAppUserProfile : Profile
    {
        public MappingAppUserProfile()
        {
            CreateMap<AppUser, ProfileDto>()
                .ForMember(d => d.Image, o => o.MapFrom(s => s.Photos.FirstOrDefault(p => p.IsMain).Url))
                .ForMember(d => d.FollowersCount, o => o.MapFrom(s => s.Followers.Count))
                .ForMember(d => d.FollowingCount, o => o.MapFrom(s => s.Followings.Count))
                .ForMember(d => d.IsFollowingThisUser, o => o.MapFrom<AppUserToProfileDtoResolver>());

            CreateMap<ProfileForUpdateDto, AppUser>();

            CreateMap<RegistrationDto, AppUser>();
        }
    }

    /// <summary>
    /// Custom resolver logic instead of using ProjectTo().
    /// Check if the current user is following any of his/her followers.
    /// </summary>
    public class AppUserToProfileDtoResolver : IValueResolver<AppUser, ProfileDto, bool>
    {
        private readonly IUserAccessor _userAccessor;

        public AppUserToProfileDtoResolver(IUserAccessor userAccessor)
        {
            _userAccessor = userAccessor;
        }

        public bool Resolve(AppUser source, ProfileDto destination, bool destMember, ResolutionContext context)
        {
            var currentUsername = _userAccessor.GetCurrentUserUsername();

            return source.Followers.Any(x => x.Observer.UserName.Equals(currentUsername));
        }
    }
}
