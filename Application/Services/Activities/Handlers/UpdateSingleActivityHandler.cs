﻿using Application.Caching;
using Application.Services.Activities.Commands;
using AutoMapper;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Exceptions.Activities;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Handlers
{
  internal sealed class UpdateSingleActivityHandler : IRequestHandler<UpdateSingleActivityCommand, Result<Unit>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public UpdateSingleActivityHandler(IUnitOfWork unitOfWork, IMapper mapper, ILoggerManager logger, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _logger = logger;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Unit>> Handle(UpdateSingleActivityCommand request, CancellationToken cancellationToken)
    {
      _memoryCacheWrapper.Remove(CachingStatics.GetActivityById(request.Id));

      var activity = await _unitOfWork.Activity.GetSingleActivityAsync(request.Id, request.TrackChanges) ?? throw new ActivityNotFoundException(request.Id);

      _mapper.Map(request.ActivityForUpdateDto, activity);

      var isUpdatedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isUpdatedSuccess)
      {
        _logger.LogError("Failed to update activity");
        return Result<Unit>.Failure("Failed to update activity");
      }

      return Result<Unit>.Success(Unit.Value);
    }
  }
}
