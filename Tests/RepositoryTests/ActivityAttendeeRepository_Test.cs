﻿using Contracts.Persistence;
using Domain.Models;
using Moq;

namespace Tests.RepositoryTests
{
    public class ActivityAttendeeRepository_Test
    {
        private readonly Mock<IActivityAttendeeRepository> _mockActivityAttendeeRepo;

        public ActivityAttendeeRepository_Test()
        {
            _mockActivityAttendeeRepo = new Mock<IActivityAttendeeRepository>();
        }

        [Fact]
        public async Task GetUserActivities_GetAllActivitiesOfSingleAttendee_ReturnQueryableListOfAttendees()
        {
            // ARRANGE
            var attendeeList = Enumerable.Empty<ActivityAttendee>().AsQueryable();

            _mockActivityAttendeeRepo.Setup(repo => repo.GetUserActivities("TestUsername", false)).Returns(attendeeList);

            // ACT
            var result = _mockActivityAttendeeRepo.Object.GetUserActivities("TestUsername", false);

            // ASSERT
            Assert.IsAssignableFrom<IQueryable<ActivityAttendee>>(result);
        }
    }
}
