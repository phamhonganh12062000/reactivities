using Domain.LinkModels;
using Domain.Models;
using Microsoft.AspNetCore.Http;

namespace Contracts.Domain
{
    public interface IAppUserLinks
    {
        LinkResponse TryGenerateLinksForMultipleUsers(IEnumerable<ProfileDto> appUsersDto, string fields, HttpContext httpContext);

        LinkResponse TryGenerateLinksForSingleUser(ProfileDto appUserDto, string fields, HttpContext httpContext);
    }
}
