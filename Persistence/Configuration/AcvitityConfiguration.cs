﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Static;

namespace Persistence.Configuration
{
    public class AcvitityConfiguration : IEntityTypeConfiguration<Activity>
    {
        /// <summary>
        /// Seed data AFTER EF Core 2.1.
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Activity> builder)
        {

            builder.HasData(
            new Activity
            {
                Id = SeedData.Activities.PastActivity1,
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            },
            new Activity
            {
                Id = SeedData.Activities.PastActivity2,
                Title = "Past Activity 2",
                Date = DateTime.UtcNow.AddMonths(-1),
                Description = "Activity 1 month ago",
                Category = "food",
                City = "Hanoi",
                Venue = "Restaurant",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity1,
                Title = "Future Activity 1",
                Date = DateTime.UtcNow.AddMonths(1),
                Description = "Activity 1 month next",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity2,
                Title = "Future Activity 2",
                Date = DateTime.UtcNow.AddMonths(2),
                Description = "Activity 2 months next",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity3,
                Title = "Future Activity 3",
                Date = DateTime.UtcNow.AddMonths(3),
                Description = "Activity 3 months next",
                Category = "food",
                City = "Hanoi",
                Venue = "Restaurant",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity4,
                Title = "Future Activity 4",
                Date = DateTime.UtcNow.AddMonths(4),
                Description = "Activity 4 months in future",
                Category = "culture",
                City = "London",
                Venue = "British Museum",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity5,
                Title = "Future Activity 5",
                Date = DateTime.UtcNow.AddMonths(5),
                Description = "Activity 5 months in future",
                Category = "drinks",
                City = "London",
                Venue = "Punch and Judy",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity6,
                Title = "Future Activity 6",
                Date = DateTime.UtcNow.AddMonths(6),
                Description = "Activity 6 months in future",
                Category = "music",
                City = "London",
                Venue = "O2 Arena",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity7,
                Title = "Future Activity 7",
                Date = DateTime.UtcNow.AddMonths(7),
                Description = "Activity 7 months in future",
                Category = "travel",
                City = "Berlin",
                Venue = "All",
            },
            new Activity
            {
                Id = SeedData.Activities.FutureActivity8,
                Title = "Future Activity 8",
                Date = DateTime.UtcNow.AddMonths(8),
                Description = "Activity 8 months in future",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            });
        }
    }
}
