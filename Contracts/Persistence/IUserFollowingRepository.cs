﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Persistence
{
    public interface IUserFollowingRepository
    {
        Task<UserFollowing> GetUserFollowingAsync(string observerId, string targetId, bool trackChanges);

        /// <summary>
        /// Go through the Target column to find the user who is being followed.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="trackChanges"></param>
        /// <returns>The list of users following the queried user.</returns>
        Task<List<AppUser>> GetUserFollowerListAsync(string username, bool trackChanges);

        /// <summary>
        /// Go through the Observer column to find the user who is following other users.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="trackChanges"></param>
        /// <returns>The list of users the queried user is following.</returns>
        Task<List<AppUser>> GetUserFollowingListAsync(string username, bool trackChanges);

        Task AddFollowingAsync(UserFollowing userFollowing);

        void RemoveFollowing(UserFollowing userFollowing);
    }
}
