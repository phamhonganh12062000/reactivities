using AutoMapper;
using Contracts.Application.Services;
using Contracts.Logger;
using Domain.ConfigurationModels;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Application.Services.Authentication
{
  internal class FacebookLoginService : IFacebookLoginService
  {
    private readonly ILoggerManager _logger;
    private readonly IMapper _mapper;
    private readonly UserManager<AppUser> _userManager;
    private readonly IOptions<FacebookConfiguration> _facebookConfigs;
    private readonly HttpClient _httpClient;

    private AppUser? _user;

    public FacebookLoginService(ILoggerManager logger, IMapper mapper, UserManager<AppUser> userManager, IOptions<FacebookConfiguration> facebookConfigs, HttpClient httpClient)
    {
      _logger = logger;
      _mapper = mapper;
      _userManager = userManager;
      _facebookConfigs = facebookConfigs;
      _httpClient = httpClient;
      _httpClient.BaseAddress = new Uri("https://graph.facebook.com");
    }

    public async Task<(IdentityResult, AppUser)> FacebookLogin(string accessToken)
    {
      var fbVerifyKeys = _facebookConfigs.Value.AppId + "|" + _facebookConfigs.Value.AppSecret;

      var verifyToken = await _httpClient.GetAsync($"debug_token?input_token={accessToken}&access_token={fbVerifyKeys}");

      if (!verifyToken.IsSuccessStatusCode)
      {
        throw new UnauthorizedAccessException("Invalid Facebook token");
      }

      var fbUrl = $"me?access_token={accessToken}&fields=name,email,picture.width(100).height(100)";

      var response = await _httpClient.GetAsync(fbUrl);

      if (!response.IsSuccessStatusCode)
      {
        throw new UnauthorizedAccessException("Invalid URL");
      }

      var fbInfo = JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());

      var username = (string)fbInfo.id;

      var user = await _userManager.Users.Include(p => p.Photos).FirstOrDefaultAsync(x => x.UserName == username);

      if (user != null)
      {
        return (IdentityResult.Success, user);
      }

      user = new AppUser
      {
        DisplayName = (string)fbInfo.name,
        Email = (string)fbInfo.email,
        UserName = (string)fbInfo.id,
        Photos = new List<Photo>
        {
            new ()
            {
                Id = "fb_" + (string)fbInfo.id,
                Url = (string)fbInfo.picture.data.url,
                IsMain = true,
            },
        },
        EmailConfirmed = true,
      };

      var result = await _userManager.CreateAsync(user);

      return (result, user);
    }
  }
}
