import { useField } from "formik";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import { Form, Label } from "semantic-ui-react";

// Reusable text input
export default function CustomDateInput(props: Partial<ReactDatePickerProps>) {
    // field contains props + even handlers,
    // while meta contains metadata about the field e.g., touch state, validaion errors
    const [field, meta, helpers] = useField(props.name!);

    return (
        // Form.Field wraps the label + input element
        // Cast the error into a boolean => Display error if field touched & has an error
        <Form.Field error={meta.touched && !!meta.error}>
            <DatePicker
                {...field}
                {...props}
                selected={(field.value && new Date(field.value)) || null}
                onChange={(value) => helpers.setValue(value)}
            />
            {meta.touched && meta.error ? (
                <Label basic color="red">
                    {meta.error}
                </Label>
            ) : null}
        </Form.Field>
    );
}
