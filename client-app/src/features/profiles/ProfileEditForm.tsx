import { Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { Button } from "semantic-ui-react";
import * as Yup from "yup";
import CustomTextArea from "../../app/common/form/CustomTextArea";
import CustomTextInput from "../../app/common/form/CustomTextInput";
import { Profile } from "../../app/models/profile";
import { useStore } from "../../app/stores/baseStore";

interface Props {
    setEditMode: (editMode: boolean) => void;
}

export default observer(function ProfileEditForm({ setEditMode }: Props) {
    const {
        profileStore: { profile, updateProfile },
    } = useStore();

    function handleUpdateProfile(updateValues: Partial<Profile>) {
        updateProfile(updateValues).then(() => {
            setEditMode(false);
        });
    }

    const profileValidation = Yup.object({
        displayName: Yup.string().required(),
    });

    return (
        <Formik
            initialValues={{
                displayName: profile?.displayName,
                bio: profile?.bio,
            }}
            onSubmit={(values) => handleUpdateProfile(values)}
            validationSchema={profileValidation}
        >
            {({ isSubmitting, isValid, dirty }) => (
                <Form className="ui form">
                    <CustomTextInput
                        placeholder="Display Name"
                        name="displayName"
                    />
                    <CustomTextArea
                        rows={3}
                        placeholder="Add your bio"
                        name="bio"
                    />
                    <Button
                        positive
                        type="submit"
                        loading={isSubmitting}
                        content="Update Profile"
                        floated="right"
                        disabled={!isValid || !dirty}
                    />
                </Form>
            )}
        </Formik>
    );
});
