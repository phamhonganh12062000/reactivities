using Domain.Models;
using Domain.Results;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Services.Photos.Commands
{
    public sealed record AddPhotoCommand(IFormFile File, bool UserTrackChanges) : IRequest<Result<Photo>>
    {
    }
}
