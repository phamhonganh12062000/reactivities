using Domain.Results;
using MediatR;

namespace Application.Services.Photos.Commands
{
    public sealed record SetMainPhotoCommand(string Id, bool UserTrackChanges) : IRequest<Result<Unit>>
    {
    }
}
