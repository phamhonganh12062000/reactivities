using AutoMapper;
using Domain.Dtos.Comment;
using Domain.Models;

namespace Application.Mappers
{
    public class MappingCommentProfile : Profile
    {
        public MappingCommentProfile()
        {
            CreateMap<Comment, CommentDto>()
                .ForMember(d => d.DisplayName, o => o.MapFrom(s => s.Author.DisplayName))
                .ForMember(d => d.Username, o => o.MapFrom(s => s.Author.UserName))
                .ForMember(d => d.Image, o => o.MapFrom(s => s.Author.Photos.FirstOrDefault(p => p.IsMain).Url));
        }
    }
}
