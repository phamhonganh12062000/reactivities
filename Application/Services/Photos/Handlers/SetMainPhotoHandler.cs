using Application.Caching;
using Application.Services.Photos.Commands;
using Contracts.Infrastructure;
using Contracts.Persistence;
using Domain.Exceptions.Photos;
using Domain.Results;
using MediatR;

namespace Application.Services.Photos.Handlers
{
  public class SetMainPhotoHandler : IRequestHandler<SetMainPhotoCommand, Result<Unit>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUserAccessor _userAccessor;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public SetMainPhotoHandler(IUserAccessor userAccessor, IUnitOfWork unitOfWork, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _userAccessor = userAccessor;
      _unitOfWork = unitOfWork;
      _memoryCacheWrapper = memoryCacheWrapper;

    }

    public async Task<Result<Unit>> Handle(SetMainPhotoCommand request, CancellationToken cancellationToken)
    {
      var currentUserUsername = _userAccessor.GetCurrentUserUsername();

      _memoryCacheWrapper.Remove(CachingStatics.GetAppUserByUsername(currentUserUsername));

      var user = await _unitOfWork.AppUser.GetAppUserAsync(currentUserUsername, request.UserTrackChanges);

      var photo = user.Photos.FirstOrDefault(p => p.Id == request.Id);

      if (photo is null)
      {
        throw new PhotoNotFoundException(photo);
      }

      var currentMainPhoto = user.Photos.FirstOrDefault(p => p.IsMain);

      // Set flag for the current photo as not main
      if (currentMainPhoto != null)
      {
        currentMainPhoto.IsMain = false;
      }

      photo.IsMain = true;

      var isSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSuccess)
      {
        return Result<Unit>.Failure("Problem setting main photo");
      }

      return Result<Unit>.Success(Unit.Value);
    }

  }
}
