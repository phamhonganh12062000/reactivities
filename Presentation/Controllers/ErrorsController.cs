﻿using Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    public class ErrorsController : BaseController
    {
        [HttpGet("not-found")]
        public IActionResult GetNotFound()
        {
            throw new NotFoundException("Not Found");
        }

        [HttpGet("bad-request")]
        public IActionResult GetBadRequest()
        {
            throw new BadRequestException("Bad Request");
        }

        [HttpGet("server-error")]
        public IActionResult GetServerError()
        {
            throw new Exception("Internal Server Error");
        }

        [HttpGet("unauthorized")]
        public IActionResult GetUnauthorized()
        {
            return Unauthorized();
        }
    }
}
