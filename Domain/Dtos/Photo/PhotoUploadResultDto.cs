namespace Domain.Dtos.Photo
{
    public record PhotoUploadResultDto
    {
        public string PublicId { get; init; }

        public string Url { get; init; }
    }
}
