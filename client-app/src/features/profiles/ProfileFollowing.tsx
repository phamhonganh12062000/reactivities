import { observer } from "mobx-react-lite";
import { Card, Grid, Header, Tab } from "semantic-ui-react";
import { useStore } from "../../app/stores/baseStore";
import ProfileCard from "./ProfileCard";

export default observer(function ProfileFollowing() {
    const { profileStore } = useStore();
    const { profile, followings, loadingFollowingsFlag, activeTabIndex } =
        profileStore;

    return (
        <Tab.Pane loading={loadingFollowingsFlag}>
            <Grid>
                <Grid.Column width={16}>
                    <Header
                        floated="left"
                        icon="user"
                        content={
                            activeTabIndex === 3
                                ? `People following ${profile?.displayName}`
                                : `People ${profile?.displayName} is following`
                        }
                    />
                </Grid.Column>
                <Grid.Column width={16}>
                    <Card.Group itemsPerRow={5}>
                        {followings.map((profile) => (
                            <ProfileCard
                                key={profile.username}
                                profile={profile}
                            />
                        ))}
                    </Card.Group>
                </Grid.Column>
            </Grid>
        </Tab.Pane>
    );
});
