﻿namespace Contracts.Application.Services
{
  public interface IServiceManager
  {
    IAuthenticationService AuthenticationService { get; }

    IFacebookLoginService FacebookLoginService { get; }

    ITokenService TokenService { get; }

    IEmailConfirmationService EmailConfirmationService { get; }
  }
}
