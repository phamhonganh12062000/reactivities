using Contracts.Infrastructure;
using Contracts.Logger;
using Domain.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Infrastructure.Email
{
  public class EmailSenderFactory : IEmailSenderFactory
  {
    private readonly IEmailSenderFactory _senderInstance;
    private readonly ILoggerManager _logger;

    public EmailSenderFactory(IConfiguration configuration, IOptions<SmtpConfiguration> smtpConfiguration,  IOptions<PostmarkConfiguration> postmarkConfiguration, ILoggerManager logger)
    {
      _senderInstance = null;
      _logger = logger;

      var doUseSmtp = bool.Parse(configuration["UseSmtp"]);

      if (doUseSmtp)
      {
        var smtp = new EmailSmtp(smtpConfiguration, logger);

        if (smtp.IsValid)
        {
          _senderInstance = smtp;
        }
        else
        {
          _logger.LogError("SMTP email provider selected but no valid SMTP settings configured.");
        }
      }
      else
      {
        var postmark = new EmailPostmark(postmarkConfiguration, smtpConfiguration, logger);

        if (postmark.IsValid)
        {
        _senderInstance = postmark;
        }
        else
        {
          _logger.LogError("Postmark email provider selected but no valid server token settings configured.");
        }
      }
    }

    public async Task SendEmailAsync(string email, string subject, string htmlMessage)
    {
      if (_senderInstance != null)
      {
        await _senderInstance.SendEmailAsync(email, subject, htmlMessage);
      }
    }
  }
}
