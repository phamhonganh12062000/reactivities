using Application.Caching;
using AutoMapper;
using Contracts.Domain;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Exceptions.AppUser;
using Domain.LinkModels;
using Domain.Models;
using Domain.Results;
using MediatR;
using Microsoft.Extensions.Caching.Memory;

namespace Application.Services.ActivityAttendees.Handlers
{
  public sealed record GetAppUserQuery(string Username, bool TrackChanges) : IRequest<Result<ProfileDto>>
  {
  }

  public sealed class GetAppUserHandler : IRequestHandler<GetAppUserQuery, Result<ProfileDto>>
  {
    private readonly IUnitOfWork _unitOfWork;

    private readonly IMapper _mapper;
    private readonly IAppUserLinks _appUserLinks;
    private readonly ILoggerManager _logger;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public GetAppUserHandler(IUnitOfWork unitOfWork, IMapper mapper, IAppUserLinks appUserLinks, ILoggerManager logger, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _appUserLinks = appUserLinks;
      _logger = logger;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<ProfileDto>> Handle(GetAppUserQuery request, CancellationToken cancellationToken)
    {
      // if (!request.AppUserLinkParameters.AppUserParameters.)

      var userCacheKey = CachingStatics.GetAppUserByUsername(request.Username);

      var user = await _memoryCacheWrapper.GetOrCreateAsync(userCacheKey, () => _unitOfWork.AppUser.GetAppUserAsync(request.Username, request.TrackChanges), null);

      if (user is null)
      {
        throw new AppUserNotFoundException(user);
      }

      var userProfileDto = _mapper.Map<ProfileDto>(user);

      // _logger.LogError(userProfileDto.FollowersCount.ToString());

      // var userProfileWithLinks = _appUserLinks.TryGenerateLinksForSingleUser(userProfileDto, request.AppUserLinkParameters.AppUserParameters.Fields, request.AppUserLinkParameters.Context);

      return Result<ProfileDto>.Success(userProfileDto);
    }
  }
}
