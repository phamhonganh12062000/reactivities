# Switching to Postgres

## Why switching?

-   Learning opportunities
-   Preparation for deployment with a more roburst and flexible database
-   Ability to dockerize the whole application

---

## The Process

-   [x] Install Docker Engine on WSL2 (without Docker Desktop) - Refer to `Deploying_On_Docker.md`
-   [x] Containerize Postgres for both development and production environments
-   [x] Refactor the codebase for the migraiton to Postgres
-   [ ] Configure Serilog for different environments (ONGOING - Bugs still present)

---

## Containerize Postgres for both Development and Production environments

-   Create Postgres containers then run

```bash
# NOTE: Adjust the `{environment}` accordingly.
docker run --name {environment} -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=secret -p 5432:5432 -d postgres:latest
```

-   Change `DefaultConnection` on `appsettings.{Environment}.json` to

```json
{
  // NOTE: Adjust the `{environment}` accordingly.
  "DefaultConnection: Server=localhost; Port=5432; User Id=admin; Password=secret; Database=reactivities_{environment}; Include Error Detail=true;
}
```

-   Drop the SQLite database

```bash
dotnet ef database drop --connection "DataSource=../Database/reactivities.db" -p .\Persistence\ -s .\API\
```

-   Delete the `Migrations` folder inside the `Persistence` project
-   Add a new migration

```bash
dotnet ef migrations add SwitchDevDbToPostgres -p .\Persistence\ -s .\API\
```

-   Update `launchSettings.json`

```json
"profiles": {
        "API (Development)": {
            "commandName": "Project",
            "dotnetRunMessages": true,
            "launchBrowser": true,
            "launchUrl": "swagger",
            "applicationUrl": "http://localhost:5000",
            "environmentVariables": {
                "ASPNETCORE_ENVIRONMENT": "Development"
            }
        },
        "API (Production)": {
            "commandName": "Project",
            "dotnetRunMessages": true,
            "launchBrowser": true,
            "launchUrl": "swagger",
            "applicationUrl": "http://localhost:5000",
            "environmentVariables": {
                "ASPNETCORE_ENVIRONMENT": "Production"
            }
        }
    }
```

---

## Refactor the codebase for the migraiton to Postgres

### `DataContextFactory.cs` && `Program.cs`

-   Add `appsettings.{Environment}.json`

```csharp
// For Program.cs
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";
...
.AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
// For DataContextFactory.cs
private string _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
...
.AddJsonFile($"appsettings.{_environment}.json", optional: true)
```

### `Configurations.cs`

-   Change `ConfigureSqlContext()` to using Postgres per environment

```csharp
public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
{
  if (environment.IsDevelopment())
  {
    services.AddDbContext<DataContext>(options
        => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));
  }
  else if (environment.IsProduction())
  {
    ...
  }
}

```

-   Set environment variable to Production

```powershell
# NOTE: This only applies to the currently running terminal session
$env:ASPNETCORE_ENVIRONMENT = "Staging"
```

-   Change `ConfigureJWT()` to get the JWT Secret Key per environment

```csharp
public static void ConfigureJWT(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
{
  ...
  // The same code
  if (environment.IsStaging())
  {
    secretKey = Environment.GetEnvironmentVariable("JWTSecretKey");
  }
  else if (environment.IsDevelopment())
  {
    // NOTE: Only work on Development environment
    secretKey = configuration["JWTSecretKey"];
  }
  // The same code
  ...
}
```

### `DatabaseSeeder.cs`

-   Remove the enable & disable key constraints (Previously added for SQLite)

```csharp
await dataContext.Database.ExecuteSqlRawAsync("PRAGMA foreign_keys=OFF;"); // Remove
await dataContext.Database.ExecuteSqlRawAsync("PRAGMA foreign_keys=ON;"); // Remove
```

### `commentStore.ts` inside `client-app`

-   Remove `"+Z"` so that it can format the date properly

---

## Configure Serilog for different environments

-   Add a check to verify whether `/API/logs` folder has been created inside the `ConfigureSerilog()` function inside `Hosting.cs`

```csharp
var logsDirectory = "logs";

if (!Directory.Exists(logsDirectory))
{
  try
  {
    Directory.CreateDirectory(logsDirectory);
  }
  catch (Exception ex)
  {
    // NOTE: Here I use a bootstrap logger
    logger.Error($"Error creating logs directory: {ex.Message}");
  }
}

```

-   Move the content from `logsettings.json` to `appsettings.json`

### Development environment

-   Change minimum logging level to `Debug` and configure console logging only

```json
"MinimumLevel": {
    "Default": "Debug",
    "Override": {
        "Microsoft.AspNetCore": "Information",
        "System": "Information"
    }
}
...
"WriteTo": [
    {
        "Name": "Console",
        "Args": {
            "theme": "Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme::Literate, Serilog.Sinks.Console",
            "formatter": "Serilog.Formatting.Compact.CompactJsonFormatter, Serilog.Formatting.Compact",
            "outputTemplate": "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] {Message:lj}{NewLine}{Exception}"
        }
    }
],

```

### Staging environment

-   Change minimum logging level to `Information` and configure file logging only

```json
"MinimumLevel": {
    "Default": "Information",
    "Override": {
        "Microsoft.AspNetCore": "Information",
        "System": "Information"
    }
}
...
"WriteTo": [
    {
        "Name": "File",
        "Args": {
            "path": "logs/log.log",
            "rollingInterval": "Day",
            "rollOnFileSizeLimit": true,
            "fileSizeLimitBytes": "10485760",
            "outputTemplate": "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
            "formatter": "Serilog.Formatting.Compact.CompactJsonFormatter, Serilog.Formatting.Compact"
        }
    }
],

```

## Drop Postgres DB inside Docker container

-   [Follow this answer](https://stackoverflow.com/a/53982123)
-   Summary:
    -   Use `docker ps` command to get the container id
    -   Use this command to delete the database `docker exec -it container-id psql -U admin -d postgres -c "DROP DATABASE database-name;"  `
