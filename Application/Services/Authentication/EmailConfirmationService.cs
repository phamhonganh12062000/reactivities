using System.Text;
using Contracts.Application;
using Contracts.Infrastructure;
using Domain.Exceptions;
using Domain.Exceptions.AppUser;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;

namespace Application.Services.Authentication
{
  public class EmailConfirmationService : IEmailConfirmationService
  {
    private readonly IEmailSenderFactory _emailSenderFactory;
    private readonly UserManager<AppUser> _userManager;

    public EmailConfirmationService(IEmailSenderFactory emailSenderFactory, UserManager<AppUser> userManager)
    {
      _emailSenderFactory = emailSenderFactory;
      _userManager = userManager;
    }

    public async Task ResendEmailConfirmationLinkAsync(string origin, string email)
    {
      var user = await _userManager.FindByEmailAsync(email);

      if (user is null)
      {
        throw new AppUserNotFoundException(user);
      }

      await SendVerificationEmail(origin, user);
    }

    public async Task<IdentityResult> VerifyEmailAsync(string token, string email)
    {
      var user = await _userManager.FindByEmailAsync(email);

      if (user is null)
      {
        throw new AppUserNotFoundException(user);
      }

      var decodedTokenBytes = WebEncoders.Base64UrlDecode(token);

      var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

      var result = await _userManager.ConfirmEmailAsync(user, decodedToken);

      return result;
    }

    public async Task SendVerificationEmail(string origin, AppUser user)
    {
      var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);

      token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));

      var verifyUrl = $"{origin}/account/verifyEmail?token={token}&email={user.Email}";

      var message = $"<p>Please click the below link to verify your email address:</p><p><a href='{verifyUrl}'>Click to verify email</a></p>";

      await _emailSenderFactory.SendEmailAsync(user.Email, "Please verify email", message);
    }
  }
}
