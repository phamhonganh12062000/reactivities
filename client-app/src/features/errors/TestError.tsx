import axios from "axios";
import { useState } from "react";
import { Button, Header, Segment } from "semantic-ui-react";
import ValidationError from "./ValidationError";

export default function TestErrors() {
    const [errors, setErrors] = useState(null);

    function handleNotFound() {
        axios
            .get("/errors/not-found")
            .catch((err) => console.log(err.response));
    }

    function handleBadRequest() {
        axios
            .get("/errors/bad-request")
            .catch((err) => console.log(err.response));
    }

    function handleServerError() {
        axios
            .get("/errors/server-error")
            .catch((err) => console.log(err.response));
    }

    function handleUnauthorised() {
        axios
            .get("/errors/unauthorized")
            .catch((err) => console.log(err.response));
    }

    // TODO: Investigate why Bad Guid is 404 and not 400 (Lack validation for Guid?)
    function handleBadGuid() {
        axios
            .get("/activities/notaguid")
            .catch((err) => console.log(err.response));
    }

    function handleValidationError() {
        axios.post("/activities", {}).catch((err) => setErrors(err));
    }

    return (
        <>
            <Header as="h1" content="TestError component" />
            <Segment>
                <Button.Group widths="7">
                    <Button
                        onClick={handleNotFound}
                        content="Not Found"
                        basic
                        primary
                    />
                    <Button
                        onClick={handleBadRequest}
                        content="Bad Request"
                        basic
                        primary
                    />
                    <Button
                        onClick={handleValidationError}
                        content="Validation Error"
                        basic
                        primary
                    />
                    <Button
                        onClick={handleServerError}
                        content="Server Error"
                        basic
                        primary
                    />
                    <Button
                        onClick={handleUnauthorised}
                        content="Unauthorized"
                        basic
                        primary
                    />
                    <Button
                        onClick={handleBadGuid}
                        content="Bad Guid"
                        basic
                        primary
                    />
                </Button.Group>
            </Segment>
            {errors && <ValidationError errors={errors} />}
        </>
    );
}
