﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.ErrorMessages.Activity
{
    public static class ErrorMessages
    {
        public const string FailedToCreateActivity = "Failed to create activity";
    }
}
