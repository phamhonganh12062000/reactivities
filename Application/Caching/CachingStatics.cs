namespace Application.Caching
{
  public static class CachingStatics
  {
    public static string GetActivityById(Guid activityId)
    {
      return $"GetActivityById-{activityId}";
    }

    public static string GetAppUserByUsername(string username)
    {
      return $"GetAppUserByUsername-{username}";
    }

    public static string GetActivityCommentsByActivityId(Guid activityId)
    {
      return $"GetActivityCommentsByActivityId-{activityId}";
    }
  }
}
