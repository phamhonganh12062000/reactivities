import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import {
    Button,
    Container,
    Divider,
    Header,
    Image,
    Segment,
} from "semantic-ui-react";
import LoginForm from "../../features/users/LoginForm";
import RegisterForm from "../../features/users/RegisterForm";
import { useStore } from "../stores/baseStore";

export default observer(function HomePage() {
    const { userStore, modalStore } = useStore();
    return (
        <Segment inverted textAlign="center" className="masthead">
            <Container text>
                <Header as="h1" inverted>
                    <Image
                        size="massive"
                        src="/assets/logo.png"
                        alt="logo"
                        style={{ marginBottom: 12 }}
                    />
                    Reactivities
                </Header>
                {userStore.isLoggedIn ? (
                    <>
                        <Header
                            as="h2"
                            inverted
                            content="Welcome to Reactivities"
                        />
                        <Button as={Link} to="/activities" size="huge" inverted>
                            Go to activities
                        </Button>
                    </>
                ) : (
                    <>
                        <Button
                            onClick={() => modalStore.openModal(<LoginForm />)}
                            size="huge"
                            inverted
                        >
                            Login
                        </Button>
                        <Button
                            onClick={() =>
                                modalStore.openModal(<RegisterForm />)
                            }
                            size="huge"
                            inverted
                        >
                            Register
                        </Button>
                        <Divider horizontal inverted>
                            Or
                        </Divider>
                        <Button
                            loading={userStore.fbLoading}
                            size="huge"
                            inverted
                            color="facebook"
                            content="Login with Facebook"
                            onClick={userStore.facebookLogin}
                        ></Button>
                    </>
                )}
            </Container>
        </Segment>
    );
});
