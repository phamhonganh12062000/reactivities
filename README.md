# Reactivities

## Build & Run the application

```powershell
# Run the backend
dotnet watch run --project "/path/to/startup/project.csprj"
# Run the frontend
cd .\client-app\
npm start
```

## Data Model

![Reactivities Data Model](https://gitlab.com/phamhonganh12062000/reactivities/-/raw/master/Docs/Reactivities_Data_Model.png?ref_type=heads)
