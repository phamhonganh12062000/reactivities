using Application.Services.Photos.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    public class PhotosController : BaseController
    {
        [HttpPost]
        public async Task<IActionResult> Add([FromForm] IFormFile file)
        {
            return HandleResult(await Mediator.Send(new AddPhotoCommand(file, UserTrackChanges: true)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            return HandleResult(await Mediator.Send(new DeletePhotoCommand(id, UserTrackChanges: true)));
        }

        [HttpPut("{id}/setMain")]
        public async Task<IActionResult> SetMain(string id)
        {
            return HandleResult(await Mediator.Send(new SetMainPhotoCommand(id, UserTrackChanges: true)));
        }
    }
}
