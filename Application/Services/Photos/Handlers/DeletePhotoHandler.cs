using Application.Caching;
using Application.Services.Photos.Commands;
using Application.Services.Photos.Interfaces;
using Contracts.Infrastructure;
using Contracts.Persistence;
using Domain.Exceptions.Photos;
using Domain.Results;
using MediatR;

namespace Application.Services.Photos.Handlers
{
  public class DeletePhotoHandler : IRequestHandler<DeletePhotoCommand, Result<Unit>>
  {
    public readonly IUnitOfWork _unitOfWork;
    private readonly IPhotoAccessor _photoAccessor;
    private readonly IUserAccessor _userAccessor;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public DeletePhotoHandler(IPhotoAccessor photoAccessor, IUserAccessor userAccessor, IUnitOfWork unitOfWork, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _photoAccessor = photoAccessor;
      _userAccessor = userAccessor;
      _unitOfWork = unitOfWork;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Unit>> Handle(DeletePhotoCommand request, CancellationToken cancellationToken)
    {
      var currentUserUsername = _userAccessor.GetCurrentUserUsername();

      // Get the cache before removing them
      var userWithPhotoCacheKey = CachingStatics.GetAppUserByUsername(currentUserUsername);

      _memoryCacheWrapper.Remove(userWithPhotoCacheKey);

      var user = await _unitOfWork.AppUser.GetAppUserAsync(currentUserUsername, request.UserTrackChanges);

      var photo = user.Photos.FirstOrDefault(p => p.Id == request.Id);

      if (photo is null)
      {
        throw new PhotoNotFoundException(photo);
      }

      if (photo.IsMain)
      {
        return Result<Unit>.Failure("The main photo cannot be deleted");
      }

      var result = await _photoAccessor.DeletePhoto(photo.Id);

      if (result is null)
      {
        return Result<Unit>.Failure("Problem deleting photo");
      }

      user.Photos.Remove(photo);

      var isRemovePhotoSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isRemovePhotoSuccess)
      {
        return Result<Unit>.Failure("Problem removing the photo from the database");
      }

      return Result<Unit>.Success(Unit.Value);
    }
  }
}
