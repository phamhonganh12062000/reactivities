﻿using Domain.Dtos;
using Domain.Models;

namespace Contracts.Application.Services
{
  public interface ITokenService
  {
    Task<string> GenerateAccessToken(AppUser user);

    Task<string> PopulateRefreshTokenExpiryDate(AppUser user, bool populateExpiryDate);

    Task<AppUser> FetchUserWithExpiredAccessToken(string refreshToken);
  }
}
