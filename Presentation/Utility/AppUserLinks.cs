using Contracts.Domain;
using Contracts.Extensions;
using Domain.LinkModels;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Net.Http.Headers;

namespace Presentation.Utility
{
    public class AppUserLinks : IAppUserLinks
    {
        private readonly LinkGenerator _linkGenerator;

        private readonly IDataShaper<ProfileDto> _dataShaper;

        public AppUserLinks(LinkGenerator linkGenerator, IDataShaper<ProfileDto> dataShaper)
        {
            _linkGenerator = linkGenerator;
            _dataShaper = dataShaper;
        }

        public LinkResponse TryGenerateLinksForMultipleUsers(IEnumerable<ProfileDto> appUsersDto, string fields, HttpContext httpContext)
        {
            var shapedAppUsers = ShapeDataForMultipleAppUsers(appUsersDto, fields);

            if (ShouldGenerateLinks(httpContext))
            {
                return ReturnLinkedMultipleAppUsers(appUsersDto, fields, httpContext, shapedAppUsers);
            }

            return ReturnShapedMultipleAppUsers(shapedAppUsers);
        }

        public LinkResponse TryGenerateLinksForSingleUser(ProfileDto appUserDto, string fields, HttpContext httpContext)
        {
            var shapedAppUser = ShapeDataForSingleAppUser(appUserDto, fields);

            if (ShouldGenerateLinks(httpContext))
            {
                return ReturnLinkedSingleAppUser(appUserDto, fields, httpContext, shapedAppUser);
            }

            return ReturnShapedSingleAppUser(shapedAppUser);
        }

        private List<DynamicEntity> ShapeDataForMultipleAppUsers(IEnumerable<ProfileDto> appUsersDto, string fields, string objectPropertyName = "UserName")
        {
            return _dataShaper.ShapeDataMultipleEntities(appUsersDto, fields, objectPropertyName)
                .Select(e => e.DynamicEntity)
                .ToList();
        }

        private DynamicEntity ShapeDataForSingleAppUser(ProfileDto appUserDto, string fields, string objectPropertyName = "UserName")
        {
            return _dataShaper.ShapeDataSingleEntity(appUserDto, fields, objectPropertyName)
                .DynamicEntity;
        }

        /// <summary>
        /// Extract the mediatype and check if it supports hateoas.
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        private bool ShouldGenerateLinks(HttpContext httpContext)
        {
            var mediaType = (MediaTypeHeaderValue) httpContext.Items["AcceptHeaderMediaType"];

            return mediaType.SubTypeWithoutSuffix
                .EndsWith("hateoas", StringComparison.InvariantCultureIgnoreCase);
        }

        private LinkResponse ReturnLinkedMultipleAppUsers(IEnumerable<ProfileDto> appUsersDto, string fields, HttpContext httpContext, List<DynamicEntity> shapedAppUsers)
        {
            var appUserDtoList = appUsersDto.ToList();

            for (int i = 0; i < appUserDtoList.Count; i++)
            {
                var appUserLinks = CreateLinksForSingleAppUser(httpContext, appUserDtoList[i].Username, fields);

                shapedAppUsers[i].Add("Links", appUserLinks);
            }

            var appUserCollection = new LinkWrapper<DynamicEntity>(shapedAppUsers);

            return new LinkResponse
            {
                HasLinks = true,
                LinkedEntities = appUserCollection,
            };
        }

        private LinkResponse ReturnLinkedSingleAppUser(ProfileDto appUserDto, string fields, HttpContext httpContext, DynamicEntity shapedAppUser)
        {
            var appUserLinks = CreateLinksForSingleAppUser(httpContext, appUserDto.Username, fields);

            shapedAppUser.Add("Links", appUserLinks);

            var appUserWithLinks = new LinkWrapper<DynamicEntity>(shapedAppUser);

            return new LinkResponse
            {
                HasLinks = true,
                LinkedEntities = appUserWithLinks,
            };
        }

        private LinkResponse ReturnShapedMultipleAppUsers(List<DynamicEntity> shapedAppUsers)
        {
            return new LinkResponse
            {
                ShapedEntities = shapedAppUsers,
            };
        }

        private LinkResponse ReturnShapedSingleAppUser(DynamicEntity shapedAppUser)
        {
            return new LinkResponse
            {
                ShapedEntity = shapedAppUser,
            };
        }

        private List<Link> CreateLinksForSingleAppUser(HttpContext httpContext, string username, string fields = "")
        {
            var links = new List<Link>
            {
                new (_linkGenerator.GetUriByAction(httpContext, "GetUserByUsernameAsync", values: new { username, fields }), "user", "GET"),
                new (_linkGenerator.GetUriByAction(httpContext, "GetUserByUsernameWithPhotosAsync", values: new { username, fields }), "user_with_photos", "GET"),
            };

            return links;
        }

        // private LinkCollectionWrapper<DynamicEntity> CreateLinksForMultipleAppUsers(HttpContext httpContext, LinkCollectionWrapper<DynamicEntity> appUsersWrapper)
        // {
        //     appUsersWrapper.Links.Add(new Link(_linkGenerator.GetUriByAction(httpContext)))
        // }

    }
}
