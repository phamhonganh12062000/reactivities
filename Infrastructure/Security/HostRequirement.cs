﻿using Contracts.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Infrastructure.Security
{
    public class HostRequirement : IAuthorizationRequirement
    {
    }

    public class HostRequirementHandler : AuthorizationHandler<HostRequirement>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUnitOfWork _unitOfWork;

        public HostRequirementHandler(IHttpContextAccessor httpContextAccessor, IUnitOfWork unitOfWork)
        {
            _httpContextAccessor = httpContextAccessor;
            _unitOfWork = unitOfWork;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, HostRequirement requirement)
        {
            // Retrieve the user's unique identifier from the currently authenticated user
            var currentAuthenticatedUserId = context.User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (currentAuthenticatedUserId == null)
            {
                return;
            }

            // Get the id parameter of the current HTTP request
            var activityId = Guid.Parse(_httpContextAccessor.HttpContext?.Request.RouteValues
                .SingleOrDefault(x => x.Key == "id").Value?.ToString());

            // trackChanges flag = false to tell EF core to not make changes to the edited activity
            var attendee = await _unitOfWork.ActivityAttendee.GetActivityAttendeeByIdAsync(activityId, currentAuthenticatedUserId, trackChanges: false);

            if (attendee == null)
            {
                return;
            }

            if (attendee.IsHost)
            {
                context.Succeed(requirement);
            }
        }
    }
}
