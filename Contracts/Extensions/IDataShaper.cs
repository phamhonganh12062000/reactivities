﻿using System.Dynamic;
using Domain.Models;

namespace Contracts.Extensions
{
    public interface IDataShaper<T>
    {
        IEnumerable<ShapedDynamicEntity> ShapeDataMultipleEntities(IEnumerable<T> entities, string fieldsString, string objectPropertyName);

        ShapedDynamicEntity ShapeDataSingleEntity(T entity, string fieldsString, string objectPropertyName);
    }
}
