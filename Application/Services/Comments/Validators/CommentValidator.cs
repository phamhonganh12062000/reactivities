using Application.Services.Comments.Handlers;
using Application.Services.Photos.Commands;
using Domain.Dtos.Comment;
using FluentValidation;
using FluentValidation.Results;

namespace Application.Services.Comments.Validators
{
    public sealed class CommentValidator :
    AbstractValidator<AddCommentCommand>
    {
        public CommentValidator()
        {
            RuleFor(c => c.Body).NotEmpty();
        }
    }

    // public sealed class AddCommentValidator :
    // AbstractValidator<AddCommentCommand>
    // {
    //     public AddCommentValidator()
    //     {
    //         RuleFor(c => c.Bo).SetValidator(new CommentValidator());
    //     }

    //     public override ValidationResult Validate(ValidationContext<AddCommentCommand> context)
    //     {
    //         return context.InstanceToValidate.CommentForCreationDto is null
    //             ? new ValidationResult(new[]
    //             {
    //                 new ValidationFailure(nameof(CommentForCreationDto), nameof(CommentForCreationDto) + " object is null."),
    //             }) : base.Validate(context);
    //     }
    // }
}
