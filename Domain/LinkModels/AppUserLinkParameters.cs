using Domain.ParameterModels;
using Microsoft.AspNetCore.Http;

namespace Domain.LinkModels
{
    public record AppUserLinkParameters(AppUserParameters AppUserParameters, HttpContext Context);
}
