using Application.Caching;
using Application.Services.Photos.Commands;
using Application.Services.Photos.Interfaces;
using AutoMapper;
using Contracts.Infrastructure;
using Contracts.Persistence;
using Domain.Models;
using Domain.Results;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Services.Photos.Handlers
{
  public sealed class AddPhotoHandler : IRequestHandler<AddPhotoCommand, Result<Photo>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPhotoAccessor _photoAccessor;
    private readonly IUserAccessor _userAccessor;
    private readonly IMapper _mapper;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public AddPhotoHandler(IPhotoAccessor photoAccessor, IUserAccessor userAccessor, IMapper mapper, IUnitOfWork unitOfWork, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _photoAccessor = photoAccessor;
      _userAccessor = userAccessor;
      _mapper = mapper;
      _unitOfWork = unitOfWork;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<Photo>> Handle(AddPhotoCommand request, CancellationToken cancellationToken)
    {
      var photoUploadResult = await _photoAccessor.AddPhoto(request.File);

      var currentUserUsername = _userAccessor.GetCurrentUserUsername();

      // Get the cache before removing them
      _memoryCacheWrapper.Remove(CachingStatics.GetAppUserByUsername(currentUserUsername));

      var user = await _unitOfWork.AppUser.GetAppUserAsync(currentUserUsername, request.UserTrackChanges);

      var photo = _mapper.Map<Photo>(photoUploadResult);

      // Check if no existing photo is the main one => Set the uploaded one as main
      if (!user.Photos.Any(x => x.IsMain))
      {
        photo.IsMain = true;
      }

      user.Photos.Add(photo);

      var isSavedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSavedSuccess)
      {
        return Result<Photo>.Failure("Problem adding new photo");
      }

      return Result<Photo>.Success(photo);
    }
  }
}
