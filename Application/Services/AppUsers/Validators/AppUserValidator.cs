﻿using Domain.Dtos.AppUser;
using FluentValidation;

namespace Application.Services.AppUsers.Validators
{
    public sealed class AppUserValidator : AbstractValidator<ProfileForManipulationDto>
    {
        public AppUserValidator()
        {
            RuleFor(x => x.DisplayName).NotEmpty();
        }
    }
}
