﻿using Application.Services.Activities.Commands;
using Application.Services.Activities.Handlers;
using Domain.Dtos.Activity;
using Domain.Results;
using MediatR;
using Moq;

namespace Tests.ControllerTests
{
    public class ActivityController_Test
    {
        private readonly Mock<IMediator> _mockMediator;

        public ActivityController_Test()
        {
            _mockMediator = new Mock<IMediator>();
        }

        // public void SetupMediator<TCommand, TResult>(object result)
        //     where TCommand : IRequest<TResult>
        // {
        //     _mockMediator.Setup(m => m.Send(It.IsAny<TCommand>(), CancellationToken.None))
        //         .ReturnsAsync((TResult)result);
        // }

        [Fact]
        public async void CreateSingleActivity_AddNewActivity_ReturnsNewActivity()
        {
            // ARRANGE
            var activityForCreationDto = new ActivityForCreationDto
            {
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            };

            var activityId = Guid.NewGuid();

            var activityDto = new ActivityDto
            {
                Id = activityId,
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            };

            var expectedResult = Result<ActivityDto>.Success(activityDto);

            // var setupMediatorMethod = GetType().GetMethod("SetupMediator")
            //     .MakeGenericMethod(typeof(CreateSingleActivityCommand), typeof(Result<ActivityDto>));
            // setupMediatorMethod.Invoke(this, new object[] { expectedResult });

            _mockMediator.Setup(m => m.Send(It.IsAny<CreateSingleActivityCommand>(), CancellationToken.None))
                .ReturnsAsync(expectedResult);

            // ACT
            var result = await _mockMediator.Object.Send(new CreateSingleActivityCommand(activityForCreationDto));

            // ASSERT
            Assert.IsType<ActivityDto>(result.Value);
            Assert.Equal(activityId, result.Value.Id);
            Assert.Equal(activityDto, result.Value);
        }
    }
}
