using Azure.Extensions.AspNetCore.Configuration.Secrets;
using Azure.Security.KeyVault.Secrets;

namespace API.Extensions
{
  internal class KeyVaultSecretReplacer : KeyVaultSecretManager
  {
    private readonly Dictionary<string, string> _placeholders;

    public KeyVaultSecretReplacer(IConfiguration configuration)
    {
      _placeholders = configuration
                      .AsEnumerable()
                      .Where(HasSecretPlaceholder)
                      .ToDictionary(
                        keySelector: GetSecretKey,
                        elementSelector: GetSecretName,
                        StringComparer.OrdinalIgnoreCase);
    }

    public override bool Load(SecretProperties secret)
    {
      return _placeholders.ContainsKey(secret.Name);
    }

    public override Dictionary<string, string> GetData(IEnumerable<KeyVaultSecret> secrets)
    {
      var loadedSecrets = secrets.ToArray();

      ValidateAllSecretsFound(loadedSecrets);

      return base.GetData(loadedSecrets);
    }

    private void ValidateAllSecretsFound(IList<KeyVaultSecret> secrets)
    {
      var missing = _placeholders.Keys.Except(secrets.Select(s => s.Name)).ToArray();

      if (!missing.Any())
      {
        return;
      }

      var vaultName = secrets.FirstOrDefault()?.Properties.VaultUri.Host.Replace(".vault.azure.net", string.Empty);
      var notFoundMessage = string.Join(", ", missing.Select(s => $" '{s}' (setting '{_placeholders[s]}')"));

      throw new InvalidOperationException(
        $"Some secrets were not found in key vault{(vaultName is null ? string.Empty : $" '{vaultName}'")} : {notFoundMessage}."
      );

    }

    internal static bool HasSecretPlaceholder(KeyValuePair<string, string?> c)
    {
      return c.Value != null && c.Value.StartsWith("${") && c.Value.EndsWith("}");
    }

    /// <summary>
    ///  Removes the `{` at the beginning and `}` at the end of the secret placeholder, leaving the actual name of the secret.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    private static string GetSecretName(KeyValuePair<string, string?> c)
    {
      return c.Value![2..^1];
    }

    private static string GetSecretKey(KeyValuePair<string, string?> c)
    {
      return c.Key;
    }
  }
}
