import {
    HubConnection,
    HubConnectionBuilder,
    LogLevel,
} from "@microsoft/signalr";
import { makeAutoObservable, runInAction } from "mobx";
import { ChatComment } from "../models/comment";
import { baseStore } from "./baseStore";

export default class CommentStore {
    comments: ChatComment[] = [];
    hubConnection: HubConnection | null = null;

    constructor() {
        makeAutoObservable(this);
    }

    createHubConnection = (activityId: string) => {
        if (baseStore.activityStore.selectedActivity) {
            this.hubConnection = new HubConnectionBuilder()
                .withUrl(
                    process.env.REACT_APP_CHAT_URL +
                        "?activityId=" +
                        activityId,
                    {
                        accessTokenFactory: () =>
                            baseStore.userStore.user?.accessToken!,
                    }
                )
                .withAutomaticReconnect()
                .configureLogging(LogLevel.Information)
                .build();

            this.hubConnection
                .start()
                .catch((error) =>
                    console.log("Error establishing the connection: " + error)
                );

            this.hubConnection.on("LoadComments", (comments: ChatComment[]) => {
                runInAction(() => {
                    comments.forEach((comment) => {
                        comment.createdAt = this.formatCommentDate(comment);
                    });
                    this.comments = comments;
                });
            });

            this.hubConnection.on("ReceiveComment", (comment: ChatComment) => {
                runInAction(() => {
                    comment.createdAt = this.formatCommentDate(comment);
                    // Put the comment at the start of the array
                    this.comments.unshift(comment);
                });
            });
        }
    };

    stopHubConnection = () => {
        this.hubConnection
            ?.stop()
            .catch((error) =>
                console.log("Error stopping connection: " + error)
            );
    };

    clearComments = () => {
        this.comments = [];
        this.stopHubConnection();
    };

    addComment = async (values: any) => {
        values.activityId = baseStore.activityStore.selectedActivity?.id;
        try {
            await this.hubConnection?.invoke("SendComment", values);
        } catch (error) {
            console.log(error);
        }
    };

    private formatCommentDate(comment: ChatComment) {
        return new Date(comment.createdAt);
    }
}
