﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Persistence;

namespace API
{
  public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
  {
    private string _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

    /// <summary>
    /// Help EF Core create an instance of DataContext during design time
    /// i.e. I do not need to run the entire application to create an instance of DataContext so that I can do migrations + updates.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public DataContext CreateDbContext(string[] args)
    {
      var configurations = new ConfigurationBuilder()
          .AddUserSecrets<Program>()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json", optional: false)
          .AddJsonFile($"appsettings.{_environment}.json", optional: true)
          .AddEnvironmentVariables()
          .Build();

      string connectionString = null;
      switch (_environment)
      {
        case "Development":
        case "Staging":
          connectionString = configurations.GetConnectionString("DefaultConnection");
          break;
        case "Production":
          connectionString = Environment.GetEnvironmentVariable("POSTGRES_CONN_STRING");
          break;
      }

      var builder = new DbContextOptionsBuilder<DataContext>()
          .UseNpgsql(connectionString,
              b => b.MigrationsAssembly(typeof(AssemblyReference).Assembly.FullName));

      return new DataContext(builder.Options);
    }
  }
}
