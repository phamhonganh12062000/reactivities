﻿using Microsoft.AspNetCore.Http;
using Shared.RequestFeatures;
using Shared.Static;
using System.Text.Json;

namespace Presentation.Utility
{
    public static class HttpExtensions
    {
        public static void AddPaginationHeader(this HttpResponse response, MetaData metaData)
        {
            response.Headers.Add(Headers.Pagination, JsonSerializer.Serialize(metaData, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            }));
        }
    }
}
