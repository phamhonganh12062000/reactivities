﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel;
using System.Reflection;

namespace Presentation.ModelBinders
{
    public class ArrayModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (!IsBindingContextEnumerableType(bindingContext))
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }

            var stringOfGuids = ExtractStringOfGuids(bindingContext);

            if (IsGuidParametersNull(stringOfGuids))
            {
                bindingContext.Result = ModelBindingResult.Success(null);
                return Task.CompletedTask;
            }

            var contextType = GetBindingContextType(bindingContext.ModelType);

            bindingContext.Model = ConvertGuidStringToArray(stringOfGuids, contextType);

            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);

            return Task.CompletedTask;
        }

        private bool IsGuidParametersNull(string stringOfGuids)
        {
            return string.IsNullOrEmpty(stringOfGuids);
        }

        private bool IsBindingContextEnumerableType(ModelBindingContext bindingContext)
        {
            return bindingContext.ModelMetadata.IsEnumerableType;
        }

        private string ExtractStringOfGuids(ModelBindingContext bindingContext)
        {
            return bindingContext.ValueProvider.GetValue(bindingContext.ModelName).ToString();
        }

        private Type GetBindingContextType(Type contextType)
        {
            return contextType.GetTypeInfo().GenericTypeArguments[0];
        }

        private Array ConvertGuidStringToArray(string stringOfGuids, Type contextType)
        {
            var converter = TypeDescriptor.GetConverter(contextType);

            var tempArr = stringOfGuids.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => converter.ConvertFromString(x.Trim()))
                .ToArray();

            var guidArr = Array.CreateInstance(contextType, tempArr.Length);

            tempArr.CopyTo(guidArr, 0);

            return guidArr;
        }
    }
}
