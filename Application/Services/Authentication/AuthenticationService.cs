﻿using AutoMapper;
using Contracts.Application.Services;
using Contracts.Logger;
using Domain.Dtos.Authentication;
using Domain.Exceptions;
using Domain.Exceptions.AppUser;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Authentication
{
  internal class AuthenticationService : IAuthenticationService
  {
    private readonly ILoggerManager _logger;
    private readonly IMapper _mapper;
    private readonly UserManager<AppUser> _userManager;
    private readonly SignInManager<AppUser> _signInManager;

    private AppUser? _user;

    public AuthenticationService(ILoggerManager logger, IMapper mapper, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
    {
      _logger = logger;
      _mapper = mapper;
      _userManager = userManager;
      _signInManager = signInManager;
    }

    public async Task<AppUser> GetUserByEmailAsync(string email)
    {
      return await _userManager.Users
          .Include(p => p.Photos)
          .FirstOrDefaultAsync(u => u.Email == email);
    }

    public async Task<(IdentityResult, AppUser)> RegisterUserAsync(RegistrationDto registrationDto)
    {
      if (await _userManager.Users.AnyAsync(x => x.UserName == registrationDto.Username))
      {
        throw new ValidationIdentityException("Username", "Username is already taken");
      }

      if (await _userManager.Users.AnyAsync(x => x.Email == registrationDto.Email))
      {
        throw new ValidationIdentityException("Email", "Email is already taken");
      }

      var user = _mapper.Map<AppUser>(registrationDto);

      var result = await _userManager.CreateAsync(user, registrationDto.Password);

      return (result, user);
    }

    public async Task<(bool, AppUser)> ValidateUserAsync(LoginDto loginDto)
    {
      _user = await _userManager.Users
          .Include(p => p.Photos)
          .FirstOrDefaultAsync(u => u.Email.Equals(loginDto.Email));

      if (_user is null)
      {
        throw new AppUserNotFoundException(_user);
      }

      if (!_user.EmailConfirmed)
      {
        throw new UnauthorizedAccessException("Email has not been confirmed.");
      }

      var result = await _userManager.CheckPasswordAsync(_user, loginDto.Password);

      if (!result)
      {
        _logger.LogWarning($"{nameof(ValidateUserAsync)}: Authentication failed. Wrong user name or password.");
      }

      return (result, _user);
    }
  }
}
