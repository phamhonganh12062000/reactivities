﻿using Contracts.Extensions;
using Domain.Models;
using System.Reflection;

namespace Application.DataShaping
{
    public class DataShaper<T> : IDataShaper<T>
        where T : class
    {
        public PropertyInfo[] Properties { get; set; }

        public DataShaper()
        {
            // Use reflection to obtain the public instance properties
            Properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        public IEnumerable<ShapedDynamicEntity> ShapeDataMultipleEntities(IEnumerable<T> entities, string fieldsString, string objectPropertyName = "Id")
        {
            var requiredProperties = GetRequiredProperties(fieldsString);

            return FetchDataForMultipleEntities(entities, requiredProperties, objectPropertyName);
        }

        public ShapedDynamicEntity ShapeDataSingleEntity(T entity, string fieldsString, string objectPropertyName = "Id")
        {
            var requiredProperties = GetRequiredProperties(fieldsString);

            return FetchDataForSingleEntity(entity, requiredProperties, objectPropertyName);
        }

        private IEnumerable<PropertyInfo> GetRequiredProperties(string fieldsString)
        {
            var requiredProperties = new List<PropertyInfo>();

            if (!string.IsNullOrWhiteSpace(fieldsString))
            {
                var fields = fieldsString.Split(',', StringSplitOptions.RemoveEmptyEntries);

                foreach (var field in fields)
                {
                    var property = Properties.FirstOrDefault(pi =>
                        pi.Name.Equals(field.Trim(), StringComparison.InvariantCultureIgnoreCase));

                    if (property == null)
                    {
                        continue;
                    }

                    requiredProperties.Add(property);
                }
            }
            else
            {
                // If null then include all props
                requiredProperties = Properties.ToList();
            }

            return requiredProperties;
        }

        private IEnumerable<ShapedDynamicEntity> FetchDataForMultipleEntities(IEnumerable<T> entities, IEnumerable<PropertyInfo> requiredProperties, string objectPropertyName)
        {
            var shapedObjectsCollection = new List<ShapedDynamicEntity>();

            foreach (var entity in entities)
            {
                var shapedObject = FetchDataForSingleEntity(entity, requiredProperties, objectPropertyName);
                shapedObjectsCollection.Add(shapedObject);
            }

            return shapedObjectsCollection;
        }

        private ShapedDynamicEntity FetchDataForSingleEntity(T entity, IEnumerable<PropertyInfo> requiredProperties, string objectPropertyName)
        {
            var shapedObject = new ShapedDynamicEntity();

            foreach (var property in requiredProperties)
            {
                var objectPropertyValue = property.GetValue(entity);
                var propertyNameCamelCase = ConvertPascalCasePropertyToCamelCase(property);
                shapedObject.DynamicEntity.TryAdd(propertyNameCamelCase, objectPropertyValue);
            }

            var objectProperty = entity.GetType().GetProperty(objectPropertyName);

            if (objectProperty != null)
            {
                // Add any property of the DynamicEntity to the shapedObject
                var objectPropertyValue = objectProperty.GetValue(objectProperty);
                shapedObject.DynamicEntity.TryAdd(objectPropertyName, objectPropertyValue);
            }

            return shapedObject;
        }

        private string ConvertPascalCasePropertyToCamelCase(PropertyInfo pascalCaseProp)
        {
            return $"{char.ToLowerInvariant(pascalCaseProp.Name[0])}{pascalCaseProp.Name[1..]}";
        }
    }
}
