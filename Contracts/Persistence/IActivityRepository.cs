﻿using Domain.Models;
using Domain.ParameterModels;
using Shared.RequestFeatures;

namespace Contracts.Persistence
{
    public interface IActivityRepository
    {
        IQueryable<Activity> GetAllActivities(ActivityParameters activityParameters, bool trackChanges);

        Task<IEnumerable<Activity>> GetMultipleActivitiesAsync(IEnumerable<Guid> ids, bool trackChanges);

        Task<Activity> GetSingleActivityAsync(Guid id, bool trackChanges);

        Task<Activity> GetSingleActivityWithComments(Guid id, bool trackChanges);

        Task CreateActivityAsync(Activity activity);

        void DeleteActivity(Activity activity);
    }
}
