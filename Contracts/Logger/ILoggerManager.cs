﻿namespace Contracts.Logger
{
    public interface ILoggerManager
    {
        void LogInfo(string message, params object[] propertyValues);

        void LogWarning(string message, params object[] propertyValues);

        void LogError(string message);

        void LogError(Exception ex, string message, params object[] propertyValues);

        void LogDebug(string message, params object[] propertyValues);
    }
}
