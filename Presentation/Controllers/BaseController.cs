﻿using Contracts.Logger;
using Domain.Results;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace Presentation.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class BaseController : ControllerBase
  {
    private IMediator _mediator;

    private IMemoryCache _memoryCache;

    private ILoggerManager _logger;

    // Locking mechanism for cache
    private static readonly SemaphoreSlim _semaphore = new(1, 1);

    protected ILoggerManager Logger
    {
      get
      {
        return _logger ??= HttpContext.RequestServices.GetService<ILoggerManager>();
      }
    }

    protected IMemoryCache MemoryCache
    {
      get
      {
        return _memoryCache ??= HttpContext.RequestServices.GetService<IMemoryCache>();
      }
    }

    protected IMediator Mediator
    {
      get
      {
        return _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
      }
    }

    [NonAction]
    protected IActionResult HandleResult<T>(Result<T> result, string routeName = null, object routeValues = null)
    {
      if (result == null)
      {
        return NotFound();
      }

      if (result.IsSuccess)
      {
        if (result.Value == null)
        {
          return NotFound();
        }
        else
        {
          if (string.IsNullOrEmpty(routeName))
          {
            return Ok(result.Value);
          }
          else
          {
            return CreatedAtRoute(routeName, routeValues, result.Value);
          }
        }
      }

      return BadRequest(result.ErrorMessage);
    }
  }
}
