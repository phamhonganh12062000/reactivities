﻿using System.Linq.Expressions;

namespace Contracts.Persistence
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> GetAll(bool trackChanges);

        IQueryable<T> Find(Expression<Func<T, bool>> condition, bool trackChanges, params Expression<Func<T, object>>[] includedProperties);

        Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> expression, bool trackChanges, params Expression<Func<T, object>>[] includedProperties);

        Task CreateAsync(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
