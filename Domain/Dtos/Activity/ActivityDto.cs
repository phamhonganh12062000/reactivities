﻿using Domain.Dtos.ActivityAttendee;
using Shared.RequestFeatures;

namespace Domain.Dtos.Activity
{
    public record ActivityDto
    {
        public Guid Id { get; init; }

        public string Title { get; init; }

        public DateTime Date { get; init; }

        public string Description { get; init; }

        public string Category { get; init; }

        public string City { get; init; }

        public string Venue { get; init; }

        public string HostUsername { get; init; }

        public bool IsCancelled { get; init; }

        public MetaData MetaData { get; init; }

        public ICollection<ActivityAttendeeDto> Attendees { get; init; }
    }
}
