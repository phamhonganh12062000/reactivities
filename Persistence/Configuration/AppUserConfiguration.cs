﻿using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Static;

namespace Persistence.Configuration
{
    public class AppUserConfiguration : IEntityTypeConfiguration<AppUser>
    {
        /// <summary>
        /// Seed data AFTER EF Core 2.1.
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.HasData(SeedAppUsers.UserBob, SeedAppUsers.UserJane, SeedAppUsers.UserTom);
        }
    }

    public class SeedAppUsers
    {
      public static AppUser UserBob = new()
      {
          Id = SeedData.AppUsers.Bob,
          DisplayName = "Bob",
          UserName = "bob",
          Email = "bob@test.com",
      };

      public static AppUser UserJane = new()
      {
          Id = SeedData.AppUsers.Jane,
          DisplayName = "Jane",
          UserName = "jane",
          Email = "jane@test.com",
      };

      public static AppUser UserTom = new()
      {
          Id = SeedData.AppUsers.Tom,
          DisplayName = "Tom",
          UserName = "tom",
          Email = "tom@test.com",
      };
    }
}
