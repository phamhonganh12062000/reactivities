import { Tab } from "semantic-ui-react";
import { Profile } from "../../app/models/profile";
import { useStore } from "../../app/stores/baseStore";
import ProfileAbout from "./ProfileAbout";
import ProfileActivities from "./ProfileActivities";
import ProfileFollowing from "./ProfileFollowing";
import ProfilePhotos from "./ProfilePhotos";

interface Props {
    profile: Profile;
}

export default function ProfileContent({ profile }: Props) {
    const panes = [
        { menuItem: "About", render: () => <ProfileAbout /> },
        {
            menuItem: "Photos",
            render: () => <ProfilePhotos profile={profile} />,
        },
        {
            menuItem: "Events",
            render: () => <ProfileActivities />,
        },
        {
            menuItem: "Followers",
            render: () => <ProfileFollowing />,
        },
        {
            menuItem: "Following",
            render: () => <ProfileFollowing />,
        },
    ];

    const { profileStore } = useStore();

    return (
        <Tab
            menu={{ fluid: true, vertical: true }}
            menuPosition="right"
            panes={panes}
            onTabChange={(e, data) =>
                profileStore.setActiveTab(data.activeIndex)
            }
        />
    );
}
