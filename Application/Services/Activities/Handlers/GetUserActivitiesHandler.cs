using Application.Caching;
using AutoMapper;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Handlers
{
  public sealed record GetUserActivitiesQuery(string Username, string Predicate, bool TrackChanges)
      : IRequest<Result<IEnumerable<UserActivityDto>>>
  {
  }

  public sealed class GetUserActivitiesHandler : IRequestHandler<GetUserActivitiesQuery, Result<IEnumerable<UserActivityDto>>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetUserActivitiesHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    public async Task<Result<IEnumerable<UserActivityDto>>> Handle(GetUserActivitiesQuery request, CancellationToken cancellationToken)
    {
      var activityAttendees = _unitOfWork.ActivityAttendee.GetUserActivities(request.Username, request.TrackChanges);

      var userActivitiesDto = _mapper.Map<IEnumerable<UserActivityDto>>(activityAttendees);

      userActivitiesDto = request.Predicate switch
      {
        "past" => userActivitiesDto.Where(a => a.Date <= DateTime.UtcNow),
        "hosting" => userActivitiesDto.Where(a => a.HostUsername.Equals(request.Username)),
        _ => userActivitiesDto.Where(a => a.Date >= DateTime.UtcNow),
      };

      return Result<IEnumerable<UserActivityDto>>.Success(userActivitiesDto.ToList());
    }
  }
}
