using API;
using API.Extensions;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Mvc.Authorization;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args);
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{environment}.json", optional: true)
        .AddEnvironmentVariables();

// Two-stage initialization
Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich.FromLogContext()
        .CreateBootstrapLogger();

builder.Services.AddControllers(config =>
{
  config.RespectBrowserAcceptHeader = true;
  config.ReturnHttpNotAcceptable = true; // Return 406 if media type is not supported
  var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build(); // Require authorization for all actions
  config.Filters.Add(new AuthorizeFilter(policy));
}).AddXmlDataContractSerializerFormatters()
    .ConfigureCustomCSVFormatter()
    .AddApplicationPart(typeof(Presentation.AssemblyReference).Assembly);
builder.Services.AddLoggerService();
builder.Services.AddMediatR(typeof(Application.AssemblyReference).Assembly);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(Application.AssemblyReference).Assembly);
builder.Services.AddPipelineValidationBehavior();
builder.Services.AddUnitOfWork();
builder.Services.AddServiceManager();
builder.Services.AddDataShaping();
builder.Services.AddHttpContextAccessor();
builder.Services.AddUserAccessor();
builder.Services.AddPhotoAccesor();
builder.Services.AddAuthorizationHandler();
builder.Services.AddEntityLinks();
builder.Services.AddValidationForMediaTypeAttribute();
builder.Services.AddEmailSenderFactory(builder.Configuration);

builder.Services.AddMemoryCacheWrapper(builder.Configuration);

builder.Host.ConfigureSerilog();
builder.Host.ConfigureAzureIntegration(builder.Configuration);
builder.Services.ConfigureCors();
builder.Services.ConfigureSqlContext(builder.Configuration, Log.Logger);
builder.Services.ConfigureCustomResponses();
builder.Services.ConfigureFluentValidators();
builder.Services.ConfigureIdentity();
builder.Services.ConfigureJWT(builder.Configuration);
builder.Services.ConfigureCloudinary(builder.Configuration, builder.Environment);
builder.Services.ConfigureCustomMediaTypes();
builder.Services.ConfigureSignalR(Log.Logger);
builder.Services.ConfigureHttpCacheHeaders();
builder.Services.ConfigureFacebookLogin(builder.Configuration, builder.Environment);
builder.Services.ConfigureHttpClient();
builder.Services.ConfigureHealthCheck(builder.Configuration, builder.Environment);

try
{
  Log.Information("Starting up");

  var app = builder.Build();

  app.UseSerilogRequestLogging();

  var databaseSeeder = new DatabaseSeeder(app.Services);

  app.ConfigureExceptionHandler();

  app.UseXContentTypeOptions(); // Prevent MIME-sniff the content type
  app.UseReferrerPolicy((opt) => opt.NoReferrer());
  app.UseXXssProtection((opt) => opt.EnabledWithBlockMode());
  app.UseXfo((opt) => opt.Deny());
  app.UseCsp((opt) => opt // Prevent cross-site scripting
    .BlockAllMixedContent() // Only load https content
    .StyleSources(s => s.Self().CustomSources("https://fonts.googleapis.com",
        "sha256-DpOoqibK/BsYhobWHnU38Pyzt5SjDZuR/mFsAiVN7kk=",
        "sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=",
        "sha256-Nrgtg419nCBP8bXgABSaWyr3F4PyQzdugyta0l/6uvU=",
        "sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=",
        "sha256-+EDRxr7ehHKOUSbkZfC0hp8LZ1Vag4KImw5+kkzOL1Y=",
        "sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=",
        "sha256-IHdzGAJ/dJwX6+L9U2MXqkGDEkza1pn7T8lsJlG1zCQ=")) // Any style-sheet from our domain is approved
    .FontSources(s => s.Self().CustomSources("https://fonts.gstatic.com", "data:"))
    .FormActions(s => s.Self())
    .FrameAncestors(s => s.Self())
    .ImageSources(s => s.Self().CustomSources("blob:", "data:", "https://res.cloudinary.com",
        "https://www.facebook.com",
        "https://platform-lookaside.fbsbx.com",
        "https://avatars3.githubusercontent.com"))
    .ScriptSources(s => s.Self().CustomSources("sha256-Jdvkb5/9No6SJoB3cj43/16JwI/BCnxj/kxzcYnd1ic=",
        "sha256-QUw4KBGk+rJjxmQakQQFkNRlNSFnGXQ6lqAVLy6rLDs=",
        "sha256-Pog3qaswXe7/IWv8apyiJjOx0FUoFHU464msTqEeZNc=",
        "sha256-0HWff3xgSUf4SC+JrndXcr2C0NwIkSC0hajivXigKlg=",
        "https://connect.facebook.net")));

  // Apply Strict-Transport-Security Header
  if (app.Environment.IsProduction())
  {
    app.UseHsts();
  }

  // Configure the HTTP request pipeline.
  if (app.Environment.IsDevelopment())
  {
    app.UseSwagger();
    app.UseSwaggerUI();
  }
  else
  {
    app.Use(async (context, next) =>
    {
      context.Response.Headers.Add("Strict-Transport-Security", "max-age=31536000");
      await next.Invoke();
    });
  }

  app.UseCors("CorsPolicy");
  app.UseHttpCacheHeaders();

  app.UseAuthentication();

  app.UseAuthorization();

  app.UseDefaultFiles();
  app.UseStaticFiles();

  await databaseSeeder.SeedDatabase();

  app.MapControllers();

  app.MapHub<ChatHub>("/chat");

  app.MapFallbackToController("Index", "Fallback");

  app.MapHealthChecks("/health", new HealthCheckOptions
  {
    // Predicate = (reg) => reg.Tags.Contains("dbContext"),
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
    AllowCachingResponses = true,
  })
    // .RequireAuthorization()
    // .RequireCors("CorsPolicy")
    ;

  app.MapHealthChecksUI();

  app.Run();
}
catch (Exception ex) when (ex is not HostAbortedException)
{
  Log.Fatal(ex, "Application start-up failed");
}
finally
{
  Log.CloseAndFlush();
}
