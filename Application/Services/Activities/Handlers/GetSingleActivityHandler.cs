﻿using Application.Caching;
using Application.Services.Activities.Queries;
using AutoMapper;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Exceptions.Activities;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Handlers
{
  internal sealed class GetSingleActivityHandler : IRequestHandler<GetSingleActivityQuery, Result<ActivityDto>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public GetSingleActivityHandler(IUnitOfWork unitOfWork, IMapper mapper, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<ActivityDto>> Handle(GetSingleActivityQuery request, CancellationToken cancellationToken)
    {
      var activityCacheKey = CachingStatics.GetActivityById(request.Id);

      var cacheResult = await _memoryCacheWrapper.GetOrCreateAsync(activityCacheKey, () => _unitOfWork.Activity.GetSingleActivityAsync(request.Id, request.TrackChanges) ?? throw new ActivityNotFoundException(request.Id), null);

      var activityDto = _mapper.Map<ActivityDto>(cacheResult);

      return Result<ActivityDto>.Success(activityDto);
    }
  }
}
