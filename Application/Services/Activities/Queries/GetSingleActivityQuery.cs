﻿using Domain.Dtos.Activity;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Queries
{
    public sealed record GetSingleActivityQuery(Guid Id, bool TrackChanges) : IRequest<Result<ActivityDto>>
    {
    }
}
