﻿namespace Domain.Dtos.AppUser
{
    public sealed record ProfileForUpdateDto : ProfileForManipulationDto
    {
    }
}
