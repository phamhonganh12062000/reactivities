using Application.Services.Activities.Handlers;
using Application.Services.ActivityAttendees.Handlers;
using Application.Services.AppUsers.Handlers;
using Domain.Dtos.AppUser;
using Domain.LinkModels;
using Domain.ParameterModels;
using Microsoft.AspNetCore.Mvc;
using Presentation.ActionFilters;

namespace Presentation.Controllers
{
    public class ProfilesController : BaseController
    {
        [HttpGet("{username}")]
        // [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
        public async Task<IActionResult> GetProfile(string username)
        {
            // var appUserLinkParams = new AppUserLinkParameters(appUserParameters, HttpContext);

            return HandleResult(await Mediator.Send(new GetAppUserQuery(username, TrackChanges: false)));
        }

        [HttpGet("{username}/activities")]
        public async Task<IActionResult> GetUserActivities(string username, string predicate)
        {
            return HandleResult(await Mediator.Send(new GetUserActivitiesQuery(username, predicate, false)));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProfile(ProfileForUpdateDto profileForUpdateDto)
        {
            var result = await Mediator.Send(new UpdateAppUserInfoCommand(profileForUpdateDto, TrackChanges: true));

            return HandleResult(result);
        }
    }
}
