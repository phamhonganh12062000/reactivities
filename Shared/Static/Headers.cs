﻿namespace Shared.Static
{
    public static class Headers
    {
        public const string Pagination = "Pagination";

        public const string WWWAuthenticate = "WWW-Authenticate";

        public const string AccessControlExposeHeaders = "Access-Control-Expose-Headers";

        public const string HATEOAS_JSON = "application/vnd.reactivities.hateoas+json";

        public const string HATEOAS_XML = "application/vnd.reactivities.hateoas+xml";
    }
}
