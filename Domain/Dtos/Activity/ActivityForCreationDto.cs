﻿namespace Domain.Dtos.Activity
{
    public record ActivityForCreationDto : ActivityForManipulationDto
    {
        public string Id { get; set; }
    }
}
