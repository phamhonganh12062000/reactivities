﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Contracts.Persistence
{
    public interface IUnitOfWork
    {
        IActivityRepository Activity { get; }

        IAppUserRepository AppUser { get; }

        IActivityAttendeeRepository ActivityAttendee { get; }

        ICommentRepository Comment { get; }

        IUserFollowingRepository UserFollowing { get; }

        Task<int> SaveAsync();
    }
}
