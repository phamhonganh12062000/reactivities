﻿using Application.Services.Authentication;
using AutoMapper;
using Contracts.Application;
using Contracts.Application.Services;
using Contracts.Infrastructure;
using Contracts.Logger;
using Domain.ConfigurationModels;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Application.Services
{
  public sealed class ServiceManager : IServiceManager
  {
    private readonly Lazy<IAuthenticationService> _authenticationService;
    private readonly Lazy<ITokenService> _tokenService;
    private readonly Lazy<IFacebookLoginService> _facebookLoginService;
    private readonly Lazy<IEmailConfirmationService> _emailConfirmationService;

    public ServiceManager(ILoggerManager logger,
        IMapper mapper,
        UserManager<AppUser> userManager,
        SignInManager<AppUser> signInManager,
        IConfiguration configuration,
        IOptions<JwtConfiguration> jwtConfigOptions,
        IOptions<FacebookConfiguration> fbConfigOptions,
        HttpClient httpClient,
        IEmailSenderFactory emailSenderFactory)
    {
      _authenticationService = new Lazy<IAuthenticationService>(() => new AuthenticationService(logger, mapper, userManager, signInManager));
      _tokenService = new Lazy<ITokenService>(() => new TokenService(configuration, jwtConfigOptions, userManager));
      _facebookLoginService = new Lazy<IFacebookLoginService>(() => new FacebookLoginService(logger, mapper, userManager, fbConfigOptions, httpClient));
      _emailConfirmationService = new Lazy<IEmailConfirmationService>(() => new EmailConfirmationService(emailSenderFactory, userManager));
    }

    public IAuthenticationService AuthenticationService => _authenticationService.Value;

    public ITokenService TokenService => _tokenService.Value;

    public IFacebookLoginService FacebookLoginService => _facebookLoginService.Value;

    public IEmailConfirmationService EmailConfirmationService => _emailConfirmationService.Value;
  }
}
