﻿using Domain.Dtos.Comment;

namespace Contracts;
public interface IChatHubClient
{
    Task ReceiveComment(CommentDto commentDto);

    Task LoadComments(IEnumerable<CommentDto> commentDtos);
}
