﻿using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Shared.Static;

namespace Persistence.Extensions
{
    public class DbInitializer
    {
        public static void Initialize(UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var userList = new List<AppUser>
                {
                    new AppUser
                    {
                        UserName = "superadmin1",
                        Email = "superadmin1@test.com",
                        DisplayName = "Super Admin 1",
                    },
                    new AppUser
                    {
                        UserName = "superadmin2",
                        Email = "superadmin2@test.com",
                        DisplayName = "Super Admin 2",
                    },
                    new AppUser
                    {
                        UserName = "superadmin3",
                        Email = "superadmin3@test.com",
                        DisplayName = "Super Admin 3",
                    },
                    new AppUser
                    {
                        UserName = "superadmin4",
                        Email = "superadmin4@test.com",
                        DisplayName = "Super Admin 4",
                    },
                };

                foreach (var user in userList)
                {
                    userManager.CreateAsync(user, password: "Admin@12345").GetAwaiter().GetResult();
                }
            }
        }
    }
}
