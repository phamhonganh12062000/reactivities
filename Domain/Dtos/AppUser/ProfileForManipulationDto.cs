﻿namespace Domain.Dtos.AppUser
{
    public record ProfileForManipulationDto
    {
        public string Username { get; init; }

        public string DisplayName { get; init; }

        public string Image { get; init; }

        public string Bio { get; init; }
    }
}
