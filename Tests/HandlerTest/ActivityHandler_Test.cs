﻿using Application.Caching;
using Application.Services.Activities.Handlers;
using Application.Services.Activities.Queries;
using Application.Services.Comments.Handlers;
using AutoMapper;
using Contracts.Extensions;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Models;
using Domain.ParameterModels;
using Domain.Results;
using Moq;
using Shared.RequestFeatures;
using System.Dynamic;

namespace Tests.HandlerTest
{
  public class ActivityHandler_Test
  {
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private readonly Mock<IMapper> _mockMapper;
    private readonly Mock<IDataShaper<ActivityDto>> _mockDataShaper;
    private readonly Mock<IUserAccessor> _mockUserAccessor;
    private readonly Mock<ILoggerManager> _mockLogger;

    private readonly Mock<IMemoryCacheWrapper> _mockMemoryCacheProvider;

    public ActivityHandler_Test()
    {
      _mockUnitOfWork = new Mock<IUnitOfWork>();
      _mockMapper = new Mock<IMapper>();
      _mockDataShaper = new Mock<IDataShaper<ActivityDto>>();
      _mockUserAccessor = new Mock<IUserAccessor>();
      _mockLogger = new Mock<ILoggerManager>();
      _mockMemoryCacheProvider = new Mock<IMemoryCacheWrapper>();
    }

    [Fact]
    public async Task GetAllActivitiesHandler_GetAllActivities_ReturnsSuccessWithAllActivityDtos()
    {
      // ARRANGE
      var handler = new GetAllActivitiesHandler(
          _mockUnitOfWork.Object,
          _mockMapper.Object,
          _mockDataShaper.Object,
          _mockUserAccessor.Object
          );

      var request = new GetAllActivitiesQuery(new ActivityParameters { Fields = "title" }, TrackChanges: false);

      var activityList = new List<Activity>
            {
                new () { Id = Guid.NewGuid(), Title = "Activity 1" },
                new () { Id = Guid.NewGuid(), Title = "Activity 2" },
                new () { Id = Guid.NewGuid(), Title = "Activity 3" },
            };

      var activityDtoList = new List<ActivityDto>
            {
                new () { Id = Guid.NewGuid(), Title = "Activity 1" },
                new () { Id = Guid.NewGuid(), Title = "Activity 2" },
                new () { Id = Guid.NewGuid(), Title = "Activity 3" },
            };

      var queryableActivityList = Enumerable.ToList(activityList).AsQueryable();

      var shapedDtos = Result<IEnumerable<ExpandoObject>>.Success(new List<ExpandoObject>());

      _mockUnitOfWork.Setup(uow => uow.Activity.GetAllActivities(request.ActivityParameters, false))
          .Returns(queryableActivityList);

      _mockMapper.Setup(m => m.Map<IEnumerable<ActivityDto>>(queryableActivityList))
          .Returns(activityDtoList);

      // _mockDataShaper.Setup(ds => ds.ShapeDataMultipleEntities(activityDtos.Value, request.ActivityParameters.Fields!))
      //     .Returns(shapedDtos.Value);

      // ACT
      var (pagedActivitiesDto, metaData) = await handler.Handle(request, CancellationToken.None);

      // ASSERT
      Assert.IsType<PagedList<ActivityDto>>(pagedActivitiesDto.Value);

      // var shapedResultDtos = result.activityDtos.Value;
      // Assert.IsType<List<ExpandoObject>>(shapedResultDtos);
      // Assert.Equal(shapedDtos.Value, shapedResultDtos);
      //Assert.Equal(page.MetaData, metaData);
    }

    [Fact]
    public async Task GetMultipleActivitiesHandler_GetMultipleActivities_ReturnSuccessWithCollectionActivityDtos()
    {
      // ARRANGE
      var handler = new GetMultipleActivitiesHandler(_mockUnitOfWork.Object, _mockMapper.Object);

      var request = new GetMultipleActivitiesQuery(new List<Guid> { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() }, TrackChanges: false);

      var activities = new List<Activity>
            {
                new () { Id = Guid.NewGuid(), Title = "Activity 1" },
                new () { Id = Guid.NewGuid(), Title = "Activity 2" },
                new () { Id = Guid.NewGuid(), Title = "Activity 3" },
            };

      var activityDtos = new List<ActivityDto>
            {
                new () { Id = Guid.NewGuid(), Title = "Activity 1" },
                new () { Id = Guid.NewGuid(), Title = "Activity 2" },
                new () { Id = Guid.NewGuid(), Title = "Activity 3" },
            };

      var expected = Result<IEnumerable<ActivityDto>>.Success(activityDtos);

      _mockUnitOfWork.Setup(uow => uow.Activity.GetMultipleActivitiesAsync(It.IsAny<IEnumerable<Guid>>(), false))
          .ReturnsAsync(activities);

      _mockMapper.Setup(m => m.Map<IEnumerable<ActivityDto>>(activities))
          .Returns(expected.Value);

      // ACT
      var resultDtos = await handler.Handle(request, CancellationToken.None);

      // ASSERT
      Assert.True(resultDtos.IsSuccess);
      Assert.Equal(3, resultDtos.Value.Count());
      Assert.Equal(activityDtos, resultDtos.Value);
    }

    [Fact]
    public async Task UpdateActivityAttendanceHandler_AddNewAttendee_ReturnSuccessWithUpdateAttendanceActivityDto()
    {
      // ARRANGE
      var handler = new UpdateAttendanceHandler(_mockUnitOfWork.Object, _mockLogger.Object, _mockUserAccessor.Object, _mockMemoryCacheProvider.Object);

      var activityId = Guid.NewGuid();

      var request = new UpdateAttendanceCommand(Id: activityId, TrackChanges: true);

      var activity = new Activity
      {
        Id = activityId,
        Title = "Past Activity 1",
        Date = DateTime.UtcNow.AddMonths(-2),
        Description = "Activity 2 months ago",
        Category = "drinks",
        City = "London",
        Venue = "Pub",
        IsCancelled = false,
        Attendees = new List<ActivityAttendee>(),
      };

      var user = new AppUser
      {
        UserName = "TestUser",
      };

      var host = new ActivityAttendee
      {
        AppUser = user,
        IsHost = true,
      };

      var attendance = new ActivityAttendee
      {
        AppUser = user,
        Activity = activity,
        IsHost = false,
      };

      _mockUnitOfWork.Setup(x => x.Activity.GetSingleActivityAsync(request.Id, It.IsAny<bool>()))
          .ReturnsAsync(activity);

      _mockUserAccessor.Setup(a => a.GetCurrentUserUsername())
          .Returns(user.UserName);

      _mockUnitOfWork.Setup(x => x.AppUser.GetAppUserAsync(user.UserName, request.TrackChanges))
          .ReturnsAsync(user);

      _mockUnitOfWork.Setup(x => x.ActivityAttendee.GetActivityHostAsync(activity, request.TrackChanges))
          .ReturnsAsync(host);

      _mockUnitOfWork.Setup(x => x.ActivityAttendee.GetActivityAttendeeByUsernameAsync(activity, user, request.TrackChanges))
          .ReturnsAsync(attendance);

      _mockUnitOfWork.Setup(x => x.SaveAsync()).ReturnsAsync(1);

      // ACT
      var result = await handler.Handle(request, CancellationToken.None);

      // ASSERT
      Assert.True(result.IsSuccess);

      // TODO: Add more conditions
    }
  }
}
