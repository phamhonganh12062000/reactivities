
using System.Linq.Dynamic.Core;
using Domain.Models;
using Shared.Helpers;

namespace Persistence.Extensions
{
    public static class ActivityRepositoryExtensions
    {
        public static IQueryable<Activity> Filter(this IQueryable<Activity> activities, string categoryFilter)
        {
            if (string.IsNullOrWhiteSpace(categoryFilter))
            {
                return activities;
            }

            return activities.Where(a => a.Category.Equals(categoryFilter));
        }

        public static IQueryable<Activity> Search(this IQueryable<Activity> activities, string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                return activities;
            }

            var lowerCaseTerm = searchTerm.Trim().ToLower();

            return activities.Where(a => a.Title.ToLower().Contains(lowerCaseTerm));
        }

        public static IQueryable<Activity> Sort(this IQueryable<Activity> activities, string orderByQueryString)
        {
            if (string.IsNullOrWhiteSpace(orderByQueryString))
            {
                return activities.OrderBy(a => a.Date);
            }

            var orderQuery = OrderQueryBuilder.CreateOrderQuery<Activity>(orderByQueryString);

            if (string.IsNullOrWhiteSpace(orderQuery))
            {
                return activities.OrderBy(a => a.Date);
            }

            return activities.OrderBy(orderQuery);
        }
    }
}
