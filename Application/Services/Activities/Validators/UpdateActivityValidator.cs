﻿using Application.Services.Activities.Commands;
using Domain.Dtos.Activity;
using FluentValidation;
using FluentValidation.Results;

namespace Application.Services.Activities.Validators
{
    public sealed class UpdateActivityValidator : AbstractValidator<UpdateSingleActivityCommand>
    {
        public UpdateActivityValidator()
        {
            RuleFor(a => a.ActivityForUpdateDto).SetValidator(new ActivityValidator());
        }

        /// <summary>
        /// Validate the null request body with Fluent Valdation rules.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override ValidationResult Validate(ValidationContext<UpdateSingleActivityCommand> context)
        {
            return context.InstanceToValidate.ActivityForUpdateDto is null
                ? new ValidationResult(new[]
                {
                    new ValidationFailure(nameof(ActivityForUpdateDto), nameof(ActivityForUpdateDto) + " object is null."),
                }) : base.Validate(context);
        }
    }
}
