﻿using Domain.Dtos.Activity;
using Domain.Results;
using MediatR;

namespace Application.Services.Activities.Queries
{
    public sealed record GetMultipleActivitiesQuery(IEnumerable<Guid> Ids, bool TrackChanges)
        : IRequest<Result<IEnumerable<ActivityDto>>>
    {
    }
}
