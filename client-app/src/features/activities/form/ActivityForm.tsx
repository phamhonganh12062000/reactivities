import { Form, Formik } from "formik";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Button, Header, Segment } from "semantic-ui-react";
import { v4 as uuid } from "uuid";
import * as Yup from "yup";
import CustomDateInput from "../../../app/common/form/CustomDateInput";
import CustomSelectInput from "../../../app/common/form/CustomSelectInput";
import CustomTextArea from "../../../app/common/form/CustomTextArea";
import CustomTextInput from "../../../app/common/form/CustomTextInput";
import { categoryOption } from "../../../app/common/options/cateoryOption";
import LoadingComponent from "../../../app/layout/LoadingComponent";
import { ActivityFormValues } from "../../../app/models/activity";
import { useStore } from "../../../app/stores/baseStore";

export default observer(function ActivityForm() {
    const { activityStore } = useStore();
    const {
        createActivity,
        updateActivity,
        loadSingleActivity,
        loadingInitial,
    } = activityStore;
    const { id } = useParams();
    const navigate = useNavigate();

    const [activity, setActivity] = useState<ActivityFormValues>(
        new ActivityFormValues()
    );

    const validationSchema = Yup.object({
        title: Yup.string().required("The activity title is required"),
        description: Yup.string().required(
            "The activity description is required"
        ),
        category: Yup.string().required("The activity category is required"),
        date: Yup.string().required("The activity date is required"),
        venue: Yup.string().required("The activity venue is required"),
        city: Yup.string().required("The activity city is required"),
    });

    useEffect(() => {
        if (id) {
            loadSingleActivity(id).then((activity) =>
                setActivity(new ActivityFormValues(activity))
            );
        }
    }, [id, loadSingleActivity]);

    function handleFormSubmit(activity: ActivityFormValues) {
        if (!activity.id) {
            let newActivity = {
                ...activity,
                id: uuid(),
            };
            console.log(newActivity);

            createActivity(newActivity).then(() =>
                navigate(`/activities/${newActivity.id}`)
            );
        } else {
            updateActivity(activity).then(() =>
                navigate(`/activities/${activity.id}`)
            );
        }
    }

    if (loadingInitial) {
        return <LoadingComponent content="Loading activity..." />;
    }

    return (
        <Segment clearing>
            <Header content="Activity Details" sub color="teal" />
            <Formik
                validationSchema={validationSchema}
                enableReinitialize
                initialValues={activity}
                onSubmit={(values) => handleFormSubmit(values)}
            >
                {({ handleSubmit, isValid, isSubmitting, dirty }) => (
                    <Form
                        className="ui form"
                        onSubmit={handleSubmit}
                        autoComplete="off"
                    >
                        <CustomTextInput name="title" placeholder="Title" />
                        <CustomTextArea
                            rows={3}
                            placeholder="Description"
                            name="description"
                        />
                        <CustomSelectInput
                            options={categoryOption}
                            placeholder="Category"
                            name="category"
                        />
                        <CustomDateInput
                            placeholderText="Date"
                            name="date"
                            showTimeSelect
                            timeCaption="time"
                            dateFormat="MMMM d, yyyy h:mm aa"
                        />
                        <Header content="Location Details" sub color="teal" />
                        <CustomTextInput placeholder="City" name="city" />
                        <CustomTextInput placeholder="Venue" name="venue" />
                        <Button
                            as={Link}
                            to="/activities"
                            floated="right"
                            type="button"
                            content="Cancel"
                        />
                        <Button
                            disabled={isSubmitting || !dirty || !isValid}
                            loading={isSubmitting}
                            floated="right"
                            positive
                            type="submit"
                            content="Submit"
                        />
                    </Form>
                )}
            </Formik>
        </Segment>
    );
});
