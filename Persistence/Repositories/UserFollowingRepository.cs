﻿using Contracts.Persistence;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class UserFollowingRepository
        : RepositoryBase<UserFollowing>, IUserFollowingRepository
    {
        public UserFollowingRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public async Task AddFollowingAsync(UserFollowing userFollowing)
        {
            await CreateAsync(userFollowing);
        }

        public async Task<List<AppUser>> GetUserFollowerListAsync(string username, bool trackChanges)
        {
            var userList = await Find(t => t.Target.UserName.Equals(username), trackChanges)

                // Navigation props like Followers and Followings are LAZILY LOADED
                // Ensure that Observer prop of each UserFollowing is loaded
                .Include(fer => fer.Observer.Followers) // Fetch the observer of the target and the observer's followers
                .ThenInclude(o => o.Observer) // For each Follower in Followers, the observer of the follower should be fetched too

                // Select items from a list (UserFollowing) to form a new list (AppUser)
                .Select(u => new AppUser
                {
                    UserName = u.Observer.UserName,
                    DisplayName = u.Observer.DisplayName,
                    Bio = u.Observer.Bio,
                    Photos = u.Observer.Photos,
                    Followers = u.Observer.Followers,
                    Followings = u.Observer.Followings,
                })
                .ToListAsync();

            return userList;
        }

        public async Task<List<AppUser>> GetUserFollowingListAsync(string username, bool trackChanges)
        {
            return await Find(o => o.Observer.UserName.Equals(username), trackChanges)
                .Include(fer => fer.Target.Followers)
                .ThenInclude(o => o.Observer)
                .Select(u => new AppUser
                {
                    UserName = u.Target.UserName,
                    DisplayName = u.Target.DisplayName,
                    Bio = u.Target.Bio,
                    Photos = u.Target.Photos,
                    Followers = u.Target.Followers,
                    Followings = u.Target.Followings,
                })
                .ToListAsync();
        }

        public async Task<UserFollowing> GetUserFollowingAsync(string observerId, string targetId, bool trackChanges)
        {
            return await FindFirstOrDefaultAsync(uf => uf.ObserverId.Equals(observerId) && uf.TargetId.Equals(targetId), trackChanges);
        }

        public void RemoveFollowing(UserFollowing userFollowing)
        {
            Delete(userFollowing);
        }
    }
}
