﻿using Domain.Models;
using Domain.ParameterModels;

namespace Contracts.Persistence
{
  public interface IAppUserRepository
  {
    Task<AppUser> GetAppUserAsync(string username, bool trackChanges);
  }
}
