﻿using MediatR;

namespace Application.Services.Activities.Commands
{
    public sealed record DeleteSingleActivityCommand(Guid Id, bool TrackChanges) : IRequest
    {
    }
}
