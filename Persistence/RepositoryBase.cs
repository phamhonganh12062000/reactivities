﻿using Contracts.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Persistence
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T>
        where T : class
    {
        protected DataContext DataContext;

        public RepositoryBase(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        public async Task CreateAsync(T entity)
        {
            await DataContext.Set<T>().AddAsync(entity);
        }

        public void Delete(T entity)
        {
            DataContext.Set<T>().Remove(entity);
        }

        public IQueryable<T> Find(
            Expression<Func<T, bool>> condition,
            bool trackChanges,
            params Expression<Func<T, object>>[] includedProperties)
        {
            IQueryable<T> query = DataContext.Set<T>().Where(condition);

            if (includedProperties != null)
            {
                foreach (var includedProperty in includedProperties)
                {
                    query = query.Include(includedProperty);
                }
            }

            if (!trackChanges)
            {
                return query.AsNoTracking();
            }

            return query;
        }

        public async Task<T> FindFirstOrDefaultAsync(
            Expression<Func<T, bool>> condition,
            bool trackChanges,
            params Expression<Func<T, object>>[] includedProperties)
        {
            IQueryable<T> query = DataContext.Set<T>().Where(condition);

            if (includedProperties != null)
            {
                foreach (var includedProperty in includedProperties)
                {
                    query = query.Include(includedProperty);
                }
            }

            if (!trackChanges)
            {
                return await query
                    .AsNoTracking()
                    .FirstOrDefaultAsync(condition);
            }
            else
            {
                return await query
                    .FirstOrDefaultAsync(condition);
            }
        }

        public IQueryable<T> GetAll(bool trackChanges)
        {
            IQueryable<T> query = DataContext.Set<T>();

            if (!trackChanges)
            {
                return query.AsNoTracking();
            }

            return query;
        }

        public void Update(T entity)
        {
            DataContext.Set<T>().Update(entity);
        }
    }
}
