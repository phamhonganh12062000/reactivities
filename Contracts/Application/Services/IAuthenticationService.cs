﻿using Domain.Dtos;
using Domain.Dtos.Authentication;
using Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace Contracts.Application.Services
{
  public interface IAuthenticationService
  {
    Task<(IdentityResult, AppUser)> RegisterUserAsync(RegistrationDto registrationDto);

    Task<AppUser> GetUserByEmailAsync(string email);

    Task<(bool, AppUser)> ValidateUserAsync(LoginDto loginDto);
  }
}
