using Application.Caching;
using AutoMapper;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Models;
using Domain.Results;
using MediatR;

namespace Application.Services.Followers.Handlers
{
  public sealed record GetFollowerListQuery(string Username, string Predicate, bool UserTrackChanges)
      : IRequest<Result<List<ProfileDto>>>
  {
  }

  public sealed class GetFollowerListHandler
      : IRequestHandler<GetFollowerListQuery, Result<List<ProfileDto>>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public GetFollowerListHandler(IUnitOfWork unitOfWork, IMapper mapper, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<List<ProfileDto>>> Handle(GetFollowerListQuery request, CancellationToken cancellationToken)
    {
      var profiles = new List<AppUser>();

      var userCacheKey = CachingStatics.GetAppUserByUsername(request.Username);

      switch (request.Predicate)
      {
        case "followers":
          profiles = await _memoryCacheWrapper.GetOrCreateAsync(userCacheKey, () => _unitOfWork.UserFollowing.GetUserFollowerListAsync(request.Username, request.UserTrackChanges), null);
          break;
        case "following":
          profiles = await _memoryCacheWrapper.GetOrCreateAsync(userCacheKey, () => _unitOfWork.UserFollowing.GetUserFollowingListAsync(request.Username, request.UserTrackChanges), null);
          break;
      }

      var profilesDto = _mapper.Map<List<ProfileDto>>(profiles);

      return Result<List<ProfileDto>>.Success(profilesDto);
    }
  }
}
