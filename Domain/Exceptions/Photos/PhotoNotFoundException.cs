using Domain.Models;

namespace Domain.Exceptions.Photos
{
  public sealed class PhotoNotFoundException : NotFoundException
  {
    public PhotoNotFoundException(Photo photo)
        : base($"The photo with id: {photo.Id} does not exist in the database.")
    {
    }
  }
}
