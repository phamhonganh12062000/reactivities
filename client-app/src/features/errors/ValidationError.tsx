import { Message } from "semantic-ui-react";

interface Props {
    errors: any;
}

export default function ValidationError({ errors }: Props) {
    return (
        <Message error>
            {/* Check if there are errors and then proceed */}
            {errors && (
                <Message.List>
                    {errors.map((err: any, i: any) => (
                        <Message.Item key={i}>{err}</Message.Item>
                    ))}
                </Message.List>
            )}
        </Message>
    );
}
