namespace Domain.Dtos.Comment
{
    public record CommentForCreationDto : CommentForManipulationDto
    {
    }
}
