﻿using Contracts.Logger;
using Domain.ErrorModels;
using Domain.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;
using System.Text.Json;

namespace API.Extensions
{
    public static class ExceptionMiddleware
    {
        public static void ConfigureExceptionHandler(this WebApplication app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context => await HandleExceptions(context));
            });
        }

        private static async Task HandleExceptions(HttpContext context)
        {
            SetDefaultResponseSettings(context);

            var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

            if (contextFeature != null)
            {
                HandleErrorStatusCode(context, contextFeature);

                await WriteErrorResponse(context, contextFeature);
            }
        }

        private static void SetDefaultResponseSettings(HttpContext context)
        {
            // Fallback value in case there is no specific error handling logic triggered by the switch statement
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";
        }

        private static void HandleErrorStatusCode(HttpContext context, IExceptionHandlerFeature contextFeature)
        {
            context.Response.StatusCode = contextFeature.Error switch
            {
                NotFoundException => StatusCodes.Status404NotFound,
                BadRequestException => StatusCodes.Status400BadRequest,
                ValidationAppException => StatusCodes.Status422UnprocessableEntity,
                ValidationIdentityException => StatusCodes.Status400BadRequest,
                UnauthorizedAccessException => StatusCodes.Status401Unauthorized,

                // Good practice to include default catch-all case
                _ => StatusCodes.Status500InternalServerError
            };
        }

        private static async Task WriteErrorResponse(HttpContext context, IExceptionHandlerFeature contextFeature)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };

            switch (contextFeature.Error)
            {
                case ValidationAppException validationAppException:
                    await WriteValidationErrorResponse(context, validationAppException, options);
                    break;
                case ValidationIdentityException validationIdentityException:
                    await WriteValidationErrorResponse(context, validationIdentityException, options);
                    break;
                default:
                    await WriteDefaultErrorResponse(context, contextFeature, options);
                    break;
            }
        }

        private static async Task WriteValidationErrorResponse(HttpContext context, Exception validationException, JsonSerializerOptions options)
        {
            await context.Response.WriteAsync(JsonSerializer.Serialize(
                new
                {
                    ((dynamic)validationException).Errors,
                },
                options));
        }

        private static async Task WriteDefaultErrorResponse(HttpContext context, IExceptionHandlerFeature contextFeature, JsonSerializerOptions options)
        {
            await context.Response.WriteAsync(JsonSerializer.Serialize(
                new ErrorDetails()
                {
                    StatusCode = context.Response.StatusCode,
                    Message = contextFeature.Error.Message,
                    StackTrace = contextFeature.Error.StackTrace,
                }, options));
        }
    }
}
