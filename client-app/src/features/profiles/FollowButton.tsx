import { observer } from "mobx-react-lite";
import { SyntheticEvent } from "react";
import { Button, Reveal } from "semantic-ui-react";
import { Profile } from "../../app/models/profile";
import { useStore } from "../../app/stores/baseStore";

interface Props {
    profile: Profile;
}

export default observer(function FollowButton({ profile }: Props) {
    const { profileStore, userStore } = useStore();
    const { toggleFollowing, loadingFlag } = profileStore;

    if (userStore.user?.username === profile.username) {
        return null;
    }

    function handleFollow(e: SyntheticEvent, username: string) {
        e.preventDefault();
        profile.isFollowingThisUser
            ? toggleFollowing(username, false)
            : toggleFollowing(username, true);
    }

    return (
        <Reveal animated="move">
            <Reveal.Content visible style={{ width: "100%" }}>
                <Button
                    fluid
                    color={profile.isFollowingThisUser ? "teal" : "orange"}
                    content={
                        profile.isFollowingThisUser
                            ? "Following"
                            : "Not Following"
                    }
                />
            </Reveal.Content>
            <Reveal.Content hidden style={{ width: "100%" }}>
                <Button
                    fluid
                    color={profile.isFollowingThisUser ? "red" : "green"}
                    content={
                        profile.isFollowingThisUser ? "Unfollow" : "Follow"
                    }
                    loading={loadingFlag}
                    onClick={(e) => handleFollow(e, profile.username)}
                />
            </Reveal.Content>
        </Reveal>
    );
});
