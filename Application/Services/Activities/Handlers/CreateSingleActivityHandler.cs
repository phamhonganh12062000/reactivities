﻿using Application.Services.Activities.Commands;
using AutoMapper;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Models;
using Domain.Results;
using MediatR;
using Shared.ErrorMessages.Activity;

namespace Application.Services.Activities.Handlers
{
  public sealed record CreateSingleActivityCommand(ActivityForCreationDto ActivityForCreationDto) : IRequest<Result<ActivityDto>>
  {
  }

  internal sealed class CreateSingleActivityHandler : IRequestHandler<CreateSingleActivityCommand, Result<ActivityDto>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILoggerManager _logger;
    private readonly IUserAccessor _userAccessor;

    public CreateSingleActivityHandler(IUnitOfWork unitOfWork, IMapper mapper, ILoggerManager logger, IUserAccessor userAccessor)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _logger = logger;
      _userAccessor = userAccessor;
    }

    public async Task<Result<ActivityDto>> Handle(CreateSingleActivityCommand request, CancellationToken cancellationToken)
    {
      var user = await _unitOfWork.AppUser.GetAppUserAsync(_userAccessor.GetCurrentUserUsername(), trackChanges: true);

      var activity = _mapper.Map<Activity>(request.ActivityForCreationDto);

      var host = new ActivityAttendee
      {
        AppUser = user,
        Activity = activity,
        IsHost = true,
      };

      activity.Attendees.Add(host);

      await _unitOfWork.Activity.CreateActivityAsync(activity);

      var isSavedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSavedSuccess)
      {
        _logger.LogError(ErrorMessages.FailedToCreateActivity);
        return Result<ActivityDto>.Failure(ErrorMessages.FailedToCreateActivity);
      }

      var activityDto = _mapper.Map<ActivityDto>(activity);

      return Result<ActivityDto>.Success(activityDto);
    }
  }
}
