﻿using Domain.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.Behaviors
{
    public sealed class ValidationAppBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationAppBehavior(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            if (!HasValidationErrors())
            {
                return await next();
            }

            ThrowValidationAppException(request);

            return await next();
        }

        private bool HasValidationErrors()
        {
            return _validators.Any();
        }

        private bool HasErrorsInDictionary(Dictionary<string, string[]> errorsDictionary)
        {
            return errorsDictionary.Any();
        }

        private void ThrowValidationAppException(TRequest request)
        {
            var errorsDictionary = ExtractErrorsFromRequestValidators(request);

            if (HasErrorsInDictionary(errorsDictionary))
            {
                throw new ValidationAppException(errorsDictionary);
            }
        }

        private Dictionary<string, string[]> ExtractErrorsFromRequestValidators(TRequest request)
        {
            var context = new ValidationContext<TRequest>(request);

            var errorsDictionary = _validators
                .Select(x => x.Validate(context)) // Run the validation process for each element (x)
                .SelectMany(x => x.Errors) // Flatten lists of errors into a single sequence of errors
                .Where(x => x != null) // Filter out null error messages
                .GroupBy(
                    x => x.PropertyName.Substring(x.PropertyName.IndexOf('.') + 1), // Key selector function => Isolate the property name
                    x => x.ErrorMessage, // Element selector function (The messages themselves)
                    (propertyName, errorMessages) => new
                    {
                        Key = propertyName,
                        Values = errorMessages.Distinct().ToArray()
                    }).ToDictionary(x => x.Key, x => x.Values);

            return errorsDictionary;
        }
    }
}
