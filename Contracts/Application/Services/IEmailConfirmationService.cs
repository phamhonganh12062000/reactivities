using Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace Contracts.Application
{
  public interface IEmailConfirmationService
  {
    Task<IdentityResult> VerifyEmailAsync(string token, string email);

    Task ResendEmailConfirmationLinkAsync(string origin, string email);

    Task SendVerificationEmail(string origin, AppUser user);
  }
}
