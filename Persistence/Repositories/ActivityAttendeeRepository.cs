﻿using Contracts.Persistence;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class ActivityAttendeeRepository : RepositoryBase<ActivityAttendee>, IActivityAttendeeRepository
    {
        public ActivityAttendeeRepository(DataContext context)
            : base(context)
        {
        }

        public async Task<ActivityAttendee> GetActivityAttendeeByIdAsync(Guid activityId, string userId, bool trackChanges)
        {
            return await FindFirstOrDefaultAsync(aa => aa.ActivityId.Equals(activityId) && aa.AppUserId.Equals(userId), trackChanges);
        }

        public IQueryable<ActivityAttendee> GetUserActivities(string username, bool trackChanges)
        {
            return Find(aa => aa.AppUser.UserName.Equals(username), trackChanges)
                .Include(a => a.Activity)
                .Include(u => u.AppUser)
                .OrderBy(a => a.Activity.Date)
                .AsQueryable();
        }

        public async Task<ActivityAttendee> GetActivityAttendeeByUsernameAsync(Activity activity, AppUser user, bool trackChanges)
        {
            return await FindFirstOrDefaultAsync(aa => aa.ActivityId.Equals(activity.Id) && aa.AppUser.UserName.Equals(user.UserName), trackChanges);
        }

        public async Task<ActivityAttendee> GetActivityHostAsync(Activity activity, bool trackChanges)
        {
            return await FindFirstOrDefaultAsync(aa => aa.ActivityId.Equals(activity.Id) && aa.IsHost, trackChanges);
        }
    }
}
