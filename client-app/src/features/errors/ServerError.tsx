import { observer } from "mobx-react-lite";
import { Container, Header, Segment } from "semantic-ui-react";
import { useStore } from "../../app/stores/baseStore";

export default observer(function ServerError() {
  const { commonStore } = useStore();
  const isProduction = process.env.NODE_ENV === "production";
  return (
      <Container>
          <Header as="h1" content="Server Error" />
          <Header
              sub
              as="h5"
              color="red"
              content={commonStore.error?.message}
          />
          {commonStore.error?.message && (
              <Segment>
                  {!isProduction && (
                      <>
                          <Header as="h4" content="Stack Trace" color="teal" />
                          <code style={{ marginTop: "10px" }}>
                              {commonStore.error.stackTrace}
                          </code>
                      </>
                  )}
              </Segment>
          )}
      </Container>
  );
});
