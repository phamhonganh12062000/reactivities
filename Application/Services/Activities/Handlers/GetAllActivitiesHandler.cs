﻿using Application.Services.Activities.Queries;
using AutoMapper;
using Contracts.Extensions;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Shared.RequestFeatures;
using Domain.Results;
using MediatR;
using Contracts.Infrastructure;
using Application.Caching;

namespace Application.Services.Activities.Handlers
{
  public sealed class GetAllActivitiesHandler : IRequestHandler<GetAllActivitiesQuery,
      (Result<PagedList<ActivityDto>> pagedActivitiesDto, MetaData metaData)>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IDataShaper<ActivityDto> _dataShaper;
    private readonly IUserAccessor _userAccessor;

    public GetAllActivitiesHandler(IUnitOfWork unitOfWork, IMapper mapper, IDataShaper<ActivityDto> dataShaper, IUserAccessor userAccessor)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _dataShaper = dataShaper;
      _userAccessor = userAccessor;
    }

    public async Task<(Result<PagedList<ActivityDto>> pagedActivitiesDto, MetaData metaData)> Handle(
        GetAllActivitiesQuery request,
        CancellationToken cancellationToken)
    {
      var currentLoggedInUserUsername = _userAccessor.GetCurrentUserUsername();

      var activities = _unitOfWork.Activity.GetAllActivities(
          request.ActivityParameters,
          request.TrackChanges);

      var activityDtos = _mapper.Map<IEnumerable<ActivityDto>>(activities);

      // Show only activities that current user is going to not as a host
      if (request.ActivityParameters.IsGoingFilter && !request.ActivityParameters.IsHostFilter)
      {
        activityDtos = activityDtos.Where(x => x.Attendees.Any(a => a.Username.Equals(currentLoggedInUserUsername)));
      }

      // Show only activities current user hosts
      if (request.ActivityParameters.IsHostFilter && !request.ActivityParameters.IsGoingFilter)
      {
        activityDtos = activityDtos.Where(x => x.HostUsername.Equals(currentLoggedInUserUsername));
      }

      // var shapedDtos = _dataShaper.ShapeDataMultipleEntities(activityDtos, request.ActivityParameters.Fields!);

      var pagedActivitiesDto = PagedList<ActivityDto>.ToPagedList(
          activityDtos,
          request.ActivityParameters.PageNumber,
          request.ActivityParameters.PageSize);

      return (pagedActivitiesDto: Result<PagedList<ActivityDto>>.Success(pagedActivitiesDto), metaData: pagedActivitiesDto.MetaData);
    }
  }
}
