import { makeAutoObservable, reaction, runInAction } from "mobx";
import agent from "../api/agent";
import { Photo, Profile, UserActivity } from "../models/profile";
import { baseStore } from "./baseStore";

export default class ProfileStore {
    profile: Profile | null = null;
    loadingProfileFlag = false;
    loadingFlag = false;
    uploadingFileFlag = false;
    loadingPhotoFlag = false;
    followings: Profile[] = [];
    loadingFollowingsFlag = false;
    activeTabIndex = 0;
    userActivities: UserActivity[] = [];
    loadingUserActivitiesFlag = false;

    constructor() {
        makeAutoObservable(this);

        reaction(
            () => this.activeTabIndex,
            (activeTabIndex) => {
                if (activeTabIndex === 3 || activeTabIndex === 4) {
                    const predicate =
                        activeTabIndex === 3 ? "followers" : "following";
                    this.loadFollowings(predicate);
                } else {
                    this.followings = [];
                }
            }
        );
    }

    get isCurrentUser() {
        if (baseStore.userStore.user && this.profile) {
            return baseStore.userStore.user.username === this.profile.username;
        }
        return false;
    }

    setActiveTab = (activeTabIndex: any) => {
        this.activeTabIndex = activeTabIndex;
    };

    loadProfile = async (username: string) => {
        this.setLoadingProfileStatus(true);
        try {
            const profile = await agent.Profiles.get(username);
            runInAction(() => {
                this.setProfile(profile);
                this.setLoadingProfileStatus(false);
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.setLoadingProfileStatus(false));
        }
    };

    uploadPhoto = async (file: Blob) => {
        this.setUploadingFileStatus(true);
        try {
            const response = await agent.Profiles.uploadPhoto(file);
            const photo = response.data;
            runInAction(() => {
                if (this.profile) {
                    // Add new photo to user's photo collection
                    this.profile.photos?.push(photo);
                    // Set main photo
                    if (photo.isMain && baseStore.userStore.user) {
                        baseStore.userStore.setImage(photo.url);
                        this.profile.image = photo.url;
                    }
                }
                this.setUploadingFileStatus(false);
            });
        } catch (error) {
            console.error(error);
            runInAction(() => this.setUploadingFileStatus(false));
        }
    };

    setMainPhoto = async (toBeMainPhoto: Photo) => {
        this.setLoadingPhotoStatus(true);
        try {
            await agent.Profiles.setMainPhoto(toBeMainPhoto.id);
            baseStore.userStore.setImage(toBeMainPhoto.url);
            runInAction(() => {
                if (this.profile && this.profile.photos) {
                    // Set the current main photo to false and the new photo to main
                    this.profile.photos.find((p) => p.isMain)!.isMain = false;
                    this.profile.photos.find(
                        (p) => p.id === toBeMainPhoto.id
                    )!.isMain = true;
                    this.profile.image = toBeMainPhoto.url;
                    this.setLoadingPhotoStatus(false);
                }
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.setLoadingPhotoStatus(false));
        }
    };

    deletePhoto = async (toBeDeletedPhoto: Photo) => {
        this.setLoadingPhotoStatus(true);
        try {
            await agent.Profiles.deletePhoto(toBeDeletedPhoto.id);
            runInAction(() => {
                if (this.profile) {
                    // Refresh photo collection without the just deleted photo
                    this.profile.photos = this.profile.photos?.filter(
                        (p) => p.id !== toBeDeletedPhoto.id
                    );
                    this.setLoadingPhotoStatus(false);
                }
            });
        } catch (error) {
            console.error(error);
            runInAction(() => this.setLoadingPhotoStatus(false));
        }
    };

    updateProfile = async (profileToUpdate: Partial<Profile>) => {
        this.setLoadingCommonStatus(true);
        try {
            await agent.Profiles.updateProfile(profileToUpdate);
            runInAction(() => {
                if (
                    profileToUpdate.displayName &&
                    profileToUpdate.displayName !==
                        baseStore.userStore.user?.displayName
                ) {
                    baseStore.userStore.setDisplayName(
                        profileToUpdate.displayName
                    );
                }
                this.profile = {
                    ...this.profile,
                    ...(profileToUpdate as Profile),
                };
                this.setLoadingCommonStatus(false);
            });
        } catch (error) {
            console.error(error);
            runInAction(() => this.setLoadingCommonStatus(false));
        }
    };

    toggleFollowing = async (
        username: string,
        isFollowingThisUser: boolean
    ) => {
        this.setLoadingCommonStatus(true);
        try {
            await agent.Profiles.updateFollowing(username);
            baseStore.activityStore.updateAttendeeFollowing(username);
            runInAction(() => {
                this.updateProfileFollowerCount(username, isFollowingThisUser);
                this.updateProfileFollowingCount(isFollowingThisUser);
                this.updateFollowerCountOfUsersProfileIsFollowing(username);
                this.setLoadingCommonStatus(false);
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.setLoadingCommonStatus(false));
        }
    };

    private updateProfileFollowerCount = (
        usernameToToggleFollow: string,
        isFollowingThisUser: boolean
    ) => {
        if (
            this.profile &&
            this.profile.username !== baseStore.userStore.user?.username &&
            // Prevent the profile from updating
            // when we are looking at a profile when trying to follow another user
            this.profile.username === usernameToToggleFollow
        ) {
            isFollowingThisUser
                ? this.profile.followersCount++
                : this.profile.followersCount--;

            this.profile.isFollowingThisUser = this.toggleFlagStatus(
                this.profile.isFollowingThisUser
            );
        }
    };

    private updateProfileFollowingCount = (isFollowingThisUser: boolean) => {
        // Update the following count of the current user
        if (
            this.profile &&
            this.profile.username === baseStore.userStore.user?.username
        ) {
            isFollowingThisUser
                ? this.profile.followingCount++
                : this.profile.followingCount--;
        }
    };

    private updateFollowerCountOfUsersProfileIsFollowing = (
        usernameToToggleFollow: string
    ) => {
        this.followings.forEach((profile) => {
            if (profile.username === usernameToToggleFollow) {
                profile.isFollowingThisUser
                    ? profile.followersCount--
                    : profile.followingCount++;

                profile.isFollowingThisUser = this.toggleFlagStatus(
                    profile.isFollowingThisUser
                );
            }
        });
    };

    loadFollowings = async (predicate: string) => {
        this.setLoadingFollowingsStatus(true);
        try {
            const followings = await agent.Profiles.listFollowings(
                this.profile?.username!,
                predicate
            );
            runInAction(() => {
                this.followings = followings;
                this.setLoadingFollowingsStatus(false);
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.setLoadingFollowingsStatus(false));
        }
    };

    loadUserActivities = async (username: string, predicate?: string) => {
        this.setLoadingUserActivitiesStatus(true);
        try {
            const userActivities = await agent.Profiles.listUserActivities(
                username,
                predicate!
            );
            runInAction(() => {
                this.userActivities = userActivities;
                this.setLoadingUserActivitiesStatus(false);
            });
        } catch (error) {
            console.log(error);
            runInAction(() => {
                this.setLoadingUserActivitiesStatus(false);
            });
        }
    };

    private toggleFlagStatus(flag: boolean) {
        return !flag;
    }

    private setLoadingFollowingsStatus(isLoading: boolean) {
        this.loadingFollowingsFlag = isLoading;
    }

    private setLoadingProfileStatus(isLoading: boolean) {
        this.loadingProfileFlag = isLoading;
    }

    private setLoadingCommonStatus(isLoading: boolean) {
        this.loadingFlag = isLoading;
    }

    private setUploadingFileStatus(isUploading: boolean) {
        this.uploadingFileFlag = isUploading;
    }

    private setLoadingPhotoStatus(isLoading: boolean) {
        this.loadingPhotoFlag = isLoading;
    }

    private setProfile(profile: Profile) {
        this.profile = profile;
    }

    private setLoadingUserActivitiesStatus(isLoading: boolean) {
        this.loadingUserActivitiesFlag = isLoading;
    }
}
