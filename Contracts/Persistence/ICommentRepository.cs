using Domain.Models;

namespace Contracts.Persistence
{
    public interface ICommentRepository
    {
        Task<IEnumerable<Comment>> GetAllActivityCommentsAsync(Guid activityId, bool trackChanges);
    }
}
