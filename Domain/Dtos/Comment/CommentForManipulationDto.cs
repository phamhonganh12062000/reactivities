namespace Domain.Dtos.Comment
{
    public record CommentForManipulationDto
    {
        public Guid ActivityId { get; init; }

        public string Body { get; init; }
    }
}
