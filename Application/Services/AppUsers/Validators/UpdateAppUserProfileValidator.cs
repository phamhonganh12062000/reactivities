﻿using Application.Services.AppUsers.Handlers;
using Domain.Dtos.AppUser;
using FluentValidation;
using FluentValidation.Results;

namespace Application.Services.AppUsers.Validators
{
    public sealed class UpdateAppUserProfileValidator : AbstractValidator<UpdateAppUserInfoCommand>
    {
        public UpdateAppUserProfileValidator()
        {
            RuleFor(x => x.ProfileForUpdateDto).SetValidator(new AppUserValidator());
        }

        public override ValidationResult Validate(ValidationContext<UpdateAppUserInfoCommand> context)
        {
            return context.InstanceToValidate.ProfileForUpdateDto is null
                ? new ValidationResult(new[]
                {
                    new ValidationFailure(nameof(ProfileForUpdateDto), nameof(ProfileForUpdateDto) + " object is null."),
                }) : base.Validate(context);
        }
    }
}
