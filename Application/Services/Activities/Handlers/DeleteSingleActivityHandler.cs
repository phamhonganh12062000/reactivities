﻿using Application.Caching;
using Application.Services.Activities.Notifications;
using Contracts.Persistence;
using Domain.Exceptions.Activities;
using MediatR;

namespace Application.Services.Activities.Handlers
{
  internal sealed class DeleteSingleActivityHandler : INotificationHandler<ActivityDeletedNotification>
  {
    private readonly IUnitOfWork _unitOfWork;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public DeleteSingleActivityHandler(IUnitOfWork unitOfWork, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task Handle(ActivityDeletedNotification notification, CancellationToken cancellationToken)
    {
      var activityCacheKey = CachingStatics.GetActivityById(notification.Id);

      _memoryCacheWrapper.Remove(activityCacheKey);

      var activity = await _unitOfWork.Activity.GetSingleActivityAsync(notification.Id, notification.TrackChanges) ?? throw new ActivityNotFoundException(notification.Id);

      _unitOfWork.Activity.DeleteActivity(activity);

      await _unitOfWork.SaveAsync();
    }
  }
}
