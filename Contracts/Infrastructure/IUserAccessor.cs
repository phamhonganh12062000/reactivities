﻿namespace Contracts.Infrastructure
{
    public interface IUserAccessor
    {
        string GetCurrentUserUsername();
    }
}
