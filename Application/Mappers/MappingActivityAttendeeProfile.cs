using AutoMapper;
using Contracts.Infrastructure;
using Domain.Dtos.Activity;
using Domain.Dtos.ActivityAttendee;
using Domain.Models;

namespace Application.Mappers
{
    public class MappingActivityAttendeeProfile : Profile
    {
        public MappingActivityAttendeeProfile()
        {
            CreateMap<ActivityAttendee, ActivityAttendeeDto>()
                .ForMember(d => d.DisplayName, o => o.MapFrom(s => s.AppUser.DisplayName))
                .ForMember(d => d.Username, o => o.MapFrom(s => s.AppUser.UserName))
                .ForMember(d => d.Bio, o => o.MapFrom(s => s.AppUser.Bio))
                .ForMember(d => d.Image, o => o.MapFrom(s => s.AppUser.Photos.FirstOrDefault(p => p.IsMain).Url))
                .ForMember(d => d.FollowersCount, o => o.MapFrom(s => s.AppUser.Followers.Count))
                .ForMember(d => d.FollowingCount, o => o.MapFrom(s => s.AppUser.Followings.Count))
                .ForMember(d => d.IsFollowingThisUser, o => o.MapFrom<AttendeeToAttendeeDtoResolver>());

            CreateMap<ActivityAttendee, UserActivityDto>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.Activity.Id))
                .ForMember(d => d.Date, o => o.MapFrom(s => s.Activity.Date))
                .ForMember(d => d.Title, o => o.MapFrom(s => s.Activity.Title))
                .ForMember(d => d.Category, o => o.MapFrom(s => s.Activity.Category))
                .ForMember(d => d.HostUsername, o => o.MapFrom(s => s.Activity.Attendees.FirstOrDefault(x => x.IsHost).AppUser.UserName));
        }
    }

    /// <summary>
    /// Custom resolver logic instead of using ProjectTo().
    /// Check if the current user is following any of his/her followers.
    /// </summary>
    public class AttendeeToAttendeeDtoResolver : IValueResolver<ActivityAttendee, ActivityAttendeeDto, bool>
    {
        private readonly IUserAccessor _userAccessor;

        public AttendeeToAttendeeDtoResolver(IUserAccessor userAccessor)
        {
            _userAccessor = userAccessor;
        }

        public bool Resolve(ActivityAttendee source, ActivityAttendeeDto destination, bool destMember, ResolutionContext context)
        {
            var currentUsername = _userAccessor.GetCurrentUserUsername();

            return source.AppUser.Followers.Any(x => x.Observer.UserName.Equals(currentUsername));
        }
    }
}
