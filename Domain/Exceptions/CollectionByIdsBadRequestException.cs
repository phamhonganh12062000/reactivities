﻿namespace Domain.Exceptions
{
    public sealed class CollectionByIdsBadRequestException : BadRequestException
    {
        public CollectionByIdsBadRequestException() : base("Parameters ids is null")
        {
        }
    }
}
