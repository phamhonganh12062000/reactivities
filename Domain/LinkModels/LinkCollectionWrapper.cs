namespace Domain.LinkModels
{
    /// <summary>
    /// A wrapper for a collection of links. For response representation purposes.
    /// </summary>
    /// <typeparam name="T">The type of the links in the collection.</typeparam>
    public class LinkWrapper<T> : LinkResourceBase
    {
        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// Gets or sets the collection of links.
        /// </summary>
        public List<T> ValueCollection { get; set; } = new List<T>();

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkWrapper{T}"/> class.
        /// </summary>
        public LinkWrapper()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkWrapper{T}"/> class with a link.
        /// </summary>
        /// <param name="value">The link to wrap.</param>
        public LinkWrapper(T value)
        {
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkWrapper{T}"/> class with a list of links.
        /// </summary>
        /// <param name="valueCollection">The list of links to wrap.</param>
        public LinkWrapper(List<T> valueCollection)
        {
            ValueCollection = valueCollection;
        }
    }
}
