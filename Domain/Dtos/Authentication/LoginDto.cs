﻿namespace Domain.Dtos.Authentication
{
    public record LoginDto
    {
        public string Email { get; init; }

        public string Password { get; init; }
    }
}
