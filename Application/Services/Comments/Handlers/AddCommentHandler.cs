using Application.Caching;
using AutoMapper;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Comment;
using Domain.Exceptions.Activities;
using Domain.Models;
using Domain.Results;
using MediatR;

namespace Application.Services.Comments.Handlers
{
  public sealed record AddCommentCommand(string Body, Guid ActivityId)
  : IRequest<Result<CommentDto>>
  {
  }

  public sealed class AddCommentHandler : IRequestHandler<AddCommentCommand, Result<CommentDto>>
  {
    private readonly IUnitOfWork _unitOfWork;

    private readonly IMapper _mapper;

    private readonly IUserAccessor _userAccessor;

    private readonly ILoggerManager _logger;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public AddCommentHandler(IUnitOfWork unitOfWork, IMapper mapper, IUserAccessor userAccessor, ILoggerManager logger, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _userAccessor = userAccessor;
      _logger = logger;
      _memoryCacheWrapper = memoryCacheWrapper;
    }

    public async Task<Result<CommentDto>> Handle(AddCommentCommand request, CancellationToken cancellationToken)
    {
      _memoryCacheWrapper.Remove(CachingStatics.GetActivityCommentsByActivityId(request.ActivityId));

      var activity = await _unitOfWork.Activity.GetSingleActivityWithComments(request.ActivityId, trackChanges: true) ?? throw new ActivityNotFoundException(request.ActivityId);

      var user = await _unitOfWork.AppUser.GetAppUserAsync(_userAccessor.GetCurrentUserUsername(), trackChanges: true);

      var comment = new Comment
      {
        Author = user,
        Activity = activity,
        Body = request.Body,
      };

      activity.Comments.Add(comment);

      var isSavedSuccess = await _unitOfWork.SaveAsync() > 0;

      if (!isSavedSuccess)
      {
        // TODO: Add new error message to ErrorMessages class here
        _logger.LogError("Failed to add new comment.");
        return Result<CommentDto>.Failure("Failed to add new comment.");
      }

      return Result<CommentDto>.Success(_mapper.Map<CommentDto>(comment));
    }
  }
}
