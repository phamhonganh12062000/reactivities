using Contracts.Infrastructure;
using Contracts.Logger;
using Domain.ConfigurationModels;
using Microsoft.Extensions.Options;
using PostmarkDotNet;
using PostmarkDotNet.Model;

namespace Infrastructure.Email
{
  public class EmailPostmark : IEmailSenderFactory
  {
    private readonly PostmarkConfiguration _postmarkConfiguration;
    private readonly SmtpConfiguration _smtpConfiguration;
    private readonly ILoggerManager _logger;

    public EmailPostmark(IOptions<PostmarkConfiguration> postmarkConfiguration, IOptions<SmtpConfiguration> smtpConfiguration, ILoggerManager logger)
    {
      _postmarkConfiguration = postmarkConfiguration.Value;
      _smtpConfiguration = smtpConfiguration.Value;
      _logger = logger;
    }

    public bool IsValid => !string.IsNullOrEmpty(_postmarkConfiguration.ServerToken) &&
                            !string.IsNullOrEmpty(_postmarkConfiguration.SenderEmail);

    public async Task SendEmailAsync(string email, string subject, string htmlMessage)
    {
      var message = new PostmarkMessage()
      {
        To = email,
        From = _postmarkConfiguration.SenderEmail,
        TrackOpens = true,
        Subject = subject,
        HtmlBody = htmlMessage,
        Headers = new HeaderCollection
        {
          new MailHeader("X-CUSTOM-HEADER", "Header content"),
        },
      };

      var client = new PostmarkClient(_postmarkConfiguration.ServerToken);
      var sendResult = await client.SendMessageAsync(message);

      if (sendResult.Status == PostmarkStatus.Success)
      {
        _logger.LogInfo($"Email sent successfully to {email}.");
      }
      else
      {
        _logger.LogWarning($"Failed to send email to {email}. Error: {sendResult.Message}");
      }
    }
  }
}
