# Setup SQLite Database

## Setups

-   (For VSCode) Install [SQLite extension by alexcvzz](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite)
-   Create a `Data` folder inside the `API` project so that SQLite can create a `.db` file inside it.

## Migrate data

```powershell
Set-Location ".\root\folder\with\solution\file"

dotnet ef migrations add NameOfMigration -s ".\path\to\startup\project\" -p ".\path\to\data-context\project\"


dotnet ef database update --connection "Data Source=/path/to/database.db" -s ".\path\to\startup\project\" -p ".\path\to\data-context\project\"
```

## Notes

The current version now has the funciton `SeedDatabase()`, so the database is auto updated _whenver a new migration is added_.

## Drop database

```powershell
Set-Location ".\root\folder\with\solution\file"

dotnet ef database drop -s ".\path\to\startup\project\" -p ".\path\to\data-context\project\"
```
