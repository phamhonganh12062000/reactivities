import axios, { AxiosError, AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { Activity, ActivityFormValues } from "../models/activity";
import { PaginatedResult } from "../models/pagination";
import { Photo, Profile, UserActivity } from "../models/profile";
import { User, UserFormValues } from "../models/user";
import { router } from "../router/Routes";
import { baseStore } from "../stores/baseStore";

const sleep = (delay: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
};

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

const responseBody = <T>(response: AxiosResponse<T>) => response.data;

axios.interceptors.request.use((config) => {
    const token = baseStore.commonStore.accessToken;
    if (token && config.headers) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

axios.interceptors.response.use(
    // Handle paginated result
    async (response) => {
        if (process.env.NODE_ENV === "development") {
            await sleep(1000);
        }
        const pagingMetaData = response.headers["pagination"];
        if (pagingMetaData) {
            response.data = new PaginatedResult(
                response.data,
                JSON.parse(pagingMetaData)
            );
            return response as AxiosResponse<PaginatedResult<any>>;
        }
        return response;
    },
    // Handle errors
    (error: AxiosError) => {
        const { data, status, config, headers } =
            error.response as AxiosResponse;
        switch (status) {
            case 400:
                if (
                    config.method === "get" &&
                    data.errors.hasOwnProperty("id")
                ) {
                    router.navigate("/not-found");
                }
                if (data.errors) {
                    const modalStateErrors = [];
                    for (const key in data.errors) {
                        if (data.errors[key]) {
                            modalStateErrors.push(data.errors[key]);
                        }
                    }
                    throw modalStateErrors.flat();
                } else {
                    toast.error(data);
                }
                break;
            case 401:
                if (
                    status === 401 &&
                    headers["www-authenticate"]?.startsWith(
                        'Bearer error="invalid_token"'
                    )
                ) {
                    baseStore.userStore.logout();
                    toast.error("Session expired - please login again");
                }
                break;
            case 403:
                toast.error("Forbidden");
                break;
            case 404:
                router.navigate("/not-found");
                break;
            case 422:
                if (data.errors) {
                    const modalStateErrors = [];
                    for (const key in data.errors) {
                        if (data.errors[key]) {
                            modalStateErrors.push(data.errors[key]);
                        }
                    }
                    // Flat nested arrays inside modalStateErrors into a single-level array
                    throw modalStateErrors.flat();
                } else {
                    toast.error(data);
                }
                break;
            case 500:
                baseStore.commonStore.setServerError(data);
                router.navigate("/server-error");
                break;
            default:
                break;
        }
        return Promise.reject(error);
    }
);

const requests = {
    get: <T>(url: string) => axios.get<T>(url).then(responseBody),
    post: <T>(url: string, body: {}) =>
        axios.post<T>(url, body).then(responseBody),
    put: <T>(url: string, body: {}) =>
        axios.put<T>(url, body).then(responseBody),
    delete: <T>(url: string) => axios.delete<T>(url).then(responseBody),
};

const Activities = {
    list: (params: URLSearchParams) =>
        axios
            .get<PaginatedResult<Activity[]>>("/activities", { params })
            .then(responseBody),
    details: (id: string) => requests.get<Activity>(`/activities/${id}`),
    create: (activity: ActivityFormValues) =>
        requests.post<void>("/activities", activity),
    update: (activity: ActivityFormValues) =>
        requests.put<void>(`/activities/${activity.id}`, activity),
    delete: (id: string) => requests.delete<void>(`/activities/${id}`),
    attend: (id: string) => requests.put<void>(`activities/${id}/attend`, {}),
};

const Accounts = {
    current: () => requests.get<User>("/account"),
    login: (user: UserFormValues) =>
        requests.post<User>("/account/login", user),
    register: (user: UserFormValues) =>
        requests.post<User>("/account/register", user),
    facebookLogin: (accessToken: string) =>
        requests.post<User>(
            `/account/facebookLogin?accessToken=${accessToken}`,
            {}
        ),
    refreshTokens: () => requests.post<User>("/account/refreshTokens", {}),
    verifyEmail: (token: string, email: string) =>
        requests.post<void>(
            `/account/verifyEmail?token=${token}&email=${email}`,
            {}
        ),
    resendEmailConfirm: (email: string) =>
        requests.get(`/account/resendEmailConfirmationLink?email=${email}`),
};

const Profiles = {
    get: (username: string) => requests.get<Profile>(`/profiles/${username}`),
    uploadPhoto: (file: Blob) => {
        let formData = new FormData();
        formData.append("File", file);
        return axios.post<Photo>("photos", formData, {
            headers: { "Content-type": "multipart/form-data" },
        });
    },
    setMainPhoto: (id: string) => requests.put(`/photos/${id}/setMain`, {}),
    deletePhoto: (id: string) => requests.delete(`/photos/${id}`),
    updateProfile: (profile: Partial<Profile>) =>
        requests.put(`/profiles`, profile),
    updateFollowing: (username: string) =>
        requests.post(`follow/${username}`, []),
    listFollowings: (username: string, predicate: string) =>
        requests.get<Profile[]>(`/follow/${username}?predicate=${predicate}`),
    listUserActivities: (username: string, predicate: string) =>
        requests.get<UserActivity[]>(
            `/profiles/${username}/activities?predicate=${predicate}`
        ),
};

const agent = {
    Activities,
    Accounts,
    Profiles,
};

export default agent;
