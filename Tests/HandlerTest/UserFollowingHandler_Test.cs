﻿using Application.Caching;
using Application.Services.Followers.Handlers;
using AutoMapper;
using Contracts.Extensions;
using Contracts.Infrastructure;
using Contracts.Logger;
using Contracts.Persistence;
using Domain.Dtos.Activity;
using Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.HandlerTest
{
  public class UserFollowingHandler_Test
  {
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private readonly Mock<IMapper> _mockMapper;
    private readonly Mock<IDataShaper<ActivityDto>> _mockDataShaper;
    private readonly Mock<IUserAccessor> _mockUserAccessor;
    private readonly Mock<ILoggerManager> _mockLogger;
    private readonly Mock<IMemoryCacheWrapper> _mockMemoryCacheWrapper;

    public UserFollowingHandler_Test()
    {
      _mockUnitOfWork = new Mock<IUnitOfWork>();
      _mockMapper = new Mock<IMapper>();
      _mockDataShaper = new Mock<IDataShaper<ActivityDto>>();
      _mockUserAccessor = new Mock<IUserAccessor>();
      _mockLogger = new Mock<ILoggerManager>();
      _mockMemoryCacheWrapper = new Mock<IMemoryCacheWrapper>();
    }

    [Fact]
    public async Task ToggleFollowing_Success_FollowAnUser()
    {
      // ARRANGE
      var handler = new ToggleFollowingHandler(
          _mockUnitOfWork.Object,
          _mockUserAccessor.Object,
          _mockLogger.Object,
          _mockMemoryCacheWrapper.Object);

      var observerId = Guid.NewGuid().ToString();

      var observer = new AppUser
      {
        Id = observerId,
        UserName = "ObserverUsername",
      };

      var targetId = Guid.NewGuid().ToString();

      var target = new AppUser
      {
        Id = targetId,
        UserName = "TargetUsername",
      };

      var following = new UserFollowing
      {
        ObserverId = observerId,
        Observer = observer,
        TargetId = targetId,
        Target = target,
      };

      var request = new ToggleFollowingCommand(target.UserName, true);

      _mockUserAccessor.Setup(a => a.GetCurrentUserUsername()).Returns(observer.UserName);
      _mockUnitOfWork.Setup(x => x.AppUser.GetAppUserAsync(observer.UserName, request.UserTrackChanges)).ReturnsAsync(observer);
      _mockUnitOfWork.Setup(x => x.AppUser.GetAppUserAsync(request.TargetUsername, request.UserTrackChanges)).ReturnsAsync(target);
      _mockUnitOfWork.Setup(x => x.UserFollowing.GetUserFollowingAsync(observerId, targetId, request.UserTrackChanges)).ReturnsAsync(following);
      _mockUnitOfWork.Setup(x => x.UserFollowing.AddFollowingAsync(following));
      _mockUnitOfWork.Setup(x => x.UserFollowing.RemoveFollowing(following));
      _mockUnitOfWork.Setup(x => x.SaveAsync()).ReturnsAsync(1);

      // ACT
      var result = await handler.Handle(request, CancellationToken.None);

      // ASSERT
      Assert.True(result.IsSuccess);
    }

    [Fact]
    public async Task GetFollowerList_GetFollowers_ReturnListOfFollowers()
    {
      // ARRANGE
      var handler = new GetFollowerListHandler(
          _mockUnitOfWork.Object,
          _mockMapper.Object,
          _mockMemoryCacheWrapper.Object);

      var request = new GetFollowerListQuery("testUsername", "followers", false);

      var profiles = new List<AppUser>
            {
                new AppUser
                {
                    UserName = "testUsername1",
                },
                new AppUser
                {
                    UserName = "testUsername2",
                },
                new AppUser
                {
                    UserName = "testUsername3",
                },
            };

      var profilesDto = new List<ProfileDto>
            {
                new ProfileDto
                {
                    Username = "testUsername1",
                },
                new ProfileDto
                {
                    Username = "testUsername2",
                },
                new ProfileDto
                {
                    Username = "testUsername3",
                },
            };

      _mockUnitOfWork.Setup(u => u.UserFollowing.GetUserFollowerListAsync(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(profiles);
      _mockMapper.Setup(m => m.Map<List<ProfileDto>>(It.IsAny<List<AppUser>>())).Returns(profilesDto);

      // ACT
      var result = await handler.Handle(request, CancellationToken.None);

      // ASSERT
      Assert.Equal(profilesDto, result.Value);
    }
  }
}
