# # Stage 1: Build the frontend
# FROM node:14 AS frontend
# WORKDIR /app
# COPY client-app/package*.json ./
# RUN npm install
# COPY client-app/ ./
# RUN npm run build

# Stage 2: Build the backend
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS backend
WORKDIR /app
EXPOSE 8080

# Copy .csproj and restore as distinct layer
COPY "Reactivities.sln" "Reactivities.sln"
COPY "API/API.csproj" "API/API.csproj"
COPY "Application/Application.csproj" "Application/Application.csproj"
COPY "Contracts/Contracts.csproj" "Contracts/Contracts.csproj"
COPY "Domain/Domain.csproj" "Domain/Domain.csproj"
COPY "Infrastructure/Infrastructure.csproj" "Infrastructure/Infrastructure.csproj"
COPY "Logger/Logger.csproj" "Logger/Logger.csproj"
COPY "Persistence/Persistence.csproj" "Persistence/Persistence.csproj"
COPY "Presentation/Presentation.csproj" "Presentation/Presentation.csproj"
COPY "Shared/Shared.csproj" "Shared/Shared.csproj"
COPY "Tests/Tests.csproj" "Tests/Tests.csproj"

RUN dotnet restore "Reactivities.sln"
# COPY --from=frontend /app/build ./API/wwwroot

# Copy everything else
COPY . .
WORKDIR /app
# RUN dotnet test -c Release
RUN dotnet publish -c Release -o out

# Stage 3: Build a runtime image
FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=backend /app/out .

ENV ASPNETCORE_ENVIRONMENT=Production
ENTRYPOINT [ "dotnet", "API.dll" ]
