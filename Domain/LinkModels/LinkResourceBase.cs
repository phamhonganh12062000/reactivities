namespace Domain.LinkModels
{
    /// <summary>
    /// Contain all the links.
    /// </summary>
    public class LinkResourceBase
    {
        public LinkResourceBase()
        {
        }

        public List<Link> Links { get; set; } = new List<Link>();
    }
}
