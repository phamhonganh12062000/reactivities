import { makeAutoObservable, reaction } from "mobx";
import { ServerError } from "../models/serverError";

export default class CommonStore {
    error: ServerError | null = null;
    accessToken: string | null = localStorage.getItem("jwt");
    isAppLoaded = false;

    constructor() {
        makeAutoObservable(this);

        reaction(
            () => this.accessToken,
            (accessToken) => {
                if (accessToken) {
                    localStorage.setItem("jwt", accessToken);
                } else {
                    localStorage.removeItem("jwt");
                }
            }
        );
    }

    setServerError(error: ServerError) {
        this.error = error;
    }

    setAccessToken = (accessToken: string | null) => {
        this.accessToken = accessToken;
    };

    setAppLoaded = () => {
        this.isAppLoaded = true;
    };
}
