﻿using MediatR;

namespace Application.Services.Activities.Notifications
{
    public sealed record ActivityDeletedNotification(Guid Id, bool TrackChanges) : INotification
    {
    }
}
