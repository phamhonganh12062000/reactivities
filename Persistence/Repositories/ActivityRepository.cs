﻿using Contracts.Persistence;
using Domain.Models;
using Domain.ParameterModels;
using Microsoft.EntityFrameworkCore;
using Persistence.Extensions;
using Shared.RequestFeatures;

namespace Persistence.Repositories
{
    public class ActivityRepository : RepositoryBase<Activity>, IActivityRepository
    {
        public ActivityRepository(DataContext context)
            : base(context)
        {
        }

        public IQueryable<Activity> GetAllActivities(ActivityParameters activityParameters, bool trackChanges)
        {
            // NOTE: Order of method calls in LINQ is IMPORTANT to return the smallest possible dataset from queries
            return GetAll(trackChanges)

                // Select activities that occur on or after the specified date
                .Where(d => d.Date >= activityParameters.StartDate)
                .Filter(activityParameters.CategoryFilter)
                .Search(activityParameters.SearchTerm)

                // Default sorting is by date
                .Sort(activityParameters.OrderBy)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(p => p.Photos)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(fer => fer.Followers).ThenInclude(obs => obs.Observer)
                .AsQueryable();

        }

        public async Task<IEnumerable<Activity>> GetMultipleActivitiesAsync(IEnumerable<Guid> ids, bool trackChanges)
        {
            return await Find(a => ids.Contains(a.Id), trackChanges)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(p => p.Photos)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(fer => fer.Followers).ThenInclude(obs => obs.Observer)
                .ToListAsync();
        }

        public async Task<Activity> GetSingleActivityAsync(Guid id, bool trackChanges)
        {
            return await Find(a => a.Id.Equals(id), trackChanges)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(p => p.Photos)
                .Include(a => a.Attendees).ThenInclude(u => u.AppUser).ThenInclude(fer => fer.Followers).ThenInclude(obs => obs.Observer)
                .SingleOrDefaultAsync();
        }

        public async Task CreateActivityAsync(Activity activity)
        {
            await CreateAsync(activity);
        }

        public void DeleteActivity(Activity activity)
        {
            Delete(activity);
        }

        public async Task<Activity> GetSingleActivityWithComments(Guid id, bool trackChanges)
        {
            return await Find(a => a.Id.Equals(id), trackChanges)
                .Include(c => c.Comments)
                .ThenInclude(a => a.Author)
                .ThenInclude(p => p.Photos)
                .FirstOrDefaultAsync();
        }
    }
}
