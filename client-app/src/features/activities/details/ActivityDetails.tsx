import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Grid } from "semantic-ui-react";
import LoadingComponent from "../../../app/layout/LoadingComponent";
import { useStore } from "../../../app/stores/baseStore";
import ActivityDetailChat from "./ActivityDetailChat";
import ActivityDetailHeader from "./ActivityDetailHeader";
import ActivityDetailInfo from "./ActivityDetailInfo";
import ActivityDetailSideBar from "./ActivityDetailSideBar";

export default observer(function ActivityDetails() {
    const { activityStore } = useStore();
    const {
        selectedActivity: activity,
        loadSingleActivity,
        loadingInitial,
        clearSelectedActivity,
    } = activityStore;
    const { id } = useParams();

    useEffect(() => {
        if (id) {
            loadSingleActivity(id);
            // Clear activity after switching to another one to avoid failing SignalR connection
            return () => clearSelectedActivity();
        }
    }, [id, loadSingleActivity, clearSelectedActivity]);

    if (loadingInitial || !activity) return <LoadingComponent content={""} />;

    return (
        <Grid>
            <Grid.Column width={10}>
                <ActivityDetailHeader activity={activity} />
                <ActivityDetailInfo activity={activity} />
                <ActivityDetailChat activityId={activity.id} />
            </Grid.Column>
            <Grid.Column width={6}>
                <ActivityDetailSideBar activity={activity} />
            </Grid.Column>
        </Grid>
    );
});
