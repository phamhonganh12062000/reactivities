# Deploying on Docker

---

## Build production version of the frontned

```bash
cd .\client-app\ && npm run build

```

## Create an image

-   Add a Dockerfile and `.dockerignore` file
-   Build an image

```bash
cd /root
# NOTE:The published version of your application will be stored inside the image itself
# Add a --no-cache before build a new an entire new image without depending on the previous one
docker build -t honganhpham/reactivities .
```

## Create a container

```bash
# NOTE: Must add host-gateway for Linux machines to resolve hostname
docker run --add-host host.docker.internal:host-gateway --rm -it -p 8081:80  --name reactivities honganhpham/reactivities
```

## Push the image to Docker Hub

-   [Basically follow this comment on StackOverflow](https://stackoverflow.com/a/44982036)

1. Login to Docker

```bash
docker login -u my-username
```

2. Tag your image build + Create a repository on Docker Hub

```bash
docker tag mylocalimage:latest my-username/my-docker-repo:my-image-tag
```

3. Push to Docker Hub

```bash
docker push my-username/my-docker-repo:my-image-tag
```
