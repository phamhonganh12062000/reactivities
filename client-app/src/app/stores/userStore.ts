import { makeAutoObservable, runInAction } from "mobx";
import agent from "../api/agent";
import { User, UserFormValues } from "../models/user";
import { router } from "../router/Routes";
import { baseStore } from "./baseStore";

export default class UserStore {
    user: User | null = null;
    fbLoading = false;
    fbAccessToken: string | null = null;
    refreshTokenTimeout: any;

    constructor() {
        makeAutoObservable(this);
    }

    get isLoggedIn() {
        // Convert falsy/truthy value to a strict boolean
        return !!this.user;
    }

    login = async (credentials: UserFormValues) => {
        try {
            const user = await agent.Accounts.login(credentials);
            baseStore.commonStore.setAccessToken(user.accessToken);
            this.startRefreshTokenTimer(user);
            runInAction(() => (this.user = user));
            router.navigate("/activities");
            baseStore.modalStore.closeModal();
        } catch (error) {
            throw error;
        }
    };

    logout = () => {
        baseStore.commonStore.setAccessToken(null);
        this.user = null;
        router.navigate("/");
    };

    register = async (credentials: UserFormValues) => {
        try {
            await agent.Accounts.register(credentials);
            router.navigate(
                `/account/registerSuccess?email=${credentials.email}`
            );
            baseStore.modalStore.closeModal();
        } catch (error) {
            throw error;
        }
    };

    getUser = async () => {
        try {
            const user = await agent.Accounts.current();
            baseStore.commonStore.setAccessToken(user.accessToken);
            runInAction(() => (this.user = user));
            // TODO: Restart the token every time we reload the page
            this.startRefreshTokenTimer(user);
        } catch (error) {
            throw error;
        }
    };

    setImage = (image: string) => {
        if (this.user) {
            this.user.image = image;
        }
    };

    setDisplayName = (name: string) => {
        if (this.user) {
            this.user.displayName = name;
        }
    };

    getFacebookLoginStatus = async () => {
        window.FB.getLoginStatus((response) => {
            if (response.status === "connected") {
                this.fbAccessToken = response.authResponse.accessToken!;
            }
        }, true);
    };

    facebookLogin = async () => {
        this.fbLoading = true;
        const apiLogin = (accessToken: string) => {
            agent.Accounts.facebookLogin(accessToken)
                .then((user) => {
                    baseStore.commonStore.setAccessToken(user.accessToken);
                    runInAction(() => {
                        this.user = user;
                        this.fbLoading = false;
                    });
                    router.navigate("/activities");
                })
                .catch((error) => {
                    console.log(error);
                    runInAction(() => (this.fbLoading = false));
                });
        };
        if (this.fbAccessToken) {
            apiLogin(this.fbAccessToken);
        } else {
            window.FB.login(
                (response) => {
                    apiLogin(response.authResponse.accessToken!);
                },
                { scope: "public_profile,email" }
            );
        }
    };

    refreshTokens = async () => {
        this.stopRefreshTokenTimer();
        try {
            const userWithRefreshedTokens =
                await agent.Accounts.refreshTokens();
            runInAction(() => {
                this.user = userWithRefreshedTokens;
            });
            baseStore.commonStore.setAccessToken(
                userWithRefreshedTokens.accessToken
            );
            this.startRefreshTokenTimer(userWithRefreshedTokens);
        } catch (error) {
            console.log(error);
        }
    };

    private startRefreshTokenTimer(user: User) {
        const jwtToken = JSON.parse(atob(user.accessToken.split(".")[1]));
        const expires = new Date(jwtToken.exp * 1000);
        const timeout = expires.getTime() - Date.now() - 60 * 1000;
        this.refreshTokenTimeout = setTimeout(
            () => this.refreshTokens(),
            timeout
        );
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }
}
