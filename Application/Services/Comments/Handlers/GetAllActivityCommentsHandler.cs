using Application.Caching;
using AutoMapper;
using Contracts.Persistence;
using Domain.Dtos.Comment;
using Domain.Results;
using MediatR;

namespace Application.Services.Comments.Handlers
{
  public sealed record GetAllActivityCommentsQuery(Guid ActivityId, bool CommentTrackChanges)
  : IRequest<Result<IEnumerable<CommentDto>>>
  {
  }

  public sealed class GetAllActivityCommentsHandler :
  IRequestHandler<GetAllActivityCommentsQuery, Result<IEnumerable<CommentDto>>>
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    private readonly IMemoryCacheWrapper _memoryCacheWrapper;

    public GetAllActivityCommentsHandler(IUnitOfWork unitOfWork, IMapper mapper, IMemoryCacheWrapper memoryCacheWrapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _memoryCacheWrapper = memoryCacheWrapper;

    }

    public async Task<Result<IEnumerable<CommentDto>>> Handle(GetAllActivityCommentsQuery request, CancellationToken cancellationToken)
    {
      var commentsCacheKey = CachingStatics.GetActivityCommentsByActivityId(request.ActivityId);

      var comments = await _memoryCacheWrapper.GetOrCreateAsync(commentsCacheKey, () => _unitOfWork.Comment.GetAllActivityCommentsAsync(request.ActivityId, request.CommentTrackChanges), null);

      var commentsDto = _mapper.Map<IEnumerable<CommentDto>>(comments);

      return Result<IEnumerable<CommentDto>>.Success(commentsDto);
    }
  }
}
