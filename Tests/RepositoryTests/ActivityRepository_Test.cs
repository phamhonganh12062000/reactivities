﻿using Contracts.Persistence;
using Domain.Models;
using Moq;

namespace Tests.RepositoryTests
{
    public class ActivityRepository_Test
    {
        private readonly Mock<IActivityRepository> _mockActivityRepo;

        public ActivityRepository_Test()
        {
            _mockActivityRepo = new Mock<IActivityRepository>();
        }

        [Fact]
        public async Task GetAllActivities_GetAllActivities_ReturnQueryableListOfActivities()
        {
            // ARRANGE
            var activitiyList = Enumerable.Empty<Activity>().AsQueryable();

            _mockActivityRepo.Setup(repo => repo.GetAllActivities(null, false))
                .Returns(activitiyList);

            // ACT
            var result = _mockActivityRepo.Object.GetAllActivities(null, false);

            // ASSERT
            Assert.IsAssignableFrom<IQueryable<Activity>>(result);
        }

        [Fact]
        public async Task GetSingleActivityAsync_GetSingleActivity_ReturnSingleActivity()
        {
            // ARRANGE
            var activityId = Guid.NewGuid();

            var expectedResult = new Activity
            {
                Id = activityId,
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            };

            _mockActivityRepo.Setup(repo => repo.GetSingleActivityAsync(activityId, false))
                .ReturnsAsync(expectedResult);

            // ACT
            var result = await _mockActivityRepo.Object.GetSingleActivityAsync(activityId, false);

            // ASSERT
            Assert.IsType<Activity>(result);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task CreateActivityAsync_AddNewActivity_ShouldAddSingleActivity()
        {
            // ARRANGE
            var expectedResult = new Activity
            {
                Id = Guid.NewGuid(),
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            };

            // ACT
            await _mockActivityRepo.Object.CreateActivityAsync(expectedResult);

            // ASSERT
            _mockActivityRepo.Verify(repo => repo.CreateActivityAsync(It.IsAny<Activity>()), Times.Once);
        }

        [Fact]
        public void DeleteActivity_RemoveActivity_ShouldDeleteSingleActivity()
        {
            // ARRANGE
            var expectedResult = new Activity
            {
                Id = Guid.NewGuid(),
                Title = "Past Activity 1",
                Date = DateTime.UtcNow.AddMonths(-2),
                Description = "Activity 2 months ago",
                Category = "drinks",
                City = "London",
                Venue = "Pub",
            };

            // ACT
            _mockActivityRepo.Object.DeleteActivity(expectedResult);

            // ASSERT
            _mockActivityRepo.Verify(repo => repo.DeleteActivity(It.IsAny<Activity>()), Times.Once);
        }
    }
}
