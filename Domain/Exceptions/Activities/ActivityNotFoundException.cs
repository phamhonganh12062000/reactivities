﻿namespace Domain.Exceptions.Activities
{
    public sealed class ActivityNotFoundException : NotFoundException
    {
        public ActivityNotFoundException(Guid id)
            : base($"The activity with id: {id} does not exist in the database.")
        {
        }
    }
}
