﻿using Application.Services.Activities.Notifications;
using Contracts.Logger;
using MediatR;

namespace Application.Services.Activities.Extensions
{
    internal sealed class EmailHandler : INotificationHandler<ActivityDeletedNotification>
    {
        private readonly ILoggerManager _logger;

        public EmailHandler(ILoggerManager logger)
        {
            _logger = logger;
        }

        public async Task Handle(ActivityDeletedNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogWarning($"Delete action for the activity with id: {notification.Id} has occured.");

            await Task.CompletedTask;
        }
    }
}
