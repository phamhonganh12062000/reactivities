﻿using Application.Caching;
using Application.Services.Comments.Handlers;
using Contracts;
using MediatR;
using Microsoft.AspNetCore.SignalR;

namespace API;
public class ChatHub : Hub<IChatHubClient>
{
  private readonly IMediator _mediator;

  public ChatHub(IMediator mediator)
  {
    _mediator = mediator;
  }

  public async Task SendComment(AddCommentCommand command)
  {
    var comment = await _mediator.Send(command);

    await Clients.Group(command.ActivityId.ToString())
        .ReceiveComment(comment.Value);
  }

  public override async Task OnConnectedAsync()
  {
    var httpContext = Context.GetHttpContext();

    var activityId = httpContext.Request.Query["activityId"];

    await Groups.AddToGroupAsync(Context.ConnectionId, activityId);

    var cachedComments = await _mediator.Send(new GetAllActivityCommentsQuery(Guid.Parse(activityId), CommentTrackChanges: true));

    await Clients.Caller.LoadComments(cachedComments.Value);
  }
}
