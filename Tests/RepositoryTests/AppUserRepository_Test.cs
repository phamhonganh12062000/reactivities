using Contracts.Persistence;
using Domain.Models;
using Moq;

namespace Tests
{
  public class AppUserRepository_Test
  {
    private readonly Mock<IAppUserRepository> _mockAppUserRepo;

    public AppUserRepository_Test()
    {
      _mockAppUserRepo = new Mock<IAppUserRepository>();
    }

    [Fact]
    public async Task AppUserRepository_GetUserByUsernameAsync_ReturnSingleUser()
    {
      // ARRANGE
      var testUsername = "Test Username";

      var testUser = new AppUser
      {
        UserName = testUsername,
      };

      _mockAppUserRepo.Setup(r => r.GetAppUserAsync(testUsername, false)).ReturnsAsync(testUser);

      // ACT
      var result = await _mockAppUserRepo.Object.GetAppUserAsync(testUsername, false);

      // ASSERT
      Assert.IsType<AppUser>(result);
      Assert.NotNull(result);
    }

    [Fact]
    public async Task AppUserRepository_GetUserByUsernameWithPhotosAsync_ReturnSingleUserWithPhotos()
    {
      var testUsername = "Test Username";

      var testPhotoList = new List<Photo>
            {
                new () { Url = "Url1" },
                new () { Url = "Url2" },
                new () { Url = "Url3" },
            };

      var testUser = new AppUser
      {
        UserName = testUsername,
        Photos = testPhotoList,
      };

      _mockAppUserRepo.Setup(r => r.GetAppUserAsync(testUsername, false)).ReturnsAsync(testUser);

      // ACT
      var result = await _mockAppUserRepo.Object.GetAppUserAsync(testUsername, false);

      // ASSERT
      Assert.IsType<AppUser>(result);
      Assert.IsType<List<Photo>>(result.Photos);
      Assert.NotNull(result);
    }
  }
}
