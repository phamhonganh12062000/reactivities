﻿using Presentation.Formatters;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Persistence;
using FluentValidation.AspNetCore;
using Shared.Static;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Infrastructure.Security;
using Domain.ConfigurationModels;
using Microsoft.AspNetCore.Mvc.Formatters;
using Npgsql;
using Contracts.Application.Services;
using Infrastructure.HealthCheck;
using Azure.Security.KeyVault.Secrets;
using Azure.Core;
using Serilog;
using Azure.Identity;

namespace API.Extensions
{
  public static class Configurations
  {
    public static void ConfigureCors(this IServiceCollection services)
    {
      services.AddCors(opts =>
      {
        opts.AddPolicy("CorsPolicy", builder =>
                  builder.WithOrigins("http://localhost:3000", "https://localhost:3000")
                  .WithExposedHeaders(Headers.WWWAuthenticate, Headers.Pagination)
                  .AllowAnyMethod()
                  .AllowAnyHeader()
                  .AllowCredentials());
      });
    }

    public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration, Serilog.ILogger logger)
    {
      services.AddDbContext<DataContext>(options
              => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));
    }

    public static void ConfigureFluentValidators(this IServiceCollection services)
    {
      services.AddFluentValidationAutoValidation();
      services.AddValidatorsFromAssembly(typeof(Application.AssemblyReference).Assembly);
    }

    public static void ConfigureCustomResponses(this IServiceCollection services)
    {
      services.Configure<ApiBehaviorOptions>(opts =>
      {
        //opts.SuppressModelStateInvalidFilter = true;
      });
    }

    public static IMvcBuilder ConfigureCustomCSVFormatter(this IMvcBuilder builder)
    {
      return builder.AddMvcOptions(config => config.OutputFormatters.Add(new CsvOutputFormatter()));
    }

    public static void ConfigureIdentity(this IServiceCollection services)
    {
      var builder = services.AddIdentityCore<AppUser>(opt =>
      {
        opt.Password.RequireNonAlphanumeric = false;
        opt.User.RequireUniqueEmail = true;
        opt.SignIn.RequireConfirmedEmail = true;
      })
          .AddEntityFrameworkStores<DataContext>()
          .AddSignInManager<SignInManager<AppUser>>()
          .AddDefaultTokenProviders();

      services.AddAuthentication();
    }

    public static void ConfigureJWT(this IServiceCollection services, IConfiguration configuration)
    {
      services.Configure<JwtConfiguration>(configuration.GetSection("JwtSettings"));
      var jwtConfiguration = new JwtConfiguration();

      // Map config values to respective properties inside the Jwt configuration class
      configuration.Bind(jwtConfiguration.Section, jwtConfiguration);

      // NOTE: This cannot be bound using Bind() method on the IConfiguration as it is not a property of the JwtSettings
      string secretKey = configuration["JWTSecretKey"];

      // Register the JWT authentication middleware
      services.AddAuthentication(opt =>
      {
        opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      })
          .AddJwtBearer(opt =>
          {
            opt.TokenValidationParameters = new TokenValidationParameters
            {
              ValidateIssuer = false,  // Check if issuer is the actual server that created the token
              ValidateAudience = false, // Check if the receiver of the token is a valid recipient
              ValidateLifetime = true, // Verify the token has not expired
              ValidateIssuerSigningKey = true, // Check if signing key is valid + trusted by the server
              IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)),
              ClockSkew = TimeSpan.Zero, // Buffer period in which the token is still considered valid after its expiration time
            };

            // Allow authenticating to SignalR
            opt.Events = new JwtBearerEvents
            {
              OnMessageReceived = (context) =>
              {
                var accessToken = context.Request.Query["access_token"];

                var path = context.HttpContext.Request.Path;

                if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/chat"))
                {
                  context.Token = accessToken;
                }

                return Task.CompletedTask;
              },
            };
          });

      // Custom authorization policy
      services.AddAuthorization(opt =>
      {
        opt.AddPolicy("IsActivityHost", policy =>
        {
          policy.Requirements.Add(new HostRequirement());
        });
      });
    }

    public static void ConfigureCloudinary(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
    {
      services.Configure<CloudinaryConfiguration>(configuration.GetSection("CloudinarySettings"));
    }

    public static void ConfigureCustomMediaTypes(this IServiceCollection services)
    {
      services.Configure<MvcOptions>(config =>
      {
        var systemTextJsonOutputFormatter = config.OutputFormatters
                  .OfType<SystemTextJsonOutputFormatter>()?.FirstOrDefault();

        systemTextJsonOutputFormatter?.SupportedMediaTypes
                    .Add(Headers.HATEOAS_JSON);

        var xmlOutputFormatter = config.OutputFormatters
                  .OfType<XmlDataContractSerializerOutputFormatter>()?
                  .FirstOrDefault();

        xmlOutputFormatter?.SupportedMediaTypes
                    .Add(Headers.HATEOAS_XML);
      });
    }

    public static void ConfigureSignalR(this IServiceCollection services, Serilog.ILogger logger)
    {
      services.AddSignalR(hubOptions =>
      {
        if (hubOptions?.SupportedProtocols is not null)
        {
          foreach (var protocol in hubOptions.SupportedProtocols)
          {
            logger.Debug($"SignalR supports {protocol} protocol");
          }
        }
      });
    }

    // Global configs for caching validation
    public static void ConfigureHttpCacheHeaders(this IServiceCollection services) => services.AddHttpCacheHeaders();

    public static void ConfigureFacebookLogin(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
    {
      services.Configure<FacebookConfiguration>(configuration.GetSection("FacebookSettings"));
    }

    public static void ConfigureHttpClient(this IServiceCollection services) => services.AddHttpClient<IFacebookLoginService>();

    public static void ConfigureHealthCheck(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
    {
      services.AddHealthChecks()

        // Check if the connection to the db is healthy
        .AddNpgSql(configuration.GetConnectionString("DefaultConnection"), tags: new[] { "database" })

        // Check if DbContext can connect to the db
        .AddCheck<DbContextHealthCheck<DataContext>>(nameof(DataContext), tags: new[] { "dbContext" });

      services.AddHealthChecksUI().AddInMemoryStorage();
    }

    public static void ConfigureSerilog(this IHostBuilder hostBuilder)
    {
      hostBuilder.UseSerilog((context, services, configuration) => configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services)
        .Enrich.FromLogContext());
    }

    public static void ConfigureAzureIntegration(this IHostBuilder hostBuilder, IConfiguration configuration)
    {
      hostBuilder.ConfigureAppConfiguration((context, configBuilder) =>
      {
        TokenCredential azureCredentials;

        var azureConfigurations = configuration.GetSection(nameof(AzureConfiguration)).Get<AzureConfiguration>()
        ?? throw new InvalidOperationException($"{nameof(AzureConfiguration)} settings not found in configuration.");

        if (context.HostingEnvironment.IsDevelopment())
        {
          configBuilder.AddUserSecrets<Program>();
          azureCredentials = new ClientSecretCredential(
                azureConfigurations.AzureTenantId,
                azureConfigurations.AzureClientId,
                azureConfigurations.AzureClientSecret);
        }
        else
        {
          azureCredentials = new DefaultAzureCredential();
        }

        // // Connect to App Configuration store
        // configBuilder.AddAzureAppConfiguration((options) =>
        // {
        //   options.Connect(azureConfigurations.AzureAppConfigurationConnectionString)

        //     // Connect to Key Vault via App Config reference
        //     .ConfigureKeyVault(keyVaultOptions =>
        //     {
        //       keyVaultOptions.SetCredential(azureCredentials);
        //     });
        // });

        // Console.WriteLine(configuration["Reactivities:Settings:JWTSecretKey"]);

        // Connect to Azure Key Vault
        var vaultName = azureConfigurations.AzureKeyVaultName;

        if (!string.IsNullOrEmpty(vaultName))
        {
          var secretClientOptions = new SecretClientOptions()
          {
            Retry =
              {
                  Delay = TimeSpan.FromSeconds(2),
                  MaxDelay = TimeSpan.FromSeconds(16),
                  MaxRetries = 5,
                  Mode = RetryMode.Exponential,
              },
          };

          // NOTE: If Key Vault is added as the final provider, it will have the HIGHEST PRIORITY.
          configBuilder.AddAzureKeyVault(
            new SecretClient(new Uri($"https://{vaultName}.vault.azure.net"), azureCredentials, secretClientOptions),
            new KeyVaultSecretReplacer(configuration));
        }
      });
    }
  }
}
