﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Static;

namespace Persistence.Configuration
{
    public class ActivityAttendeeConfiguration : IEntityTypeConfiguration<ActivityAttendee>
    {
        // Define relations for ActivityAttendee table
        public void Configure(EntityTypeBuilder<ActivityAttendee> builder)
        {
            builder.HasKey(aa => new
            {
                aa.AppUserId,
                aa.ActivityId,
            });

            // ActivityAttendee relation with AppUser
            builder.HasOne(u => u.AppUser)
                .WithMany(a => a.Activities)
                .HasForeignKey(aa => aa.AppUserId);

            // ActivityAttendee relation with Activity
            builder.HasOne(a => a.Activity)
                .WithMany(att => att.Attendees)
                .HasForeignKey(aa => aa.ActivityId);

            builder.HasData(

                // Activity 1
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.PastActivity1,
                    AppUserId = SeedData.AppUsers.Bob,
                    // AppUser = SeedAppUsers.UserBob,
                    IsHost = true,
                },

                // Activity 2
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.PastActivity2,
                    AppUserId = SeedData.AppUsers.Bob,
                    // AppUser = SeedAppUsers.UserBob,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.PastActivity2,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = false,
                },

                // Activity 3
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity1,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity1,
                    AppUserId = SeedData.AppUsers.Tom,
                    // AppUser = SeedAppUsers.UserTom,
                    IsHost = false,
                },

                // Activity 4
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity2,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity2,
                    AppUserId = SeedData.AppUsers.Tom,
                    // AppUser = SeedAppUsers.UserTom,
                    IsHost = false,
                },

                // Activity 5
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity3,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity3,
                    AppUserId = SeedData.AppUsers.Bob,
                    // AppUser = SeedAppUsers.UserBob,
                    IsHost = false,
                },

                // Activity 6
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity4,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = true,
                },

                // Activity 7
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity5,
                    AppUserId = SeedData.AppUsers.Bob,
                    // AppUser = SeedAppUsers.UserBob,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity5,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = false,
                },

                // Activity 8
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity6,
                    AppUserId = SeedData.AppUsers.Tom,
                    // AppUser = SeedAppUsers.UserTom,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity6,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = false,
                },

                // Activity 9
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity7,
                    AppUserId = SeedData.AppUsers.Bob,
                    // AppUser = SeedAppUsers.UserBob,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity7,
                    AppUserId = SeedData.AppUsers.Tom,
                    // AppUser = SeedAppUsers.UserTom,
                    IsHost = false,
                },

                // Activity 10
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity8,
                    AppUserId = SeedData.AppUsers.Tom,
                    // AppUser = SeedAppUsers.UserTom,
                    IsHost = true,
                },
                new ActivityAttendee
                {
                    ActivityId = SeedData.Activities.FutureActivity8,
                    AppUserId = SeedData.AppUsers.Jane,
                    // AppUser = SeedAppUsers.UserJane,
                    IsHost = false,
                }
            );
        }
    }
}
