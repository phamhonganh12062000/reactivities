﻿using Contracts.Application.Services;
using Domain.Dtos;
using Domain.Dtos.Authentication;
using Domain.Exceptions.AppUser;
using Domain.Exceptions.Authentication;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Presentation.Controllers
{
  public class AccountController : BaseController
  {
    private readonly IServiceManager _service;

    public AccountController(IServiceManager service)
    {
      _service = service;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<ActionResult<AuthenticationDto>> Login(LoginDto loginDto)
    {
      try
      {
        var (result, user) = await _service.AuthenticationService.ValidateUserAsync(loginDto);

        if (!result)
        {
          return Unauthorized();
        }

        await SaveRefreshTokenToCookies(user);

        return await MapUserObjectToAuthenticationDto(user);
      }
      catch (AggregateException ae)
      {
        ae.Handle(ex =>
        {
          if (ex is AppUserNotFoundException or UnauthorizedAccessException)
          {
            return true; // The exception is handled.
          }

          return false; // The exception is not handled and will be thrown again.
        });
        return Unauthorized();
      }
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<ActionResult<AuthenticationDto>> Register([FromBody] RegistrationDto registrationDto)
    {
      var (result, user) = await _service.AuthenticationService.RegisterUserAsync(registrationDto);

      if (!result.Succeeded)
      {
        foreach (var error in result.Errors)
        {
          ModelState.TryAddModelError(error.Code, error.Description);
        }

        return BadRequest(ModelState);
      }

      var origin = Request.Headers["origin"];

      await _service.EmailConfirmationService.SendVerificationEmail(origin, user);

      return await MapUserObjectToAuthenticationDto(user);
    }

    [Authorize]
    [HttpGet]
    public async Task<ActionResult<AuthenticationDto>> GetCurrentUser()
    {
      var user = await _service.AuthenticationService.GetUserByEmailAsync(User.FindFirstValue(ClaimTypes.Email));

      await SaveRefreshTokenToCookies(user);

      return await MapUserObjectToAuthenticationDto(user);
    }

    [AllowAnonymous]
    [HttpPost("facebookLogin")]
    public async Task<ActionResult<AuthenticationDto>> FacebookLogin(string accessToken)
    {
      try
      {
        var (result, user) = await _service.FacebookLoginService.FacebookLogin(accessToken);
        if (!result.Succeeded)
        {
          foreach (var error in result.Errors)
          {
            ModelState.TryAddModelError(error.Code, error.Description);
          }

          return BadRequest(ModelState);
        }

        await SaveRefreshTokenToCookies(user);

        return await MapUserObjectToAuthenticationDto(user);
      }
      catch (UnauthorizedAccessException)
      {
        return Unauthorized();
      }
    }

    [Authorize]
    [HttpPost("refreshTokens")]
    public async Task<ActionResult<AuthenticationDto>> RefreshToken()
    {
      try
      {
        var refreshTokenInCookies = Request.Cookies["refreshToken"];

        var user = await _service.TokenService.FetchUserWithExpiredAccessToken(refreshTokenInCookies);

        return await MapUserObjectToAuthenticationDto(user);
      }
      catch (RefreshTokenBadRequest)
      {
        return Unauthorized();
      }
    }

    [AllowAnonymous]
    [HttpPost("verifyEmail")]
    public async Task<IActionResult> VerifyEmail(string token, string email)
    {
      var result = await _service.EmailConfirmationService.VerifyEmailAsync(token, email);

      if (!result.Succeeded)
      {
        return BadRequest("Could not verify email address");
      }

      return Ok("Email confirmed - You can now login");
    }

    [AllowAnonymous]
    [HttpGet("resendEmailConfirmationLink")]
    public async Task<IActionResult> ResendEmailConfirmationLink(string email)
    {
      var origin = Request.Headers["origin"];

      await _service.EmailConfirmationService.ResendEmailConfirmationLinkAsync(origin, email);

      return Ok("Email verification link resent");
    }

    private async Task<AuthenticationDto> MapUserObjectToAuthenticationDto(AppUser user)
    {
      var accessToken = await _service.TokenService.GenerateAccessToken(user);

      return new AuthenticationDto
      {
        DisplayName = user.DisplayName,
        Image = user?.Photos?.FirstOrDefault(p => p.IsMain)?.Url,
        AccessToken = accessToken,
        Username = user.UserName,
        Bio = user.Bio,
      };
    }

    private async Task SaveRefreshTokenToCookies(AppUser user)
    {
      var refreshToken = await _service.TokenService.PopulateRefreshTokenExpiryDate(user, populateExpiryDate: true);

      var cookieOptions = new CookieOptions
      {
        HttpOnly = true,
        Expires = DateTime.UtcNow.AddDays(7),
      };

      Response.Cookies.Append("refreshToken", refreshToken, cookieOptions);
    }
  }
}
