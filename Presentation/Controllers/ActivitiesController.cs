﻿using Application.Services.Activities.Commands;
using Application.Services.Activities.Handlers;
using Application.Services.Activities.Notifications;
using Application.Services.Activities.Queries;
using Domain.Dtos.Activity;
using Domain.ParameterModels;
using Domain.Results;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Presentation.ActionFilters;
using Presentation.ModelBinders;
using Presentation.Utility;
using Shared.Static;
using System.Text.Json;

namespace Presentation.Controllers
{
  public class ActivitiesController : BaseController
  {
    [HttpGet(Name = "GetEntireActivities")]
    public async Task<IActionResult> GetAllActivities([FromQuery] ActivityParameters activityParameters)
    {
      var (activityDtos, metaData) = await Mediator.Send(new GetAllActivitiesQuery(activityParameters, TrackChanges: false));

      Response.AddPaginationHeader(metaData);

      return HandleResult(activityDtos);
    }

    [HttpGet("collection/({ids})", Name = "GetActivityCollection")]
    public async Task<IActionResult> GetMultipleActivities([ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
    {
      return HandleResult(await Mediator.Send(new GetMultipleActivitiesQuery(ids, TrackChanges: false)));
    }

    [HttpGet("{id:guid}", Name = "GetActivity")]

    // Action configs can override global configs
    public async Task<IActionResult> GetSingleActivity(Guid id)
    {
      return HandleResult(await Mediator.Send(new GetSingleActivityQuery(id, TrackChanges: false)));
    }

    [HttpPost]
    public async Task<IActionResult> CreateSingleActivity([FromBody] ActivityForCreationDto activityForCreationDto)
    {
      var result = await Mediator.Send(new CreateSingleActivityCommand(activityForCreationDto));

      return HandleResult(result);
    }

    [Authorize(Policy = "IsActivityHost")]
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> UpdateSingleActivity(Guid id, ActivityForUpdateDto activityForUpdateDto)
    {
      return HandleResult(await Mediator.Send(new UpdateSingleActivityCommand(id, activityForUpdateDto, TrackChanges: true)));
    }

    [Authorize(Policy = "IsActivityHost")]
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteSingleActivity(Guid id)
    {
      await Mediator.Publish(new ActivityDeletedNotification(id, TrackChanges: false));

      return NoContent();
    }

    [HttpPut("{id:guid}/attend")]
    public async Task<IActionResult> Attend(Guid id)
    {
      return HandleResult(await Mediator.Send(new UpdateAttendanceCommand(id, TrackChanges: true)));
    }

    [HttpOptions]
    public IActionResult GetActivitiesOptions()
    {
      Response.Headers.Add("Allow", "GET, POST, PUT, DELETE OPTIONS");
      return Ok();
    }
  }
}
