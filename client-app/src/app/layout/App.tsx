import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Outlet, ScrollRestoration, useLocation } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { Container } from "semantic-ui-react";
import ModalContainer from "../common/modals/ModalContainer";
import HomePage from "../home/HomePage";
import { useStore } from "../stores/baseStore";
import LoadingComponent from "./LoadingComponent";
import NavBar from "./NavBar";

function App() {
    const location = useLocation();
    const { commonStore, userStore } = useStore();

    useEffect(() => {
        // Normal login
        if (commonStore.accessToken) {
            userStore.getUser().finally(() => commonStore.setAppLoaded());
        } else {
        // Login with facebook
            userStore
                .getFacebookLoginStatus()
                .then(() => commonStore.setAppLoaded());
        }
    }, [commonStore, userStore]);

    if (!commonStore.isAppLoaded) {
        return <LoadingComponent content="Loading the application..." />;
    }

    return (
        <>
            <ScrollRestoration />
            <ModalContainer />
            <ToastContainer position="bottom-right" theme="colored" />
            {location.pathname === "/" ? (
                <HomePage />
            ) : (
                <>
                    <NavBar />
                    <Container style={{ marginTop: "7em" }}>
                        <Outlet />
                    </Container>
                </>
            )}
        </>
    );
}

export default observer(App);
