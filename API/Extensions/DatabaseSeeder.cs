﻿using Contracts.Logger;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Extensions;

namespace API.Extensions
{
  public class DatabaseSeeder
  {
    private readonly IServiceProvider _serviceProvider;

    public DatabaseSeeder(IServiceProvider serviceProvider)
    {
      _serviceProvider = serviceProvider;
    }

    public async Task SeedDatabase()
    {
      using var scope = _serviceProvider.CreateScope();
      try
      {
        var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();

        // Disable foreign key constraints for Postgres
        // await dataContext.Database.ExecuteSqlRawAsync("ALTER TABLE ActivityAttendees DROP CONSTRAINT FK_ActivityAttendees_Activities_ActivityId;");

        // Seed data
        await dataContext.Database.MigrateAsync();
        DbInitializer.Initialize(userManager);

        // Re-enable foreign key constraints for Postgres
        // await dataContext.Database.ExecuteSqlRawAsync("ALTER TABLE ActivityAttendees ADD CONSTRAINT FK_ActivityAttendees_Activities_ActivityId FOREIGN KEY (ActivityId) REFERENCES Activities (ActivityId);");
      }
      catch (Exception ex)
      {
        var logger = scope.ServiceProvider.GetRequiredService<ILoggerManager>();
        logger.LogError(ex, "An error has occurred during migration.");
        throw;
      }
    }
  }
}
