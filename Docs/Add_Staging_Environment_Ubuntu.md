# Use Local Ubuntu machine as the staging environment

## Setups

-   Install the .NET SDK instead of the runtime package

## Manage secrets

-   Install the User Secrets tool `dotnet tool install --global dotnet-user-secrets`
-   Set secrets at the **startup folder**, as these secrets can be accessed via the `IConfiguration` interface

```bash
dotnet user-secrets init
dotnet user-secrets set "JWTSecretKey" "your_api_key_here"
dotnet user-secrets set "CloudinarySettings:CloudName" "my-cloud-name"
dotnet user-secrets set "CloudinarySettings:ApiKey" "your_api_key_here"
dotnet user-secrets set "CloudinarySettings:ApiSecret" "your_api_secret_here"
```
