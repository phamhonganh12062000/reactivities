import { observer } from "mobx-react-lite";
import Calendar from "react-calendar";
import { Header, Menu } from "semantic-ui-react";
import { useStore } from "../../../app/stores/baseStore";

export default observer(function ActivityFilters() {
    const {
        activityStore: { predicate, setPredicateKeyValue },
    } = useStore();

    return (
        <>
            <Menu
                vertical
                size="large"
                style={{ width: "100%", marginTop: 25 }}
            >
                <Header icon="filter" attached color="teal" content="Filters" />
                <Menu.Item
                    content="All Activities"
                    active={predicate.has("all")}
                    onClick={() => setPredicateKeyValue("all", "true")}
                />
                <Menu.Item
                    content="I'm going"
                    active={predicate.has("isGoingFilter")}
                    onClick={() =>
                        setPredicateKeyValue("isGoingFilter", "true")
                    }
                />
                <Menu.Item
                    content="I'm hosting"
                    active={predicate.has("isHostFilter")}
                    onClick={() => setPredicateKeyValue("isHostFilter", "true")}
                />
            </Menu>
            <Header
                icon="calendar"
                attached
                color="teal"
                content="Select date"
            />
            <Calendar
                onChange={(date: any) =>
                    setPredicateKeyValue("startDate", date as Date)
                }
                value={predicate.get("startDate") || new Date()}
            />
        </>
    );
});
