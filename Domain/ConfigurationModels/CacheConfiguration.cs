using Microsoft.Extensions.Caching.Memory;

namespace Domain.ConfigurationModels
{
  public class CacheConfiguration
  {
    public TimeSpan SlidingExpiration { get; set; }

    public TimeSpan AbsoluteExpiration { get; set; }

    public CacheItemPriority Priority { get; set; }

    public long Size { get; set; }
  }
}
