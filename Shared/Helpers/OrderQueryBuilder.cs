using System.Reflection;
using System.Text;

namespace Shared.Helpers
{
    public static class OrderQueryBuilder
    {
        public static string CreateOrderQuery<TEntity>(string orderByQueryString)
        {
            string[] orderParams = SplitOrderParamsFromQuery(orderByQueryString);

            PropertyInfo[] propertyInfos = GetPropertyInfos<TEntity>();

            string orderQuery = BuildOrderQuery(orderParams, propertyInfos);

            return orderQuery;
        }

        private static string[] SplitOrderParamsFromQuery(string orderByQueryString)
        {
            return orderByQueryString.Trim().Split(",");
        }

        private static PropertyInfo[] GetPropertyInfos<TEntity>()
        {
            return typeof(TEntity).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        private static PropertyInfo GetPropertyByName(PropertyInfo[] propertyInfos, string propertyNameFromQuery)
        {
            return propertyInfos.FirstOrDefault(pi => pi.Name.Equals(propertyNameFromQuery, StringComparison.InvariantCultureIgnoreCase));
        }

        private static string BuildOrderQuery(string[] orderParams, PropertyInfo[] propertyInfos)
        {
            StringBuilder orderQueryBuilder = new ();

            foreach (var param in orderParams)
            {
                if (string.IsNullOrWhiteSpace(param))
                {
                    continue;
                }

                // Remove white spaces and get the entity's property
                string propertyNameFromQuery = param.Split(" ")[0];
                PropertyInfo objectProperty = GetPropertyByName(propertyInfos, propertyNameFromQuery);

                if (objectProperty == null)
                {
                    continue;
                }

                // Decide whether the order would be descending or ascending
                var orderDirection = param.EndsWith(" desc") ? "descending" : "ascending";

                orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {orderDirection}, ");
            }

            return orderQueryBuilder.ToString().TrimEnd(',', ' ');
        }
    }
}
