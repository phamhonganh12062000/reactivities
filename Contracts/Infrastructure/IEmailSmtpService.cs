namespace Contracts.Infrastructure
{
  public interface IEmailSenderFactory
  {
    Task SendEmailAsync(string email, string subject, string htmlMessage);

  }

}
