export interface PagingMetaData {
    currentPage: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
    hasNext: boolean;
}

export class PaginatedResult<T> {
    data: T;
    pagingMetaData: PagingMetaData;

    constructor(data: T, pagingMetaData: PagingMetaData) {
        this.data = data;
        this.pagingMetaData = pagingMetaData;
    }
}

export class PagingParams {
    pageNumber;
    pageSize;

    constructor(pageNumber = 1, pageSize = 2) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }
}
