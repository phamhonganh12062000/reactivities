﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class SwitchDbToPostgres : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Category = table.Column<string>(type: "text", nullable: true),
                    City = table.Column<string>(type: "text", nullable: true),
                    Venue = table.Column<string>(type: "text", nullable: true),
                    IsCancelled = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: true),
                    Bio = table.Column<string>(type: "text", nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActivityAttendees",
                columns: table => new
                {
                    AppUserId = table.Column<string>(type: "text", nullable: false),
                    ActivityId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsHost = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityAttendees", x => new { x.AppUserId, x.ActivityId });
                    table.ForeignKey(
                        name: "FK_ActivityAttendees_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityAttendees_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Body = table.Column<string>(type: "text", nullable: true),
                    AuthorId = table.Column<string>(type: "text", nullable: true),
                    ActivityId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: true),
                    IsMain = table.Column<bool>(type: "boolean", nullable: false),
                    AppUserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photos_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserFollowings",
                columns: table => new
                {
                    ObserverId = table.Column<string>(type: "text", nullable: false),
                    TargetId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFollowings", x => new { x.ObserverId, x.TargetId });
                    table.ForeignKey(
                        name: "FK_UserFollowings_AspNetUsers_ObserverId",
                        column: x => x.ObserverId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserFollowings_AspNetUsers_TargetId",
                        column: x => x.TargetId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Activities",
                columns: new[] { "Id", "Category", "City", "Date", "Description", "IsCancelled", "Title", "Venue" },
                values: new object[,]
                {
                    { new Guid("24d5c211-ed62-4c42-8e55-3a5b27964f2b"), "food", "Hanoi", new DateTime(2024, 1, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2225), "Activity 1 month ago", false, "Past Activity 2", "Restaurant" },
                    { new Guid("2cdbb75f-9c94-43f9-b80b-76da48ca38c9"), "music", "London", new DateTime(2024, 8, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2235), "Activity 6 months in future", false, "Future Activity 6", "O2 Arena" },
                    { new Guid("8e3baf54-02e1-4ffe-b458-0004f5da7063"), "food", "Hanoi", new DateTime(2024, 5, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2230), "Activity 3 months next", false, "Future Activity 3", "Restaurant" },
                    { new Guid("927d5ee8-27b0-4fd9-a748-6d2532fc3430"), "drinks", "London", new DateTime(2024, 7, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2233), "Activity 5 months in future", false, "Future Activity 5", "Punch and Judy" },
                    { new Guid("956b6ed6-dc59-47d2-91ad-3e4eb968901a"), "drinks", "London", new DateTime(2024, 10, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2238), "Activity 8 months in future", false, "Future Activity 8", "Pub" },
                    { new Guid("a91286aa-4a4d-46f1-9b7e-a6236b4ff446"), "travel", "Berlin", new DateTime(2024, 9, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2237), "Activity 7 months in future", false, "Future Activity 7", "All" },
                    { new Guid("bf3011d1-d759-4a7c-a92b-8948945b4a2d"), "drinks", "London", new DateTime(2024, 3, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2227), "Activity 1 month next", false, "Future Activity 1", "Pub" },
                    { new Guid("c2f9c062-7623-4101-9170-37698edc7707"), "drinks", "London", new DateTime(2023, 12, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2215), "Activity 2 months ago", false, "Past Activity 1", "Pub" },
                    { new Guid("e48d0284-f3c7-4101-a9e8-98ce9657bc1a"), "drinks", "London", new DateTime(2024, 4, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2229), "Activity 2 months next", false, "Future Activity 2", "Pub" },
                    { new Guid("efca1dc6-cbde-47fd-8141-44c4a2d7c291"), "culture", "London", new DateTime(2024, 6, 5, 16, 28, 49, 892, DateTimeKind.Utc).AddTicks(2232), "Activity 4 months in future", false, "Future Activity 4", "British Museum" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "Bio", "ConcurrencyStamp", "DisplayName", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "0781475a-89de-4a77-98f6-4d04e2878cc1", 0, null, "0a4280d2-8474-4efc-a7c3-e36149bf4220", "Tom", "tom@test.com", false, false, null, null, null, null, null, false, "69f32245-8531-44fb-b873-122098eed61f", false, "tom" },
                    { "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", 0, null, "0a357677-d21c-4df0-af04-fac78b8756e0", "Jane", "jane@test.com", false, false, null, null, null, null, null, false, "0db9b220-90b1-4e21-8e2b-42b8b3f8d3b6", false, "jane" },
                    { "975ef757-940c-47a9-98b0-42b0707769c8", 0, null, "5530b0f2-09a6-42e1-8b27-ac6c61921a0a", "Bob", "bob@test.com", false, false, null, null, null, null, null, false, "ea268762-c1eb-4277-856c-c4721f6d03ff", false, "bob" }
                });

            migrationBuilder.InsertData(
                table: "ActivityAttendees",
                columns: new[] { "ActivityId", "AppUserId", "IsHost" },
                values: new object[,]
                {
                    { new Guid("2cdbb75f-9c94-43f9-b80b-76da48ca38c9"), "0781475a-89de-4a77-98f6-4d04e2878cc1", true },
                    { new Guid("956b6ed6-dc59-47d2-91ad-3e4eb968901a"), "0781475a-89de-4a77-98f6-4d04e2878cc1", true },
                    { new Guid("a91286aa-4a4d-46f1-9b7e-a6236b4ff446"), "0781475a-89de-4a77-98f6-4d04e2878cc1", false },
                    { new Guid("bf3011d1-d759-4a7c-a92b-8948945b4a2d"), "0781475a-89de-4a77-98f6-4d04e2878cc1", false },
                    { new Guid("e48d0284-f3c7-4101-a9e8-98ce9657bc1a"), "0781475a-89de-4a77-98f6-4d04e2878cc1", false },
                    { new Guid("24d5c211-ed62-4c42-8e55-3a5b27964f2b"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", false },
                    { new Guid("2cdbb75f-9c94-43f9-b80b-76da48ca38c9"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", false },
                    { new Guid("8e3baf54-02e1-4ffe-b458-0004f5da7063"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", true },
                    { new Guid("927d5ee8-27b0-4fd9-a748-6d2532fc3430"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", false },
                    { new Guid("956b6ed6-dc59-47d2-91ad-3e4eb968901a"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", false },
                    { new Guid("bf3011d1-d759-4a7c-a92b-8948945b4a2d"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", true },
                    { new Guid("e48d0284-f3c7-4101-a9e8-98ce9657bc1a"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", true },
                    { new Guid("efca1dc6-cbde-47fd-8141-44c4a2d7c291"), "89aa0cb3-69a3-4dfd-b46f-1e442a14602c", true },
                    { new Guid("24d5c211-ed62-4c42-8e55-3a5b27964f2b"), "975ef757-940c-47a9-98b0-42b0707769c8", true },
                    { new Guid("8e3baf54-02e1-4ffe-b458-0004f5da7063"), "975ef757-940c-47a9-98b0-42b0707769c8", false },
                    { new Guid("927d5ee8-27b0-4fd9-a748-6d2532fc3430"), "975ef757-940c-47a9-98b0-42b0707769c8", true },
                    { new Guid("a91286aa-4a4d-46f1-9b7e-a6236b4ff446"), "975ef757-940c-47a9-98b0-42b0707769c8", true },
                    { new Guid("c2f9c062-7623-4101-9170-37698edc7707"), "975ef757-940c-47a9-98b0-42b0707769c8", true }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAttendees_ActivityId",
                table: "ActivityAttendees",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ActivityId",
                table: "Comments",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_AppUserId",
                table: "Photos",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFollowings_TargetId",
                table: "UserFollowings",
                column: "TargetId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityAttendees");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "UserFollowings");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
