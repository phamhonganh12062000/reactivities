﻿using Contracts.Persistence;
using Persistence.Repositories;

namespace Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        private readonly Lazy<IActivityRepository> _activityRepository;

        private readonly Lazy<IAppUserRepository> _appUserRepository;

        private readonly Lazy<IActivityAttendeeRepository> _activityAttendeeRepository;

        private readonly Lazy<ICommentRepository> _commentRepository;

        private readonly Lazy<IUserFollowingRepository> _userFollowingRepository;

        public UnitOfWork(DataContext context)
        {
            _context = context;

            // Deferred initialization with Lazy<T>
            _activityRepository = new Lazy<IActivityRepository>(() => new ActivityRepository(context));
            _appUserRepository = new Lazy<IAppUserRepository>(() => new AppUserRepository(context));
            _activityAttendeeRepository = new Lazy<IActivityAttendeeRepository>(() => new ActivityAttendeeRepository(context));
            _commentRepository = new Lazy<ICommentRepository>(() => new CommentRepository(context));
            _userFollowingRepository = new Lazy<IUserFollowingRepository>(() => new UserFollowingRepository(context));
        }

        public IActivityRepository Activity
        {
            get
            {
                return _activityRepository.Value;
            }
        }

        public IAppUserRepository AppUser
        {
            get
            {
                return _appUserRepository.Value;
            }
        }

        public IActivityAttendeeRepository ActivityAttendee
        {
            get
            {
                return _activityAttendeeRepository.Value;
            }
        }

        public ICommentRepository Comment
        {
            get
            {
                return _commentRepository.Value;
            }
        }

        public IUserFollowingRepository UserFollowing
        {
            get
            {
                return _userFollowingRepository.Value;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
