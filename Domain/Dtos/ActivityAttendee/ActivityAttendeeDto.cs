﻿namespace Domain.Dtos.ActivityAttendee
{
    public record ActivityAttendeeDto
    {
        public string Username { get; init; }

        public string DisplayName { get; init; }

        public string Image { get; init; }

        public string Bio { get; init; }

        public bool IsFollowingThisUser { get; set; }

        public int FollowersCount { get; set; }

        public int FollowingCount { get; set; }
    }
}
