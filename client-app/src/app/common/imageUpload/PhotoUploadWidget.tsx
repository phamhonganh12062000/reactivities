import { useEffect, useState } from "react";
import { Button, Grid, Header } from "semantic-ui-react";
import PhotoWidgetCropper from "./PhotoWidgetCropper";
import PhotoWidgetDropzone from "./PhotoWidgetDropzone";

interface Props {
    loading: boolean;
    uploadPhoto: (file: Blob) => void;
}

export default function PhotoUploadWidget({ loading, uploadPhoto }: Props) {
    const [filesArr, setFiles] = useState<any>([]);
    const [cropper, setCropper] = useState<Cropper>();

    function onCrop() {
        if (cropper) {
            cropper.getCroppedCanvas().toBlob((blob) => uploadPhoto(blob!));
        }
    }

    // Dispose file url components
    useEffect(() => {
        return () => {
            filesArr.forEach((file: any) => {
                URL.revokeObjectURL(file.preview);
            });
        };
    }, [filesArr]);

    return (
        <Grid>
            <Grid.Column width={4}>
                <Header sub color="teal" content="Step 1 - Add Photo" />
                <PhotoWidgetDropzone setFiles={setFiles} />
            </Grid.Column>
            <Grid.Column width={1} />
            <Grid.Column width={4}>
                <Header sub color="teal" content="Step 2 - Resize Photo" />
                {filesArr && filesArr.length > 0 && (
                    <PhotoWidgetCropper
                        setCropper={setCropper}
                        imagePreview={filesArr[0].preview}
                    />
                )}
            </Grid.Column>
            <Grid.Column width={1} />
            <Grid.Column width={4}>
                <Header sub color="teal" content="Step 3 - Preview & Upload" />
                {filesArr && filesArr.length > 0 && (
                    <>
                        <div
                            className="img-preview"
                            style={{ minHeight: 200, overflow: "hidden" }}
                        />
                        <Button.Group widths={2}>
                            <Button
                                loading={loading}
                                onClick={onCrop}
                                positive
                                icon="check"
                            />
                            {/* Not intend to crop => set the file array to null */}
                            <Button
                                disabled={loading}
                                onClick={() => setFiles([])}
                                icon="close"
                            />
                        </Button.Group>
                    </>
                )}
            </Grid.Column>
        </Grid>
    );
}
