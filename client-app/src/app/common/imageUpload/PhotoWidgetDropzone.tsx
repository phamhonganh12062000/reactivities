import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { Header, Icon } from "semantic-ui-react";

interface Props {
    setFiles: (files: any) => void;
}

export default function PhotoWidgetDropzone({ setFiles }: Props) {
    const dropzoneStyles = {
        border: "dashed 3px #eee",
        borderColor: "#eee",
        borderRadius: "5px",
        paddingTop: "30px",
        textAlign: "center" as "center", // TODO: Why?
        height: 200,
    };

    const dropzoneStylesActive = {
        borderColor: "green",
    };

    // Create preview URLs for each image
    // Update the `files` state with a new array of files
    const onDrop = useCallback(
        (acceptedFilesArr: any) => {
            setFiles(
                acceptedFilesArr.map((file: any) =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    })
                )
            );
        },
        [setFiles]
    );

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
    });

    return (
        <div
            {...getRootProps()}
            style={
                isDragActive
                    ? { ...dropzoneStyles, ...dropzoneStylesActive }
                    : { ...dropzoneStyles }
            }
        >
            <input {...getInputProps()} />
            <Icon name="upload" size="huge" />
            <Header content="Drop your image here" />
        </div>
    );
}
