using System.Text.Json.Serialization;

namespace Domain.Dtos.Activity
{
    public class UserActivityDto
    {
        public Guid Id { get; init; }

        public string Title { get; init; }

        public string Category { get; init; }

        public DateTime Date { get; init; }

        [JsonIgnore]
        public string HostUsername { get; init; }
    }
}
